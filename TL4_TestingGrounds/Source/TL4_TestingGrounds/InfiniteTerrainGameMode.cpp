// Copyright (c) Shivam Mukherjee 2017

#include "TL4_TestingGrounds.h"
#include "InfiniteTerrainGameMode.h"
#include "NavMesh/NavMeshBoundsVolume.h"
#include "EngineUtils.h"
#include "ActorPool.h"
#include "TL4_SaveGame.h"
#include "Kismet/GameplayStatics.h"

AInfiniteTerrainGameMode::AInfiniteTerrainGameMode()
{
	Score = 0;
	NavMeshBoundsVolumePool = CreateDefaultSubobject<UActorPool>(FName(TEXT("Nav Mesh Bounds Volume Pool")));
}

void AInfiniteTerrainGameMode::BeginPlay()
{
	Super::BeginPlay();

	auto *SavedGameData = Cast<UTL4_SaveGame>(UGameplayStatics::CreateSaveGameObject(UTL4_SaveGame::StaticClass()));
	SavedGameData = Cast<UTL4_SaveGame>(UGameplayStatics::LoadGameFromSlot(UTL4_SaveGame::SaveSlotName, UTL4_SaveGame::UserIndex));
	if (SavedGameData)
	{
		HighScore = SavedGameData->HighScore;
		UE_LOG(LogTestingGrounds, Warning, TEXT("Game saved loaded successfully. HighScore == %d"), HighScore);
	}
}

void AInfiniteTerrainGameMode::PopulateBoundsVolumePool()
{
	for (auto* FoundVolume : TActorRange<ANavMeshBoundsVolume>(GetWorld()))
	{
		AddToPool(FoundVolume);
	}
}

void AInfiniteTerrainGameMode::NewTileConquered()
{
	Score++;
	
	if (Score > HighScore)
	{
		HighScore = Score;
		bHighScoreChangedAtLeastOnce = true;
	}

	UE_LOG(LogTestingGrounds, Warning, TEXT("[%s] Score: %i"), *GetName(), Score);
}

void AInfiniteTerrainGameMode::BeginDestroy()
{
	if (bHighScoreChangedAtLeastOnce)
	{
		auto *SavedGameData = Cast<UTL4_SaveGame>(UGameplayStatics::CreateSaveGameObject(UTL4_SaveGame::StaticClass()));
		if (SavedGameData)
		{
			SavedGameData->HighScore = HighScore;
			UGameplayStatics::SaveGameToSlot(SavedGameData, UTL4_SaveGame::SaveSlotName, UTL4_SaveGame::UserIndex);
			UE_LOG(LogTestingGrounds, Warning, TEXT("Game saved successfully. HighScore == %d"), SavedGameData->HighScore);
		}
	}
	else
	{
		UE_LOG(LogTestingGrounds, Warning, TEXT("Game save unchanged. HighScore == %d"), HighScore);
	}

	Super::BeginDestroy();
}

void AInfiniteTerrainGameMode::AddToPool(ANavMeshBoundsVolume *VolumeToAdd)
{
	NavMeshBoundsVolumePool->Add(VolumeToAdd);
} 
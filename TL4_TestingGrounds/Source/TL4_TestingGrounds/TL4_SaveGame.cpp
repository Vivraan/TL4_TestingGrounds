 // Copyright (c) Shivam Mukherjee 2017

#include "TL4_TestingGrounds.h"
#include "TL4_SaveGame.h"

const FString UTL4_SaveGame::SaveSlotName = TEXT("HighScore");
const int32 UTL4_SaveGame::UserIndex = 0;

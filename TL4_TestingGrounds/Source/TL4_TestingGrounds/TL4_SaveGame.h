// Copyright (c) Shivam Mukherjee 2017

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SaveGame.h"
#include "TL4_SaveGame.generated.h"

/**
 * 
 */
UCLASS()
class TL4_TESTINGGROUNDS_API UTL4_SaveGame : public USaveGame
{
	GENERATED_BODY()

public:
	static const FString SaveSlotName;
	static const int32 UserIndex;

	UPROPERTY(BlueprintReadOnly, Category = "Score")
	int32 HighScore;
};

// Copyright (c) Shivam Mukherjee 2017

using UnrealBuildTool;

public class NativizedAssets : ModuleRules
{
	public NativizedAssets(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;
		PrivatePCHHeaderFile = "Private/NativizedAssets.h";
	
		PublicDependencyModuleNames.AddRange(new string[] { "Core", "TL4_TestingGrounds" });

		PrivateDependencyModuleNames.AddRange(new string[] { "CoreUObject", "Engine", "NavigationSystem", "UMG", "InputCore", "AnimGraphRuntime", "SlateCore", "AIModule", "GameplayTasks", "ClothingSystemRuntimeInterface", "AudioPlatformConfiguration", "PropertyPath", "MovieScene", "Slate", "AnimationCore", "ClothingSystemRuntime", "GameplayTags" });

		// Uncomment if you are using Slate UI
		// PrivateDependencyModuleNames.AddRange(new string[] { "Slate", "SlateCore" });
		
		// Uncomment if you are using online features
		// PrivateDependencyModuleNames.Add("OnlineSubsystem");

		// To include OnlineSubsystemSteam, add it to the plugins section in your uproject file with the Enabled attribute set to true
	}
}

#pragma once
#include "Blueprint/BlueprintSupport.h"
#include "Runtime/UMG/Public/Blueprint/UserWidget.h"
class UTextBlock;
class UButton;
#include "WBP_GameOver__pf3389175594.generated.h"
UCLASS(config=Engine, Blueprintable, BlueprintType, meta=(ReplaceConverted="/Game/Dynamic/UI/WBP_GameOver.WBP_GameOver_C", OverrideNativeName="WBP_GameOver_C"))
class UWBP_GameOver_C__pf3389175594 : public UUserWidget
{
public:
	GENERATED_BODY()
	UPROPERTY(BlueprintReadWrite, Export, meta=(DisplayName="Button_758", Category="WBP_GameOver", OverrideNativeName="Button_758"))
	UButton* bpv__Button_758__pf;
	UPROPERTY(BlueprintReadWrite, Export, meta=(DisplayName="Button_760", Category="WBP_GameOver", OverrideNativeName="Button_760"))
	UButton* bpv__Button_760__pf;
	UPROPERTY(Export, meta=(DisplayName="TextBlock_191", OverrideNativeName="TextBlock_191"))
	UTextBlock* bpv__TextBlock_191__pf;
	UWBP_GameOver_C__pf3389175594(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());
	virtual void PostLoadSubobjects(FObjectInstancingGraph* OuterInstanceGraph) override;
	static void __CustomDynamicClassInitialization(UDynamicClass* InDynamicClass);
	static void __StaticDependenciesAssets(TArray<FBlueprintDependencyData>& AssetsToLoad);
	static void __StaticDependencies_DirectlyUsedAssets(TArray<FBlueprintDependencyData>& AssetsToLoad);
	void bpf__ExecuteUbergraph_WBP_GameOver__pf_0(int32 bpp__EntryPoint__pf);
	void bpf__ExecuteUbergraph_WBP_GameOver__pf_1(int32 bpp__EntryPoint__pf);
	UFUNCTION(meta=(OverrideNativeName="BndEvt__Button_760_K2Node_ComponentBoundEvent_100_OnButtonClickedEvent__DelegateSignature"))
	virtual void bpf__BndEvt__Button_760_K2Node_ComponentBoundEvent_100_OnButtonClickedEvent__DelegateSignature__pf();
	UFUNCTION(meta=(OverrideNativeName="BndEvt__Button_758_K2Node_ComponentBoundEvent_35_OnButtonClickedEvent__DelegateSignature"))
	virtual void bpf__BndEvt__Button_758_K2Node_ComponentBoundEvent_35_OnButtonClickedEvent__DelegateSignature__pf();
	UFUNCTION(BlueprintCallable, BlueprintPure, meta=(Category, OverrideNativeName="GetScore"))
	virtual FText  bpf__GetScore__pf();
	UFUNCTION(BlueprintCallable, BlueprintPure, meta=(Category, OverrideNativeName="GetHealthPercent"))
	virtual float  bpf__GetHealthPercent__pf();
	UFUNCTION(BlueprintCallable, BlueprintPure, meta=(Category, OverrideNativeName="GetHealthText"))
	virtual FText  bpf__GetHealthText__pf();
	UFUNCTION(BlueprintCallable, BlueprintPure, meta=(Category, OverrideNativeName="GetGameOverText"))
	virtual FText  bpf__GetGameOverText__pf();
public:
	virtual void GetSlotNames(TArray<FName>& SlotNames) const override;
	virtual void PreSave(const class ITargetPlatform* TargetPlatform) override;
	virtual void InitializeNativeClassData() override;
};

#pragma once
#include "Blueprint/BlueprintSupport.h"
#include "Runtime/Engine/Classes/Components/SceneComponent.h"
#include "BP_FireComponent__pf704589392.generated.h"
UCLASS(config=Engine, Blueprintable, BlueprintType, meta=(ReplaceConverted="/Game/Dynamic/Weapons/FPWeapon/Behaviour/BP_FireComponent.BP_FireComponent_C", OverrideNativeName="BP_FireComponent_C"))
class UBP_FireComponent_C__pf704589392 : public USceneComponent
{
public:
	GENERATED_BODY()
	UBP_FireComponent_C__pf704589392(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());
	virtual void PostLoadSubobjects(FObjectInstancingGraph* OuterInstanceGraph) override;
	static void __CustomDynamicClassInitialization(UDynamicClass* InDynamicClass);
	static void __StaticDependenciesAssets(TArray<FBlueprintDependencyData>& AssetsToLoad);
	static void __StaticDependencies_DirectlyUsedAssets(TArray<FBlueprintDependencyData>& AssetsToLoad);
	UFUNCTION(BlueprintCallable, meta=(Category, OverrideNativeName="Fire"))
	virtual void bpf__Fire__pf();
public:
};

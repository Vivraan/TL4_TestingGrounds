#pragma once
#include "Blueprint/BlueprintSupport.h"
#include "Runtime/UMG/Public/Blueprint/UserWidget.h"
class UTL4_SaveGame;
class UTextBlock;
class UProgressBar;
#include "WBP_Score__pf3389175594.generated.h"
UCLASS(config=Engine, Blueprintable, BlueprintType, meta=(ReplaceConverted="/Game/Dynamic/UI/WBP_Score.WBP_Score_C", OverrideNativeName="WBP_Score_C"))
class UWBP_Score_C__pf3389175594 : public UUserWidget
{
public:
	GENERATED_BODY()
	UPROPERTY(BlueprintReadWrite, Export, meta=(DisplayName="Health Bar", Category="WBP_Score", OverrideNativeName="HealthBar"))
	UProgressBar* bpv__HealthBar__pf;
	UPROPERTY(Export, meta=(DisplayName="High Score", OverrideNativeName="HighScore"))
	UTextBlock* bpv__HighScore__pf;
	UPROPERTY(Export, meta=(DisplayName="TextBlock_731", OverrideNativeName="TextBlock_731"))
	UTextBlock* bpv__TextBlock_731__pf;
	UPROPERTY(Export, meta=(DisplayName="Tiles Conquered", OverrideNativeName="TilesConquered"))
	UTextBlock* bpv__TilesConquered__pf;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta=(DisplayName="Saved Game Data", Category="Default", MultiLine="true", OverrideNativeName="SavedGameData"))
	UTL4_SaveGame* bpv__SavedGameData__pf;
	UPROPERTY(Transient, DuplicateTransient, meta=(OverrideNativeName="K2Node_DynamicCast_AsTL4_Save_Game"))
	UTL4_SaveGame* b0l__K2Node_DynamicCast_AsTL4_Save_Game__pf;
	UPROPERTY(Transient, DuplicateTransient, meta=(OverrideNativeName="K2Node_DynamicCast_bSuccess"))
	bool b0l__K2Node_DynamicCast_bSuccess__pf;
	UWBP_Score_C__pf3389175594(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());
	virtual void PostLoadSubobjects(FObjectInstancingGraph* OuterInstanceGraph) override;
	static void __CustomDynamicClassInitialization(UDynamicClass* InDynamicClass);
	static void __StaticDependenciesAssets(TArray<FBlueprintDependencyData>& AssetsToLoad);
	static void __StaticDependencies_DirectlyUsedAssets(TArray<FBlueprintDependencyData>& AssetsToLoad);
	void bpf__ExecuteUbergraph_WBP_Score__pf_0(int32 bpp__EntryPoint__pf);
	UFUNCTION(BlueprintCosmetic, meta=(Category="User Interface", Keywords="Begin Play", ToolTip="Called after the underlying slate widget is constructed.  Depending on how the slate object is usedthis event may be called multiple times due to adding and removing from the hierarchy.If you need a true called-once-when-created event, use OnInitialized.", CppFromBpEvent, OverrideNativeName="Construct"))
	virtual void bpf__Construct__pf();
	UFUNCTION(BlueprintCallable, BlueprintPure, meta=(Category, OverrideNativeName="GetScore"))
	virtual FText  bpf__GetScore__pf();
	UFUNCTION(BlueprintCallable, BlueprintPure, meta=(Category, OverrideNativeName="GetHealthPercent"))
	virtual float  bpf__GetHealthPercent__pf();
	UFUNCTION(BlueprintCallable, BlueprintPure, meta=(Category, OverrideNativeName="GetHealthText"))
	virtual FText  bpf__GetHealthText__pf();
	UFUNCTION(BlueprintCallable, BlueprintPure, meta=(Category, OverrideNativeName="GetHighScore"))
	virtual FText  bpf__GetHighScore__pf();
public:
	virtual void GetSlotNames(TArray<FName>& SlotNames) const override;
	virtual void PreSave(const class ITargetPlatform* TargetPlatform) override;
	virtual void InitializeNativeClassData() override;
};

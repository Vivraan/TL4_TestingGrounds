#include "ABP_TP__pf117350442.h"
#include "GeneratedCodeHelpers.h"
#include "Animation/BlendProfile.h"
#include "Runtime/Engine/Classes/Animation/Skeleton.h"
#include "Runtime/Engine/Classes/GameFramework/Pawn.h"
#include "Runtime/Engine/Classes/GameFramework/Actor.h"
#include "Runtime/CoreUObject/Public/UObject/NoExportTypes.h"
#include "Runtime/Engine/Classes/Engine/EngineBaseTypes.h"
#include "Runtime/Engine/Classes/Engine/EngineTypes.h"
#include "Runtime/Engine/Classes/Engine/NetSerialization.h"
#include "Runtime/Engine/Classes/Components/SceneComponent.h"
#include "Runtime/Engine/Classes/Components/ActorComponent.h"
#include "Runtime/Engine/Classes/Engine/AssetUserData.h"
#include "Runtime/Engine/Classes/EdGraph/EdGraphPin.h"
#include "Runtime/Engine/Classes/Interfaces/Interface_AssetUserData.h"
#include "Runtime/Engine/Classes/GameFramework/PhysicsVolume.h"
#include "Runtime/Engine/Classes/GameFramework/Volume.h"
#include "Runtime/Engine/Classes/Engine/Brush.h"
#include "Runtime/Engine/Classes/Components/BrushComponent.h"
#include "Runtime/Engine/Classes/Components/PrimitiveComponent.h"
#include "Runtime/Engine/Classes/PhysicsEngine/BodyInstance.h"
#include "Runtime/Engine/Classes/PhysicalMaterials/PhysicalMaterial.h"
#include "Runtime/Engine/Classes/PhysicsEngine/PhysicsSettingsEnums.h"
#include "Runtime/Engine/Classes/PhysicalMaterials/PhysicalMaterialPropertyBase.h"
#include "Runtime/Engine/Classes/Vehicles/TireType.h"
#include "Runtime/Engine/Classes/Engine/DataAsset.h"
#include "Runtime/InputCore/Classes/InputCoreTypes.h"
#include "Runtime/Engine/Classes/Materials/MaterialInterface.h"
#include "Runtime/Engine/Classes/Engine/SubsurfaceProfile.h"
#include "Runtime/Engine/Classes/Materials/Material.h"
#include "Runtime/Engine/Public/MaterialShared.h"
#include "Runtime/Engine/Classes/Materials/MaterialExpression.h"
#include "Runtime/Engine/Classes/Materials/MaterialFunction.h"
#include "Runtime/Engine/Classes/Materials/MaterialFunctionInterface.h"
#include "Runtime/Engine/Classes/Materials/MaterialParameterCollection.h"
#include "Runtime/Engine/Classes/Engine/BlendableInterface.h"
#include "Runtime/Engine/Classes/Engine/Texture.h"
#include "Runtime/Engine/Classes/Engine/TextureDefines.h"
#include "Runtime/Engine/Classes/Materials/MaterialInstanceDynamic.h"
#include "Runtime/Engine/Classes/Materials/MaterialInstance.h"
#include "Runtime/Engine/Classes/Materials/MaterialLayersFunctions.h"
#include "Runtime/Engine/Classes/Engine/Font.h"
#include "Runtime/Engine/Classes/Engine/Texture2D.h"
#include "Runtime/Engine/Classes/Engine/FontImportOptions.h"
#include "Runtime/SlateCore/Public/Fonts/CompositeFont.h"
#include "Runtime/SlateCore/Public/Fonts/FontProviderInterface.h"
#include "Runtime/Engine/Classes/Materials/MaterialInstanceBasePropertyOverrides.h"
#include "Runtime/Engine/Public/StaticParameterSet.h"
#include "Runtime/Engine/Classes/AI/Navigation/NavRelevantInterface.h"
#include "Runtime/Engine/Classes/PhysicsEngine/BodySetup.h"
#include "Runtime/Engine/Classes/PhysicsEngine/AggregateGeom.h"
#include "Runtime/Engine/Classes/PhysicsEngine/SphereElem.h"
#include "Runtime/Engine/Classes/PhysicsEngine/ShapeElem.h"
#include "Runtime/Engine/Classes/PhysicsEngine/BoxElem.h"
#include "Runtime/Engine/Classes/PhysicsEngine/SphylElem.h"
#include "Runtime/Engine/Classes/PhysicsEngine/ConvexElem.h"
#include "Runtime/Engine/Classes/PhysicsEngine/TaperedCapsuleElem.h"
#include "Runtime/Engine/Classes/PhysicsEngine/BodySetupEnums.h"
#include "Runtime/Engine/Classes/Components/InputComponent.h"
#include "Runtime/Engine/Classes/GameFramework/PlayerInput.h"
#include "Runtime/Engine/Classes/GameFramework/PlayerController.h"
#include "Runtime/Engine/Classes/GameFramework/Controller.h"
#include "Runtime/Engine/Classes/GameFramework/PlayerState.h"
#include "Runtime/Engine/Classes/GameFramework/Info.h"
#include "Runtime/Engine/Classes/Components/BillboardComponent.h"
#include "Runtime/Engine/Classes/GameFramework/LocalMessage.h"
#include "Runtime/Engine/Classes/GameFramework/OnlineReplStructs.h"
#include "Runtime/CoreUObject/Public/UObject/CoreOnline.h"
#include "Runtime/Engine/Classes/GameFramework/EngineMessage.h"
#include "Runtime/Engine/Classes/GameFramework/DamageType.h"
#include "Runtime/Engine/Classes/GameFramework/Character.h"
#include "Runtime/Engine/Classes/Components/SkeletalMeshComponent.h"
#include "Runtime/Engine/Classes/Components/SkinnedMeshComponent.h"
#include "Runtime/Engine/Classes/Components/MeshComponent.h"
#include "Runtime/Engine/Classes/Engine/SkeletalMesh.h"
#include "Runtime/Engine/Classes/Engine/SkeletalMeshSocket.h"
#include "Runtime/Engine/Classes/Animation/SmartName.h"
#include "Runtime/Engine/Classes/Animation/BlendProfile.h"
#include "Runtime/Engine/Public/BoneContainer.h"
#include "Runtime/Engine/Classes/Interfaces/Interface_PreviewMeshProvider.h"
#include "Runtime/Engine/Public/Components.h"
#include "Runtime/Engine/Public/PerPlatformProperties.h"
#include "Runtime/Engine/Public/SkeletalMeshReductionSettings.h"
#include "Runtime/Engine/Classes/Animation/AnimSequence.h"
#include "Runtime/Engine/Classes/Animation/AnimSequenceBase.h"
#include "Runtime/Engine/Classes/Animation/AnimationAsset.h"
#include "Runtime/Engine/Classes/Animation/AnimMetaData.h"
#include "Runtime/Engine/Public/Animation/AnimTypes.h"
#include "Runtime/Engine/Classes/Animation/AnimLinkableElement.h"
#include "Runtime/Engine/Classes/Animation/AnimMontage.h"
#include "Runtime/Engine/Classes/Animation/AnimCompositeBase.h"
#include "Runtime/Engine/Public/AlphaBlend.h"
#include "Runtime/Engine/Classes/Curves/CurveFloat.h"
#include "Runtime/Engine/Classes/Curves/CurveBase.h"
#include "Runtime/Engine/Classes/Curves/RichCurve.h"
#include "Runtime/Engine/Classes/Curves/IndexedCurve.h"
#include "Runtime/Engine/Classes/Curves/KeyHandle.h"
#include "Runtime/Engine/Classes/Animation/AnimEnums.h"
#include "Runtime/Engine/Classes/Animation/TimeStretchCurve.h"
#include "Runtime/Engine/Classes/Animation/AnimNotifies/AnimNotify.h"
#include "Runtime/Engine/Classes/Animation/AnimNotifies/AnimNotifyState.h"
#include "Runtime/Engine/Public/Animation/AnimCurveTypes.h"
#include "Runtime/Engine/Classes/PhysicsEngine/PhysicsAsset.h"
#include "Runtime/Engine/Classes/PhysicsEngine/PhysicalAnimationComponent.h"
#include "Runtime/Engine/Classes/PhysicsEngine/PhysicsConstraintTemplate.h"
#include "Runtime/Engine/Classes/PhysicsEngine/ConstraintInstance.h"
#include "Runtime/Engine/Classes/PhysicsEngine/ConstraintTypes.h"
#include "Runtime/Engine/Classes/PhysicsEngine/ConstraintDrives.h"
#include "Runtime/Engine/Classes/EditorFramework/ThumbnailInfo.h"
#include "Runtime/Engine/Public/Animation/NodeMappingContainer.h"
#include "Runtime/Engine/Public/Animation/NodeMappingProviderInterface.h"
#include "Runtime/Engine/Classes/Animation/MorphTarget.h"
#include "Runtime/Engine/Public/Animation/AnimNotifyQueue.h"
#include "Runtime/Engine/Public/Animation/PoseSnapshot.h"
#include "Runtime/ClothingSystemRuntimeInterface/Public/ClothingAssetInterface.h"
#include "Runtime/Engine/Classes/Engine/SkeletalMeshSampling.h"
#include "Runtime/Engine/Classes/Engine/SkeletalMeshLODSettings.h"
#include "Runtime/Engine/Classes/Engine/Blueprint.h"
#include "Runtime/Engine/Classes/Engine/BlueprintCore.h"
#include "Runtime/Engine/Classes/Engine/SimpleConstructionScript.h"
#include "Runtime/Engine/Classes/Engine/SCS_Node.h"
#include "Runtime/Engine/Classes/Engine/BlueprintGeneratedClass.h"
#include "Runtime/Engine/Classes/Engine/TimelineTemplate.h"
#include "Runtime/Engine/Classes/Components/TimelineComponent.h"
#include "Runtime/Engine/Classes/Curves/CurveVector.h"
#include "Runtime/Engine/Classes/Curves/CurveLinearColor.h"
#include "Runtime/Engine/Classes/Engine/InheritableComponentHandler.h"
#include "Runtime/Engine/Classes/Interfaces/Interface_CollisionDataProvider.h"
#include "Runtime/Engine/Classes/Animation/AnimBlueprintGeneratedClass.h"
#include "Runtime/Engine/Classes/Engine/DynamicBlueprintBinding.h"
#include "Runtime/Engine/Classes/Animation/AnimStateMachineTypes.h"
#include "Runtime/Engine/Classes/Animation/AnimClassInterface.h"
#include "Runtime/Engine/Public/SingleAnimationPlayData.h"
#include "Runtime/ClothingSystemRuntimeInterface/Public/ClothingSimulationFactoryInterface.h"
#include "Runtime/ClothingSystemRuntimeInterface/Public/ClothingSimulationInteractor.h"
#include "Runtime/Engine/Classes/GameFramework/CharacterMovementComponent.h"
#include "Runtime/Engine/Classes/GameFramework/PawnMovementComponent.h"
#include "Runtime/Engine/Classes/GameFramework/NavMovementComponent.h"
#include "Runtime/Engine/Classes/GameFramework/MovementComponent.h"
#include "Runtime/Engine/Classes/AI/Navigation/NavigationTypes.h"
#include "Runtime/Engine/Classes/AI/Navigation/NavigationAvoidanceTypes.h"
#include "Runtime/Engine/Classes/GameFramework/RootMotionSource.h"
#include "Runtime/Engine/Public/AI/RVOAvoidanceInterface.h"
#include "Runtime/Engine/Classes/Interfaces/NetworkPredictionInterface.h"
#include "Runtime/Engine/Classes/Components/CapsuleComponent.h"
#include "Runtime/Engine/Classes/Components/ShapeComponent.h"
#include "Runtime/Engine/Classes/AI/Navigation/NavAreaBase.h"
#include "Runtime/AIModule/Classes/AIController.h"
#include "Runtime/AIModule/Classes/Navigation/PathFollowingComponent.h"
#include "Runtime/NavigationSystem/Public/NavigationData.h"
#include "Runtime/Engine/Classes/AI/Navigation/NavigationDataInterface.h"
#include "Runtime/AIModule/Classes/AIResourceInterface.h"
#include "Runtime/Engine/Classes/AI/Navigation/PathFollowingAgentInterface.h"
#include "Runtime/AIModule/Classes/BrainComponent.h"
#include "Runtime/AIModule/Classes/BehaviorTree/BlackboardComponent.h"
#include "Runtime/AIModule/Classes/BehaviorTree/BlackboardData.h"
#include "Runtime/AIModule/Classes/BehaviorTree/Blackboard/BlackboardKeyType.h"
#include "Runtime/AIModule/Classes/Perception/AIPerceptionComponent.h"
#include "Runtime/AIModule/Classes/Perception/AISenseConfig.h"
#include "Runtime/AIModule/Classes/Perception/AISense.h"
#include "Runtime/AIModule/Classes/Perception/AIPerceptionTypes.h"
#include "Runtime/AIModule/Classes/Perception/AIPerceptionSystem.h"
#include "Runtime/AIModule/Classes/Perception/AISenseEvent.h"
#include "Runtime/AIModule/Classes/Actions/PawnActionsComponent.h"
#include "Runtime/AIModule/Classes/Actions/PawnAction.h"
#include "Runtime/AIModule/Classes/AITypes.h"
#include "Runtime/GameplayTasks/Classes/GameplayTasksComponent.h"
#include "Runtime/GameplayTasks/Classes/GameplayTask.h"
#include "Runtime/GameplayTasks/Classes/GameplayTaskOwnerInterface.h"
#include "Runtime/GameplayTasks/Classes/GameplayTaskResource.h"
#include "Runtime/NavigationSystem/Public/NavFilters/NavigationQueryFilter.h"
#include "Runtime/NavigationSystem/Public/NavAreas/NavArea.h"
#include "Runtime/AIModule/Classes/BehaviorTree/BehaviorTree.h"
#include "Runtime/AIModule/Classes/BehaviorTree/BTCompositeNode.h"
#include "Runtime/AIModule/Classes/BehaviorTree/BTNode.h"
#include "Runtime/AIModule/Classes/BehaviorTree/BTTaskNode.h"
#include "Runtime/AIModule/Classes/BehaviorTree/BTService.h"
#include "Runtime/AIModule/Classes/BehaviorTree/BTAuxiliaryNode.h"
#include "Runtime/AIModule/Classes/BehaviorTree/BTDecorator.h"
#include "Runtime/AIModule/Classes/BehaviorTree/BehaviorTreeTypes.h"
#include "Runtime/AIModule/Classes/Perception/AIPerceptionListenerInterface.h"
#include "Runtime/AIModule/Classes/GenericTeamAgentInterface.h"
#include "Runtime/Engine/Public/VisualLogger/VisualLoggerDebugSnapshotInterface.h"
#include "Runtime/Engine/Classes/AI/Navigation/NavAgentInterface.h"
#include "Runtime/Engine/Classes/Engine/Player.h"
#include "Runtime/Engine/Classes/Matinee/InterpTrackInstDirector.h"
#include "Runtime/Engine/Classes/Matinee/InterpTrackInst.h"
#include "Runtime/Engine/Classes/GameFramework/HUD.h"
#include "Runtime/Engine/Classes/Engine/Canvas.h"
#include "Runtime/Engine/Classes/Debug/ReporterGraph.h"
#include "Runtime/Engine/Classes/Debug/ReporterBase.h"
#include "Runtime/Engine/Classes/GameFramework/DebugTextInfo.h"
#include "Runtime/Engine/Classes/Camera/PlayerCameraManager.h"
#include "Runtime/Engine/Classes/Camera/CameraTypes.h"
#include "Runtime/Engine/Classes/Engine/Scene.h"
#include "Runtime/Engine/Classes/Engine/TextureCube.h"
#include "Runtime/Engine/Classes/Camera/CameraModifier.h"
#include "Runtime/Engine/Classes/Particles/EmitterCameraLensEffectBase.h"
#include "Runtime/Engine/Classes/Particles/Emitter.h"
#include "Runtime/Engine/Classes/Particles/ParticleSystemComponent.h"
#include "Runtime/Engine/Classes/Particles/ParticleSystem.h"
#include "Runtime/Engine/Classes/Particles/ParticleEmitter.h"
#include "Runtime/Engine/Public/ParticleHelper.h"
#include "Runtime/Engine/Classes/Particles/ParticleLODLevel.h"
#include "Runtime/Engine/Classes/Particles/ParticleModuleRequired.h"
#include "Runtime/Engine/Classes/Particles/ParticleModule.h"
#include "Runtime/Engine/Classes/Particles/ParticleSpriteEmitter.h"
#include "Runtime/Engine/Classes/Distributions/DistributionFloat.h"
#include "Runtime/Engine/Classes/Distributions/Distribution.h"
#include "Runtime/Engine/Classes/Particles/SubUVAnimation.h"
#include "Runtime/Engine/Classes/Particles/TypeData/ParticleModuleTypeDataBase.h"
#include "Runtime/Engine/Classes/Particles/Spawn/ParticleModuleSpawn.h"
#include "Runtime/Engine/Classes/Particles/Spawn/ParticleModuleSpawnBase.h"
#include "Runtime/Engine/Classes/Particles/Event/ParticleModuleEventGenerator.h"
#include "Runtime/Engine/Classes/Particles/Event/ParticleModuleEventBase.h"
#include "Runtime/Engine/Classes/Particles/Event/ParticleModuleEventSendToGame.h"
#include "Runtime/Engine/Classes/Particles/Orbit/ParticleModuleOrbit.h"
#include "Runtime/Engine/Classes/Particles/Orbit/ParticleModuleOrbitBase.h"
#include "Runtime/Engine/Classes/Distributions/DistributionVector.h"
#include "Runtime/Engine/Classes/Particles/Event/ParticleModuleEventReceiverBase.h"
#include "Runtime/Engine/Classes/Engine/InterpCurveEdSetup.h"
#include "Runtime/Engine/Classes/Particles/ParticleSystemReplay.h"
#include "Runtime/Engine/Classes/Camera/CameraModifier_CameraShake.h"
#include "Runtime/Engine/Classes/Camera/CameraShake.h"
#include "Runtime/Engine/Classes/Camera/CameraAnim.h"
#include "Runtime/Engine/Classes/Matinee/InterpGroup.h"
#include "Runtime/Engine/Classes/Matinee/InterpTrack.h"
#include "Runtime/Engine/Classes/Camera/CameraAnimInst.h"
#include "Runtime/Engine/Classes/Matinee/InterpGroupInst.h"
#include "Runtime/Engine/Classes/Matinee/InterpTrackMove.h"
#include "Runtime/Engine/Classes/Matinee/InterpTrackInstMove.h"
#include "Runtime/Engine/Classes/Camera/CameraActor.h"
#include "Runtime/Engine/Classes/Camera/CameraComponent.h"
#include "Runtime/Engine/Classes/GameFramework/CheatManager.h"
#include "Runtime/Engine/Classes/Engine/DebugCameraController.h"
#include "Runtime/Engine/Classes/Components/DrawFrustumComponent.h"
#include "Runtime/Engine/Classes/GameFramework/ForceFeedbackEffect.h"
#include "Runtime/Engine/Classes/Engine/NetConnection.h"
#include "Runtime/Engine/Classes/Engine/ChildConnection.h"
#include "Runtime/Engine/Classes/Engine/PackageMapClient.h"
#include "Runtime/Engine/Classes/Engine/NetDriver.h"
#include "Runtime/Engine/Classes/Engine/World.h"
#include "Runtime/Engine/Classes/Engine/Level.h"
#include "Runtime/Engine/Classes/Components/ModelComponent.h"
#include "Runtime/Engine/Classes/Engine/LevelActorContainer.h"
#include "Runtime/Engine/Classes/Engine/LevelScriptActor.h"
#include "Runtime/Engine/Classes/Engine/NavigationObjectBase.h"
#include "Runtime/Engine/Classes/AI/Navigation/NavigationDataChunk.h"
#include "Runtime/Engine/Classes/Engine/MapBuildDataRegistry.h"
#include "Runtime/Engine/Classes/GameFramework/WorldSettings.h"
#include "Runtime/Engine/Classes/AI/NavigationSystemConfig.h"
#include "Runtime/Engine/Classes/GameFramework/DefaultPhysicsVolume.h"
#include "Runtime/Engine/Classes/PhysicsEngine/PhysicsCollisionHandler.h"
#include "Runtime/Engine/Classes/Sound/SoundBase.h"
#include "Runtime/Engine/Classes/Sound/SoundClass.h"
#include "Runtime/Engine/Classes/Sound/SoundMix.h"
#include "Runtime/Engine/Classes/Sound/SoundConcurrency.h"
#include "Runtime/Engine/Classes/Sound/SoundAttenuation.h"
#include "Runtime/Engine/Classes/Engine/Attenuation.h"
#include "Runtime/Engine/Public/IAudioExtensionPlugin.h"
#include "Runtime/Engine/Classes/Sound/SoundSubmix.h"
#include "Runtime/Engine/Classes/Sound/SoundEffectSubmix.h"
#include "Runtime/Engine/Classes/Sound/SoundEffectPreset.h"
#include "Runtime/Engine/Public/IAmbisonicsMixer.h"
#include "Runtime/Engine/Classes/Sound/SoundWave.h"
#include "Runtime/AudioPlatformConfiguration/Public/AudioCompressionSettings.h"
#include "Runtime/Engine/Classes/Sound/SoundGroups.h"
#include "Runtime/Engine/Classes/Engine/CurveTable.h"
#include "Runtime/Engine/Classes/Sound/SoundEffectSource.h"
#include "Runtime/Engine/Classes/Sound/SoundSourceBusSend.h"
#include "Runtime/Engine/Classes/Sound/SoundSourceBus.h"
#include "Runtime/Engine/Classes/GameFramework/GameModeBase.h"
#include "Runtime/Engine/Classes/GameFramework/GameSession.h"
#include "Runtime/Engine/Classes/GameFramework/GameStateBase.h"
#include "Runtime/Engine/Classes/GameFramework/SpectatorPawn.h"
#include "Runtime/Engine/Classes/GameFramework/DefaultPawn.h"
#include "Runtime/Engine/Classes/Components/SphereComponent.h"
#include "Runtime/Engine/Classes/Components/StaticMeshComponent.h"
#include "Runtime/Engine/Classes/Engine/StaticMesh.h"
#include "Runtime/Engine/Classes/Engine/StaticMeshSocket.h"
#include "Runtime/Engine/Classes/AI/Navigation/NavCollisionBase.h"
#include "Runtime/Engine/Classes/Engine/TextureStreamingTypes.h"
#include "Runtime/Engine/Classes/GameFramework/FloatingPawnMovement.h"
#include "Runtime/Engine/Classes/GameFramework/SpectatorPawnMovement.h"
#include "Runtime/Engine/Classes/Engine/ServerStatReplicator.h"
#include "Runtime/Engine/Classes/GameFramework/GameNetworkManager.h"
#include "Runtime/Engine/Classes/Sound/AudioVolume.h"
#include "Runtime/Engine/Classes/Sound/ReverbEffect.h"
#include "Runtime/Engine/Classes/Engine/BookMark.h"
#include "Runtime/Engine/Classes/Engine/BookmarkBase.h"
#include "Runtime/Engine/Classes/Components/LineBatchComponent.h"
#include "Runtime/Engine/Classes/Engine/LevelStreaming.h"
#include "Runtime/Engine/Classes/Engine/LevelStreamingVolume.h"
#include "Runtime/Engine/Classes/Engine/DemoNetDriver.h"
#include "Runtime/Engine/Classes/Particles/ParticleEventManager.h"
#include "Runtime/Engine/Classes/AI/NavigationSystemBase.h"
#include "Runtime/Engine/Classes/AI/AISystemBase.h"
#include "Runtime/Engine/Classes/AI/Navigation/AvoidanceManager.h"
#include "Runtime/Engine/Classes/Engine/GameInstance.h"
#include "Runtime/Engine/Classes/Engine/LocalPlayer.h"
#include "Runtime/Engine/Classes/Engine/GameViewportClient.h"
#include "Runtime/Engine/Classes/Engine/ScriptViewportClient.h"
#include "Runtime/Engine/Classes/Engine/Console.h"
#include "Runtime/Engine/Classes/Engine/DebugDisplayProperty.h"
#include "Runtime/Engine/Classes/Engine/Engine.h"
#include "Runtime/Engine/Classes/GameFramework/GameUserSettings.h"
#include "Runtime/Engine/Classes/Engine/AssetManager.h"
#include "Runtime/Engine/Classes/Engine/EngineCustomTimeStep.h"
#include "Runtime/Engine/Classes/Engine/TimecodeProvider.h"
#include "Runtime/Engine/Classes/GameFramework/OnlineSession.h"
#include "Runtime/Engine/Classes/Materials/MaterialParameterCollectionInstance.h"
#include "Runtime/Engine/Classes/Engine/WorldComposition.h"
#include "Runtime/Engine/Classes/Particles/WorldPSCPool.h"
#include "Runtime/Engine/Classes/Engine/Channel.h"
#include "Runtime/Engine/Classes/Engine/ReplicationDriver.h"
#include "Runtime/Engine/Classes/GameFramework/TouchInterface.h"
#include "Runtime/UMG/Public/Blueprint/UserWidget.h"
#include "Runtime/UMG/Public/Components/Widget.h"
#include "Runtime/UMG/Public/Components/Visual.h"
#include "Runtime/UMG/Public/Components/PanelSlot.h"
#include "Runtime/UMG/Public/Components/PanelWidget.h"
#include "Runtime/UMG/Public/Components/SlateWrapperTypes.h"
#include "Runtime/UMG/Public/Slate/WidgetTransform.h"
#include "Runtime/SlateCore/Public/Layout/Clipping.h"
#include "Runtime/UMG/Public/Blueprint/WidgetNavigation.h"
#include "Runtime/SlateCore/Public/Input/NavigationReply.h"
#include "Runtime/SlateCore/Public/Types/SlateEnums.h"
#include "Runtime/UMG/Public/Binding/PropertyBinding.h"
#include "Runtime/UMG/Public/Binding/DynamicPropertyPath.h"
#include "Runtime/PropertyPath/Public/PropertyPathHelpers.h"
#include "Runtime/SlateCore/Public/Layout/Geometry.h"
#include "Runtime/SlateCore/Public/Input/Events.h"
#include "Runtime/SlateCore/Public/Styling/SlateColor.h"
#include "Runtime/SlateCore/Public/Styling/SlateBrush.h"
#include "Runtime/SlateCore/Public/Layout/Margin.h"
#include "Runtime/SlateCore/Public/Styling/SlateTypes.h"
#include "Runtime/UMG/Public/Animation/UMGSequencePlayer.h"
#include "Runtime/UMG/Public/Animation/WidgetAnimation.h"
#include "Runtime/MovieScene/Public/MovieSceneSequence.h"
#include "Runtime/MovieScene/Public/MovieSceneSignedObject.h"
#include "Runtime/MovieScene/Public/Evaluation/MovieSceneEvaluationTemplate.h"
#include "Runtime/MovieScene/Public/Evaluation/MovieSceneTrackIdentifier.h"
#include "Runtime/MovieScene/Public/Evaluation/MovieSceneEvaluationTrack.h"
#include "Runtime/MovieScene/Public/Evaluation/MovieSceneSegment.h"
#include "Runtime/MovieScene/Public/MovieSceneTrack.h"
#include "Runtime/MovieScene/Public/Evaluation/MovieSceneEvalTemplate.h"
#include "Runtime/MovieScene/Public/Evaluation/MovieSceneTrackImplementation.h"
#include "Runtime/MovieScene/Public/Evaluation/MovieSceneEvaluationField.h"
#include "Runtime/MovieScene/Public/MovieSceneFrameMigration.h"
#include "Runtime/MovieScene/Public/MovieSceneSequenceID.h"
#include "Runtime/MovieScene/Public/Evaluation/MovieSceneEvaluationKey.h"
#include "Runtime/MovieScene/Public/Evaluation/MovieSceneSequenceHierarchy.h"
#include "Runtime/MovieScene/Public/Evaluation/MovieSceneSequenceTransform.h"
#include "Runtime/MovieScene/Public/Evaluation/MovieSceneSequenceInstanceData.h"
#include "Runtime/MovieScene/Public/MovieSceneSection.h"
#include "Runtime/MovieScene/Public/MovieScene.h"
#include "Runtime/MovieScene/Public/MovieSceneSpawnable.h"
#include "Runtime/MovieScene/Public/MovieScenePossessable.h"
#include "Runtime/MovieScene/Public/MovieSceneBinding.h"
#include "Runtime/MovieScene/Public/MovieSceneFwd.h"
#include "Runtime/UMG/Public/Animation/WidgetAnimationBinding.h"
#include "Runtime/UMG/Public/Blueprint/WidgetTree.h"
#include "Runtime/Slate/Public/Widgets/Layout/Anchors.h"
#include "Runtime/UMG/Public/Blueprint/DragDropOperation.h"
#include "Runtime/UMG/Public/Components/NamedSlotInterface.h"
#include "Runtime/Engine/Classes/Haptics/HapticFeedbackEffect_Base.h"
#include "Runtime/Engine/Classes/Engine/LatentActionManager.h"
#include "Runtime/Engine/Classes/Matinee/MatineeActor.h"
#include "Runtime/Engine/Classes/Matinee/InterpData.h"
#include "Runtime/Engine/Classes/Matinee/InterpGroupDirector.h"
#include "Runtime/Engine/Classes/Components/ChildActorComponent.h"
#include "BP_TL4Character__pf3043094308.h"
#include "Runtime/Engine/Classes/Kismet/KismetMathLibrary.h"
#include "Runtime/Engine/Classes/Kismet/BlueprintFunctionLibrary.h"
#include "Runtime/Engine/Classes/Animation/AnimNodeBase.h"
#include "Runtime/Engine/Classes/Engine/InputAxisDelegateBinding.h"
#include "Runtime/Engine/Classes/Engine/InputDelegateBinding.h"
#include "Runtime/Engine/Classes/Engine/InputKeyDelegateBinding.h"
#include "Runtime/Slate/Public/Framework/Commands/InputChord.h"
#include "Runtime/Engine/Classes/Engine/InputActionDelegateBinding.h"
#include "WBP_Resetting__pf3389175594.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Runtime/Engine/Classes/Components/AudioComponent.h"
#include "Runtime/Engine/Classes/GameFramework/ForceFeedbackAttenuation.h"
#include "Runtime/Engine/Classes/Components/ForceFeedbackComponent.h"
#include "Runtime/Engine/Classes/Sound/DialogueWave.h"
#include "Runtime/Engine/Classes/Sound/DialogueTypes.h"
#include "Runtime/Engine/Classes/Sound/DialogueVoice.h"
#include "Runtime/Engine/Classes/Sound/DialogueSoundWaveProxy.h"
#include "Runtime/Engine/Classes/Components/DecalComponent.h"
#include "Runtime/Engine/Classes/GameFramework/SaveGame.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStaticsTypes.h"
#include "Runtime/Engine/Classes/Kismet/KismetSystemLibrary.h"
#include "Runtime/UMG/Public/Blueprint/WidgetBlueprintLibrary.h"
#include "Runtime/Engine/Public/Slate/SGameLayerManager.h"
#include "Runtime/Engine/Classes/Slate/SlateBrushAsset.h"
#include "Runtime/Engine/Classes/Kismet/KismetStringLibrary.h"
#include "Runtime/Engine/Classes/Components/PostProcessComponent.h"
#include "Runtime/Engine/Classes/Interfaces/Interface_PostProcessVolume.h"
#include "TL4_TestingGrounds/TL4Character/Mannequin.h"
#include "TL4_TestingGrounds/Weapons/Gun.h"
#include "TL4_TestingGrounds/Weapons/BallProjectile.h"
#include "Runtime/Engine/Classes/GameFramework/ProjectileMovementComponent.h"
#include "WBP_GameOver__pf3389175594.h"
#include "Runtime/Engine/Classes/Components/ArrowComponent.h"
#include "Runtime/Engine/Classes/Engine/CollisionProfile.h"
#include "Runtime/Engine/Classes/Components/BoxComponent.h"
#include "TL4_TestingGrounds/NPC/PatrolRoute.h"
#include "BP_NPC_AI__pf2310560554.h"
#include "Runtime/Engine/Classes/Animation/AnimNode_AssetPlayerBase.h"
#include "Runtime/Engine/Classes/Animation/BlendSpaceBase.h"
#include "Runtime/Engine/Classes/Animation/InputScaleBias.h"
#include "Runtime/AnimGraphRuntime/Public/AnimNodes/AnimNode_BlendListBase.h"
#include "Runtime/AnimGraphRuntime/Public/BoneControllers/AnimNode_SkeletalControlBase.h"
#include "Runtime/AnimationCore/Public/CommonAnimTypes.h"
#include "Runtime/Engine/Classes/Animation/AnimData/BoneMaskFilter.h"
#include "Runtime/Engine/Classes/Animation/BlendSpace.h"
#include "Runtime/Engine/Classes/Animation/AimOffsetBlendSpace1D.h"


#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
PRAGMA_DISABLE_OPTIMIZATION
UABP_TP_C__pf117350442::UABP_TP_C__pf117350442(const FObjectInitializer& ObjectInitializer) : Super()
{
	if(HasAnyFlags(RF_ClassDefaultObject) && (UABP_TP_C__pf117350442::StaticClass() == GetClass()))
	{
		UABP_TP_C__pf117350442::__CustomDynamicClassInitialization(CastChecked<UDynamicClass>(GetClass()));
	}
	
	__InitAllAnimNodes();
	bpv__Speed__pf = 0.000000f;
	bpv__Direction__pf = 0.000000f;
	bpv__EnablexJump__pfT = false;
	bpv__Jumping__pf = false;
	bpv__Crouching__pf = false;
	bpv__AimAngle__pf = 0.000000f;
	bpv__Aiming__pf = false;
	bpv__IsxDead__pfT = false;
}
PRAGMA_ENABLE_OPTIMIZATION
void UABP_TP_C__pf117350442::__InitAllAnimNodes()
{
	__InitAnimNode__AnimGraphNode_Root_0C0304664E5D637E777F8F82B80BDD47();
	__InitAnimNode__AnimGraphNode_TransitionResult_4AAD4A594EABD4F92E1D44BBD3F4830B();
	__InitAnimNode__AnimGraphNode_TransitionResult_78C3746B4C10022D609AD6A27618D2ED();
	__InitAnimNode__AnimGraphNode_TransitionResult_58EDEBE34160C294191EBFAB9F63A3B7();
	__InitAnimNode__AnimGraphNode_TransitionResult_F13AB6764EBE8335385B039A555B9993();
	__InitAnimNode__AnimGraphNode_TransitionResult_23C30FC54F432C27A8D2E086D1A0B4F4();
	__InitAnimNode__AnimGraphNode_TransitionResult_FFFCF0144598EDEE64EE439BD7BDAFDF();
	__InitAnimNode__AnimGraphNode_TransitionResult_5540D45C4BCA55AEC05DA1A1E21BFB58();
	__InitAnimNode__AnimGraphNode_TransitionResult_E2FA68D046B28AF86B5CBAA099FA056F();
	__InitAnimNode__AnimGraphNode_TransitionResult_315E9BF04EA579C328EB6A866B91C07A();
	__InitAnimNode__AnimGraphNode_TransitionResult_D25429B644E6C66D625827BA5AE5F04E();
	__InitAnimNode__AnimGraphNode_TransitionResult_5C6D350441E998F44093C798BB4C071C();
	__InitAnimNode__AnimGraphNode_TransitionResult_3163F8234400D90A8D5A4494801CFE23();
	__InitAnimNode__AnimGraphNode_BlendSpacePlayer_C6840A414E888EE73F8F539647D22D1E();
	__InitAnimNode__AnimGraphNode_StateResult_4664B17F4CB02F4B6A675C8868CAFEA8();
	__InitAnimNode__AnimGraphNode_SequencePlayer_C2D643BA4C3600B1D9D7D8A75F5F8A1F();
	__InitAnimNode__AnimGraphNode_StateResult_923A2E92452270AE79162487AF01084E();
	__InitAnimNode__AnimGraphNode_BlendListByBool_190BB93E435681C335E326B9FFD454C7();
	__InitAnimNode__AnimGraphNode_SequencePlayer_C9E7B9F5420E8D13B586D0B99107B101();
	__InitAnimNode__AnimGraphNode_SequencePlayer_1C0BEA184A4517BE4A73F6B8B18270A7();
	__InitAnimNode__AnimGraphNode_StateResult_EB35F24F40F27C351EA7A7914C563941();
	__InitAnimNode__AnimGraphNode_SequencePlayer_7CE5CCED48F84FFA8C99C89BAFADD7E9();
	__InitAnimNode__AnimGraphNode_StateResult_8707377A4A7AB494C1B0E98C0818F0AE();
	__InitAnimNode__AnimGraphNode_SequencePlayer_6B905355488C69F57D1302949E0D172D();
	__InitAnimNode__AnimGraphNode_StateResult_2CECE82A42D9A4C2879D88A5ECEE39CB();
	__InitAnimNode__AnimGraphNode_BlendSpacePlayer_1CB238214E41B71CA1F34B95EA74ADA1();
	__InitAnimNode__AnimGraphNode_BlendListByBool_43889F2643908C49EFAE0D8D7EB4CCC0();
	__InitAnimNode__AnimGraphNode_BlendSpacePlayer_1995CFFC4E4C2C5D00C8549266C69DEE();
	__InitAnimNode__AnimGraphNode_StateResult_5925BA954F8CE550AF7F2186CB70990D();
	__InitAnimNode__AnimGraphNode_StateMachine_AD21F2574F606D48D79179B2C8B9494C();
	__InitAnimNode__AnimGraphNode_RotationOffsetBlendSpace_AEA90FFA4CA2B79C913CE8860FF4295A();
	__InitAnimNode__AnimGraphNode_TwoBoneIK_111D181F4D478FDDAF41D68A861FC312();
	__InitAnimNode__AnimGraphNode_LocalToComponentSpace_2DCCCE774E2909C36E5D9FB62C8CA8AB();
	__InitAnimNode__AnimGraphNode_ComponentToLocalSpace_72D9FB7049B2FDBC82442B84EDB8401A();
	__InitAnimNode__AnimGraphNode_BlendListByBool_50D1A82A4333BC39A9A45E961A45F789();
	__InitAnimNode__AnimGraphNode_SequencePlayer_96134A414BCA0FA7562755AF6E8B0FAF();
	__InitAnimNode__AnimGraphNode_LayeredBoneBlend_1EBB97F049A58A8C49417BBCCDCB127C();
	__InitAnimNode__AnimGraphNode_SaveCachedPose_4E344B114BA23C91AF76CA82B09BCF3F();
	__InitAnimNode__AnimGraphNode_UseCachedPose_228FCBE0403B02D29A99AF8FCECD2598();
	__InitAnimNode__AnimGraphNode_Slot_8C801B6C4B22962DD963CB948A9E9169();
	__InitAnimNode__AnimGraphNode_UseCachedPose_C94056184C8FA2A45BD06883D0526F36();
}
void UABP_TP_C__pf117350442::__InitAnimNode__AnimGraphNode_Root_0C0304664E5D637E777F8F82B80BDD47()
{
	bpv__AnimGraphNode_Root_0C0304664E5D637E777F8F82B80BDD47__pf.Result.LinkID = 34;
}
void UABP_TP_C__pf117350442::__InitAnimNode__AnimGraphNode_TransitionResult_4AAD4A594EABD4F92E1D44BBD3F4830B()
{
	bpv__AnimGraphNode_TransitionResult_4AAD4A594EABD4F92E1D44BBD3F4830B__pf.EvaluateGraphExposedInputs.BoundFunction = FName(TEXT("EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_TP_AnimGraphNode_TransitionResult_4AAD4A594EABD4F92E1D44BBD3F4830B"));
}
void UABP_TP_C__pf117350442::__InitAnimNode__AnimGraphNode_TransitionResult_78C3746B4C10022D609AD6A27618D2ED()
{
	bpv__AnimGraphNode_TransitionResult_78C3746B4C10022D609AD6A27618D2ED__pf.EvaluateGraphExposedInputs.CopyRecords = TArray<FExposedValueCopyRecord> ();
	bpv__AnimGraphNode_TransitionResult_78C3746B4C10022D609AD6A27618D2ED__pf.EvaluateGraphExposedInputs.CopyRecords.AddUninitialized(1);
	FExposedValueCopyRecord::StaticStruct()->InitializeStruct(bpv__AnimGraphNode_TransitionResult_78C3746B4C10022D609AD6A27618D2ED__pf.EvaluateGraphExposedInputs.CopyRecords.GetData(), 1);
	auto& __Local__0 = bpv__AnimGraphNode_TransitionResult_78C3746B4C10022D609AD6A27618D2ED__pf.EvaluateGraphExposedInputs.CopyRecords[0];
	__Local__0.SourcePropertyName = FName(TEXT("Crouching"));
	__Local__0.DestProperty = FindFieldChecked<UBoolProperty>(FAnimNode_TransitionResult::StaticStruct(), TEXT("bCanEnterTransition"));
	__Local__0.Size = 1;
	__Local__0.PostCopyOperation = EPostCopyOperation::LogicalNegateBool;
}
void UABP_TP_C__pf117350442::__InitAnimNode__AnimGraphNode_TransitionResult_58EDEBE34160C294191EBFAB9F63A3B7()
{
	bpv__AnimGraphNode_TransitionResult_58EDEBE34160C294191EBFAB9F63A3B7__pf.EvaluateGraphExposedInputs.CopyRecords = TArray<FExposedValueCopyRecord> ();
	bpv__AnimGraphNode_TransitionResult_58EDEBE34160C294191EBFAB9F63A3B7__pf.EvaluateGraphExposedInputs.CopyRecords.AddUninitialized(1);
	FExposedValueCopyRecord::StaticStruct()->InitializeStruct(bpv__AnimGraphNode_TransitionResult_58EDEBE34160C294191EBFAB9F63A3B7__pf.EvaluateGraphExposedInputs.CopyRecords.GetData(), 1);
	auto& __Local__1 = bpv__AnimGraphNode_TransitionResult_58EDEBE34160C294191EBFAB9F63A3B7__pf.EvaluateGraphExposedInputs.CopyRecords[0];
	__Local__1.SourcePropertyName = FName(TEXT("Crouching"));
	__Local__1.DestProperty = FindFieldChecked<UBoolProperty>(FAnimNode_TransitionResult::StaticStruct(), TEXT("bCanEnterTransition"));
	__Local__1.Size = 1;
	__Local__1.PostCopyOperation = EPostCopyOperation::LogicalNegateBool;
}
void UABP_TP_C__pf117350442::__InitAnimNode__AnimGraphNode_TransitionResult_F13AB6764EBE8335385B039A555B9993()
{
	bpv__AnimGraphNode_TransitionResult_F13AB6764EBE8335385B039A555B9993__pf.EvaluateGraphExposedInputs.BoundFunction = FName(TEXT("EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_TP_AnimGraphNode_TransitionResult_F13AB6764EBE8335385B039A555B9993"));
}
void UABP_TP_C__pf117350442::__InitAnimNode__AnimGraphNode_TransitionResult_23C30FC54F432C27A8D2E086D1A0B4F4()
{
	bpv__AnimGraphNode_TransitionResult_23C30FC54F432C27A8D2E086D1A0B4F4__pf.EvaluateGraphExposedInputs.BoundFunction = FName(TEXT("EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_TP_AnimGraphNode_TransitionResult_23C30FC54F432C27A8D2E086D1A0B4F4"));
}
void UABP_TP_C__pf117350442::__InitAnimNode__AnimGraphNode_TransitionResult_FFFCF0144598EDEE64EE439BD7BDAFDF()
{
	bpv__AnimGraphNode_TransitionResult_FFFCF0144598EDEE64EE439BD7BDAFDF__pf.EvaluateGraphExposedInputs.BoundFunction = FName(TEXT("EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_TP_AnimGraphNode_TransitionResult_FFFCF0144598EDEE64EE439BD7BDAFDF"));
}
void UABP_TP_C__pf117350442::__InitAnimNode__AnimGraphNode_TransitionResult_5540D45C4BCA55AEC05DA1A1E21BFB58()
{
	bpv__AnimGraphNode_TransitionResult_5540D45C4BCA55AEC05DA1A1E21BFB58__pf.EvaluateGraphExposedInputs.BoundFunction = FName(TEXT("EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_TP_AnimGraphNode_TransitionResult_5540D45C4BCA55AEC05DA1A1E21BFB58"));
}
void UABP_TP_C__pf117350442::__InitAnimNode__AnimGraphNode_TransitionResult_E2FA68D046B28AF86B5CBAA099FA056F()
{
	bpv__AnimGraphNode_TransitionResult_E2FA68D046B28AF86B5CBAA099FA056F__pf.EvaluateGraphExposedInputs.CopyRecords = TArray<FExposedValueCopyRecord> ();
	bpv__AnimGraphNode_TransitionResult_E2FA68D046B28AF86B5CBAA099FA056F__pf.EvaluateGraphExposedInputs.CopyRecords.AddUninitialized(1);
	FExposedValueCopyRecord::StaticStruct()->InitializeStruct(bpv__AnimGraphNode_TransitionResult_E2FA68D046B28AF86B5CBAA099FA056F__pf.EvaluateGraphExposedInputs.CopyRecords.GetData(), 1);
	auto& __Local__2 = bpv__AnimGraphNode_TransitionResult_E2FA68D046B28AF86B5CBAA099FA056F__pf.EvaluateGraphExposedInputs.CopyRecords[0];
	__Local__2.SourcePropertyName = FName(TEXT("Crouching"));
	__Local__2.DestProperty = FindFieldChecked<UBoolProperty>(FAnimNode_TransitionResult::StaticStruct(), TEXT("bCanEnterTransition"));
	__Local__2.Size = 1;
}
void UABP_TP_C__pf117350442::__InitAnimNode__AnimGraphNode_TransitionResult_315E9BF04EA579C328EB6A866B91C07A()
{
	bpv__AnimGraphNode_TransitionResult_315E9BF04EA579C328EB6A866B91C07A__pf.EvaluateGraphExposedInputs.BoundFunction = FName(TEXT("EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_TP_AnimGraphNode_TransitionResult_315E9BF04EA579C328EB6A866B91C07A"));
}
void UABP_TP_C__pf117350442::__InitAnimNode__AnimGraphNode_TransitionResult_D25429B644E6C66D625827BA5AE5F04E()
{
	bpv__AnimGraphNode_TransitionResult_D25429B644E6C66D625827BA5AE5F04E__pf.EvaluateGraphExposedInputs.BoundFunction = FName(TEXT("EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_TP_AnimGraphNode_TransitionResult_D25429B644E6C66D625827BA5AE5F04E"));
}
void UABP_TP_C__pf117350442::__InitAnimNode__AnimGraphNode_TransitionResult_5C6D350441E998F44093C798BB4C071C()
{
	bpv__AnimGraphNode_TransitionResult_5C6D350441E998F44093C798BB4C071C__pf.EvaluateGraphExposedInputs.CopyRecords = TArray<FExposedValueCopyRecord> ();
	bpv__AnimGraphNode_TransitionResult_5C6D350441E998F44093C798BB4C071C__pf.EvaluateGraphExposedInputs.CopyRecords.AddUninitialized(1);
	FExposedValueCopyRecord::StaticStruct()->InitializeStruct(bpv__AnimGraphNode_TransitionResult_5C6D350441E998F44093C798BB4C071C__pf.EvaluateGraphExposedInputs.CopyRecords.GetData(), 1);
	auto& __Local__3 = bpv__AnimGraphNode_TransitionResult_5C6D350441E998F44093C798BB4C071C__pf.EvaluateGraphExposedInputs.CopyRecords[0];
	__Local__3.SourcePropertyName = FName(TEXT("Crouching"));
	__Local__3.DestProperty = FindFieldChecked<UBoolProperty>(FAnimNode_TransitionResult::StaticStruct(), TEXT("bCanEnterTransition"));
	__Local__3.Size = 1;
}
void UABP_TP_C__pf117350442::__InitAnimNode__AnimGraphNode_TransitionResult_3163F8234400D90A8D5A4494801CFE23()
{
	bpv__AnimGraphNode_TransitionResult_3163F8234400D90A8D5A4494801CFE23__pf.EvaluateGraphExposedInputs.BoundFunction = FName(TEXT("EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_TP_AnimGraphNode_TransitionResult_3163F8234400D90A8D5A4494801CFE23"));
}
void UABP_TP_C__pf117350442::__InitAnimNode__AnimGraphNode_BlendSpacePlayer_C6840A414E888EE73F8F539647D22D1E()
{
	bpv__AnimGraphNode_BlendSpacePlayer_C6840A414E888EE73F8F539647D22D1E__pf.BlendSpace = CastChecked<UBlendSpaceBase>(CastChecked<UDynamicClass>(UABP_TP_C__pf117350442::StaticClass())->UsedAssets[0], ECastCheckedType::NullAllowed);
	bpv__AnimGraphNode_BlendSpacePlayer_C6840A414E888EE73F8F539647D22D1E__pf.EvaluateGraphExposedInputs.CopyRecords = TArray<FExposedValueCopyRecord> ();
	bpv__AnimGraphNode_BlendSpacePlayer_C6840A414E888EE73F8F539647D22D1E__pf.EvaluateGraphExposedInputs.CopyRecords.AddUninitialized(2);
	FExposedValueCopyRecord::StaticStruct()->InitializeStruct(bpv__AnimGraphNode_BlendSpacePlayer_C6840A414E888EE73F8F539647D22D1E__pf.EvaluateGraphExposedInputs.CopyRecords.GetData(), 2);
	auto& __Local__4 = bpv__AnimGraphNode_BlendSpacePlayer_C6840A414E888EE73F8F539647D22D1E__pf.EvaluateGraphExposedInputs.CopyRecords[0];
	__Local__4.SourcePropertyName = FName(TEXT("Direction"));
	__Local__4.DestProperty = FindFieldChecked<UFloatProperty>(FAnimNode_BlendSpacePlayer::StaticStruct(), TEXT("X"));
	__Local__4.Size = 4;
	auto& __Local__5 = bpv__AnimGraphNode_BlendSpacePlayer_C6840A414E888EE73F8F539647D22D1E__pf.EvaluateGraphExposedInputs.CopyRecords[1];
	__Local__5.SourcePropertyName = FName(TEXT("Speed"));
	__Local__5.DestProperty = FindFieldChecked<UFloatProperty>(FAnimNode_BlendSpacePlayer::StaticStruct(), TEXT("Y"));
	__Local__5.Size = 4;
}
void UABP_TP_C__pf117350442::__InitAnimNode__AnimGraphNode_StateResult_4664B17F4CB02F4B6A675C8868CAFEA8()
{
	bpv__AnimGraphNode_StateResult_4664B17F4CB02F4B6A675C8868CAFEA8__pf.Result.LinkID = 13;
}
void UABP_TP_C__pf117350442::__InitAnimNode__AnimGraphNode_SequencePlayer_C2D643BA4C3600B1D9D7D8A75F5F8A1F()
{
	bpv__AnimGraphNode_SequencePlayer_C2D643BA4C3600B1D9D7D8A75F5F8A1F__pf.Sequence = CastChecked<UAnimSequenceBase>(CastChecked<UDynamicClass>(UABP_TP_C__pf117350442::StaticClass())->UsedAssets[1], ECastCheckedType::NullAllowed);
}
void UABP_TP_C__pf117350442::__InitAnimNode__AnimGraphNode_StateResult_923A2E92452270AE79162487AF01084E()
{
	bpv__AnimGraphNode_StateResult_923A2E92452270AE79162487AF01084E__pf.Result.LinkID = 15;
}
void UABP_TP_C__pf117350442::__InitAnimNode__AnimGraphNode_BlendListByBool_190BB93E435681C335E326B9FFD454C7()
{
	bpv__AnimGraphNode_BlendListByBool_190BB93E435681C335E326B9FFD454C7__pf.BlendPose = TArray<FPoseLink> ();
	bpv__AnimGraphNode_BlendListByBool_190BB93E435681C335E326B9FFD454C7__pf.BlendPose.AddUninitialized(2);
	FPoseLink::StaticStruct()->InitializeStruct(bpv__AnimGraphNode_BlendListByBool_190BB93E435681C335E326B9FFD454C7__pf.BlendPose.GetData(), 2);
	auto& __Local__6 = bpv__AnimGraphNode_BlendListByBool_190BB93E435681C335E326B9FFD454C7__pf.BlendPose[0];
	__Local__6.LinkID = 18;
	auto& __Local__7 = bpv__AnimGraphNode_BlendListByBool_190BB93E435681C335E326B9FFD454C7__pf.BlendPose[1];
	__Local__7.LinkID = 19;
	bpv__AnimGraphNode_BlendListByBool_190BB93E435681C335E326B9FFD454C7__pf.BlendTime = TArray<float> ();
	bpv__AnimGraphNode_BlendListByBool_190BB93E435681C335E326B9FFD454C7__pf.BlendTime.Reserve(2);
	bpv__AnimGraphNode_BlendListByBool_190BB93E435681C335E326B9FFD454C7__pf.BlendTime.Add(0.100000f);
	bpv__AnimGraphNode_BlendListByBool_190BB93E435681C335E326B9FFD454C7__pf.BlendTime.Add(0.100000f);
	bpv__AnimGraphNode_BlendListByBool_190BB93E435681C335E326B9FFD454C7__pf.EvaluateGraphExposedInputs.CopyRecords = TArray<FExposedValueCopyRecord> ();
	bpv__AnimGraphNode_BlendListByBool_190BB93E435681C335E326B9FFD454C7__pf.EvaluateGraphExposedInputs.CopyRecords.AddUninitialized(1);
	FExposedValueCopyRecord::StaticStruct()->InitializeStruct(bpv__AnimGraphNode_BlendListByBool_190BB93E435681C335E326B9FFD454C7__pf.EvaluateGraphExposedInputs.CopyRecords.GetData(), 1);
	auto& __Local__8 = bpv__AnimGraphNode_BlendListByBool_190BB93E435681C335E326B9FFD454C7__pf.EvaluateGraphExposedInputs.CopyRecords[0];
	__Local__8.SourcePropertyName = FName(TEXT("Aiming"));
	__Local__8.DestProperty = FindFieldChecked<UBoolProperty>(FAnimNode_BlendListByBool::StaticStruct(), TEXT("bActiveValue"));
	__Local__8.Size = 1;
}
void UABP_TP_C__pf117350442::__InitAnimNode__AnimGraphNode_SequencePlayer_C9E7B9F5420E8D13B586D0B99107B101()
{
	bpv__AnimGraphNode_SequencePlayer_C9E7B9F5420E8D13B586D0B99107B101__pf.Sequence = CastChecked<UAnimSequenceBase>(CastChecked<UDynamicClass>(UABP_TP_C__pf117350442::StaticClass())->UsedAssets[2], ECastCheckedType::NullAllowed);
}
void UABP_TP_C__pf117350442::__InitAnimNode__AnimGraphNode_SequencePlayer_1C0BEA184A4517BE4A73F6B8B18270A7()
{
	bpv__AnimGraphNode_SequencePlayer_1C0BEA184A4517BE4A73F6B8B18270A7__pf.Sequence = CastChecked<UAnimSequenceBase>(CastChecked<UDynamicClass>(UABP_TP_C__pf117350442::StaticClass())->UsedAssets[3], ECastCheckedType::NullAllowed);
}
void UABP_TP_C__pf117350442::__InitAnimNode__AnimGraphNode_StateResult_EB35F24F40F27C351EA7A7914C563941()
{
	bpv__AnimGraphNode_StateResult_EB35F24F40F27C351EA7A7914C563941__pf.Result.LinkID = 17;
}
void UABP_TP_C__pf117350442::__InitAnimNode__AnimGraphNode_SequencePlayer_7CE5CCED48F84FFA8C99C89BAFADD7E9()
{
	bpv__AnimGraphNode_SequencePlayer_7CE5CCED48F84FFA8C99C89BAFADD7E9__pf.Sequence = CastChecked<UAnimSequenceBase>(CastChecked<UDynamicClass>(UABP_TP_C__pf117350442::StaticClass())->UsedAssets[4], ECastCheckedType::NullAllowed);
}
void UABP_TP_C__pf117350442::__InitAnimNode__AnimGraphNode_StateResult_8707377A4A7AB494C1B0E98C0818F0AE()
{
	bpv__AnimGraphNode_StateResult_8707377A4A7AB494C1B0E98C0818F0AE__pf.Result.LinkID = 21;
}
void UABP_TP_C__pf117350442::__InitAnimNode__AnimGraphNode_SequencePlayer_6B905355488C69F57D1302949E0D172D()
{
	bpv__AnimGraphNode_SequencePlayer_6B905355488C69F57D1302949E0D172D__pf.Sequence = CastChecked<UAnimSequenceBase>(CastChecked<UDynamicClass>(UABP_TP_C__pf117350442::StaticClass())->UsedAssets[5], ECastCheckedType::NullAllowed);
}
void UABP_TP_C__pf117350442::__InitAnimNode__AnimGraphNode_StateResult_2CECE82A42D9A4C2879D88A5ECEE39CB()
{
	bpv__AnimGraphNode_StateResult_2CECE82A42D9A4C2879D88A5ECEE39CB__pf.Result.LinkID = 23;
}
void UABP_TP_C__pf117350442::__InitAnimNode__AnimGraphNode_BlendSpacePlayer_1CB238214E41B71CA1F34B95EA74ADA1()
{
	bpv__AnimGraphNode_BlendSpacePlayer_1CB238214E41B71CA1F34B95EA74ADA1__pf.BlendSpace = CastChecked<UBlendSpaceBase>(CastChecked<UDynamicClass>(UABP_TP_C__pf117350442::StaticClass())->UsedAssets[6], ECastCheckedType::NullAllowed);
	bpv__AnimGraphNode_BlendSpacePlayer_1CB238214E41B71CA1F34B95EA74ADA1__pf.EvaluateGraphExposedInputs.CopyRecords = TArray<FExposedValueCopyRecord> ();
	bpv__AnimGraphNode_BlendSpacePlayer_1CB238214E41B71CA1F34B95EA74ADA1__pf.EvaluateGraphExposedInputs.CopyRecords.AddUninitialized(2);
	FExposedValueCopyRecord::StaticStruct()->InitializeStruct(bpv__AnimGraphNode_BlendSpacePlayer_1CB238214E41B71CA1F34B95EA74ADA1__pf.EvaluateGraphExposedInputs.CopyRecords.GetData(), 2);
	auto& __Local__9 = bpv__AnimGraphNode_BlendSpacePlayer_1CB238214E41B71CA1F34B95EA74ADA1__pf.EvaluateGraphExposedInputs.CopyRecords[0];
	__Local__9.SourcePropertyName = FName(TEXT("Direction"));
	__Local__9.DestProperty = FindFieldChecked<UFloatProperty>(FAnimNode_BlendSpacePlayer::StaticStruct(), TEXT("X"));
	__Local__9.Size = 4;
	auto& __Local__10 = bpv__AnimGraphNode_BlendSpacePlayer_1CB238214E41B71CA1F34B95EA74ADA1__pf.EvaluateGraphExposedInputs.CopyRecords[1];
	__Local__10.SourcePropertyName = FName(TEXT("Speed"));
	__Local__10.DestProperty = FindFieldChecked<UFloatProperty>(FAnimNode_BlendSpacePlayer::StaticStruct(), TEXT("Y"));
	__Local__10.Size = 4;
}
void UABP_TP_C__pf117350442::__InitAnimNode__AnimGraphNode_BlendListByBool_43889F2643908C49EFAE0D8D7EB4CCC0()
{
	bpv__AnimGraphNode_BlendListByBool_43889F2643908C49EFAE0D8D7EB4CCC0__pf.BlendPose = TArray<FPoseLink> ();
	bpv__AnimGraphNode_BlendListByBool_43889F2643908C49EFAE0D8D7EB4CCC0__pf.BlendPose.AddUninitialized(2);
	FPoseLink::StaticStruct()->InitializeStruct(bpv__AnimGraphNode_BlendListByBool_43889F2643908C49EFAE0D8D7EB4CCC0__pf.BlendPose.GetData(), 2);
	auto& __Local__11 = bpv__AnimGraphNode_BlendListByBool_43889F2643908C49EFAE0D8D7EB4CCC0__pf.BlendPose[0];
	__Local__11.LinkID = 25;
	auto& __Local__12 = bpv__AnimGraphNode_BlendListByBool_43889F2643908C49EFAE0D8D7EB4CCC0__pf.BlendPose[1];
	__Local__12.LinkID = 27;
	bpv__AnimGraphNode_BlendListByBool_43889F2643908C49EFAE0D8D7EB4CCC0__pf.BlendTime = TArray<float> ();
	bpv__AnimGraphNode_BlendListByBool_43889F2643908C49EFAE0D8D7EB4CCC0__pf.BlendTime.Reserve(2);
	bpv__AnimGraphNode_BlendListByBool_43889F2643908C49EFAE0D8D7EB4CCC0__pf.BlendTime.Add(0.100000f);
	bpv__AnimGraphNode_BlendListByBool_43889F2643908C49EFAE0D8D7EB4CCC0__pf.BlendTime.Add(0.100000f);
	bpv__AnimGraphNode_BlendListByBool_43889F2643908C49EFAE0D8D7EB4CCC0__pf.EvaluateGraphExposedInputs.CopyRecords = TArray<FExposedValueCopyRecord> ();
	bpv__AnimGraphNode_BlendListByBool_43889F2643908C49EFAE0D8D7EB4CCC0__pf.EvaluateGraphExposedInputs.CopyRecords.AddUninitialized(1);
	FExposedValueCopyRecord::StaticStruct()->InitializeStruct(bpv__AnimGraphNode_BlendListByBool_43889F2643908C49EFAE0D8D7EB4CCC0__pf.EvaluateGraphExposedInputs.CopyRecords.GetData(), 1);
	auto& __Local__13 = bpv__AnimGraphNode_BlendListByBool_43889F2643908C49EFAE0D8D7EB4CCC0__pf.EvaluateGraphExposedInputs.CopyRecords[0];
	__Local__13.SourcePropertyName = FName(TEXT("Aiming"));
	__Local__13.DestProperty = FindFieldChecked<UBoolProperty>(FAnimNode_BlendListByBool::StaticStruct(), TEXT("bActiveValue"));
	__Local__13.Size = 1;
}
void UABP_TP_C__pf117350442::__InitAnimNode__AnimGraphNode_BlendSpacePlayer_1995CFFC4E4C2C5D00C8549266C69DEE()
{
	bpv__AnimGraphNode_BlendSpacePlayer_1995CFFC4E4C2C5D00C8549266C69DEE__pf.BlendSpace = CastChecked<UBlendSpaceBase>(CastChecked<UDynamicClass>(UABP_TP_C__pf117350442::StaticClass())->UsedAssets[7], ECastCheckedType::NullAllowed);
	bpv__AnimGraphNode_BlendSpacePlayer_1995CFFC4E4C2C5D00C8549266C69DEE__pf.EvaluateGraphExposedInputs.CopyRecords = TArray<FExposedValueCopyRecord> ();
	bpv__AnimGraphNode_BlendSpacePlayer_1995CFFC4E4C2C5D00C8549266C69DEE__pf.EvaluateGraphExposedInputs.CopyRecords.AddUninitialized(2);
	FExposedValueCopyRecord::StaticStruct()->InitializeStruct(bpv__AnimGraphNode_BlendSpacePlayer_1995CFFC4E4C2C5D00C8549266C69DEE__pf.EvaluateGraphExposedInputs.CopyRecords.GetData(), 2);
	auto& __Local__14 = bpv__AnimGraphNode_BlendSpacePlayer_1995CFFC4E4C2C5D00C8549266C69DEE__pf.EvaluateGraphExposedInputs.CopyRecords[0];
	__Local__14.SourcePropertyName = FName(TEXT("Direction"));
	__Local__14.DestProperty = FindFieldChecked<UFloatProperty>(FAnimNode_BlendSpacePlayer::StaticStruct(), TEXT("X"));
	__Local__14.Size = 4;
	auto& __Local__15 = bpv__AnimGraphNode_BlendSpacePlayer_1995CFFC4E4C2C5D00C8549266C69DEE__pf.EvaluateGraphExposedInputs.CopyRecords[1];
	__Local__15.SourcePropertyName = FName(TEXT("Speed"));
	__Local__15.DestProperty = FindFieldChecked<UFloatProperty>(FAnimNode_BlendSpacePlayer::StaticStruct(), TEXT("Y"));
	__Local__15.Size = 4;
}
void UABP_TP_C__pf117350442::__InitAnimNode__AnimGraphNode_StateResult_5925BA954F8CE550AF7F2186CB70990D()
{
	bpv__AnimGraphNode_StateResult_5925BA954F8CE550AF7F2186CB70990D__pf.Result.LinkID = 26;
}
void UABP_TP_C__pf117350442::__InitAnimNode__AnimGraphNode_StateMachine_AD21F2574F606D48D79179B2C8B9494C()
{
}
void UABP_TP_C__pf117350442::__InitAnimNode__AnimGraphNode_RotationOffsetBlendSpace_AEA90FFA4CA2B79C913CE8860FF4295A()
{
	bpv__AnimGraphNode_RotationOffsetBlendSpace_AEA90FFA4CA2B79C913CE8860FF4295A__pf.BasePose.LinkID = 36;
	bpv__AnimGraphNode_RotationOffsetBlendSpace_AEA90FFA4CA2B79C913CE8860FF4295A__pf.BlendSpace = CastChecked<UBlendSpaceBase>(CastChecked<UDynamicClass>(UABP_TP_C__pf117350442::StaticClass())->UsedAssets[8], ECastCheckedType::NullAllowed);
	bpv__AnimGraphNode_RotationOffsetBlendSpace_AEA90FFA4CA2B79C913CE8860FF4295A__pf.EvaluateGraphExposedInputs.CopyRecords = TArray<FExposedValueCopyRecord> ();
	bpv__AnimGraphNode_RotationOffsetBlendSpace_AEA90FFA4CA2B79C913CE8860FF4295A__pf.EvaluateGraphExposedInputs.CopyRecords.AddUninitialized(1);
	FExposedValueCopyRecord::StaticStruct()->InitializeStruct(bpv__AnimGraphNode_RotationOffsetBlendSpace_AEA90FFA4CA2B79C913CE8860FF4295A__pf.EvaluateGraphExposedInputs.CopyRecords.GetData(), 1);
	auto& __Local__16 = bpv__AnimGraphNode_RotationOffsetBlendSpace_AEA90FFA4CA2B79C913CE8860FF4295A__pf.EvaluateGraphExposedInputs.CopyRecords[0];
	__Local__16.SourcePropertyName = FName(TEXT("AimAngle"));
	__Local__16.DestProperty = FindFieldChecked<UFloatProperty>(FAnimNode_BlendSpacePlayer::StaticStruct(), TEXT("X"));
	__Local__16.Size = 4;
}
void UABP_TP_C__pf117350442::__InitAnimNode__AnimGraphNode_TwoBoneIK_111D181F4D478FDDAF41D68A861FC312()
{
	bpv__AnimGraphNode_TwoBoneIK_111D181F4D478FDDAF41D68A861FC312__pf.IKBone.BoneName = FName(TEXT("hand_l"));
	bpv__AnimGraphNode_TwoBoneIK_111D181F4D478FDDAF41D68A861FC312__pf.EffectorLocationSpace = EBoneControlSpace::BCS_BoneSpace;
	bpv__AnimGraphNode_TwoBoneIK_111D181F4D478FDDAF41D68A861FC312__pf.EffectorLocation = FVector(-34.431999, 14.381000, 2.443000);
	bpv__AnimGraphNode_TwoBoneIK_111D181F4D478FDDAF41D68A861FC312__pf.EffectorTarget.BoneReference.BoneName = FName(TEXT("hand_r"));
	bpv__AnimGraphNode_TwoBoneIK_111D181F4D478FDDAF41D68A861FC312__pf.JointTargetLocationSpace = EBoneControlSpace::BCS_BoneSpace;
	bpv__AnimGraphNode_TwoBoneIK_111D181F4D478FDDAF41D68A861FC312__pf.JointTargetLocation = FVector(-11.050435, 53.528187, -24.737900);
	bpv__AnimGraphNode_TwoBoneIK_111D181F4D478FDDAF41D68A861FC312__pf.JointTarget.BoneReference.BoneName = FName(TEXT("hand_r"));
	bpv__AnimGraphNode_TwoBoneIK_111D181F4D478FDDAF41D68A861FC312__pf.ComponentPose.LinkID = 32;
}
void UABP_TP_C__pf117350442::__InitAnimNode__AnimGraphNode_LocalToComponentSpace_2DCCCE774E2909C36E5D9FB62C8CA8AB()
{
	bpv__AnimGraphNode_LocalToComponentSpace_2DCCCE774E2909C36E5D9FB62C8CA8AB__pf.LocalPose.LinkID = 30;
}
void UABP_TP_C__pf117350442::__InitAnimNode__AnimGraphNode_ComponentToLocalSpace_72D9FB7049B2FDBC82442B84EDB8401A()
{
	bpv__AnimGraphNode_ComponentToLocalSpace_72D9FB7049B2FDBC82442B84EDB8401A__pf.ComponentPose.LinkID = 31;
}
void UABP_TP_C__pf117350442::__InitAnimNode__AnimGraphNode_BlendListByBool_50D1A82A4333BC39A9A45E961A45F789()
{
	bpv__AnimGraphNode_BlendListByBool_50D1A82A4333BC39A9A45E961A45F789__pf.BlendPose = TArray<FPoseLink> ();
	bpv__AnimGraphNode_BlendListByBool_50D1A82A4333BC39A9A45E961A45F789__pf.BlendPose.AddUninitialized(2);
	FPoseLink::StaticStruct()->InitializeStruct(bpv__AnimGraphNode_BlendListByBool_50D1A82A4333BC39A9A45E961A45F789__pf.BlendPose.GetData(), 2);
	auto& __Local__17 = bpv__AnimGraphNode_BlendListByBool_50D1A82A4333BC39A9A45E961A45F789__pf.BlendPose[0];
	__Local__17.LinkID = 35;
	auto& __Local__18 = bpv__AnimGraphNode_BlendListByBool_50D1A82A4333BC39A9A45E961A45F789__pf.BlendPose[1];
	__Local__18.LinkID = 33;
	bpv__AnimGraphNode_BlendListByBool_50D1A82A4333BC39A9A45E961A45F789__pf.BlendTime = TArray<float> ();
	bpv__AnimGraphNode_BlendListByBool_50D1A82A4333BC39A9A45E961A45F789__pf.BlendTime.Reserve(2);
	bpv__AnimGraphNode_BlendListByBool_50D1A82A4333BC39A9A45E961A45F789__pf.BlendTime.Add(0.100000f);
	bpv__AnimGraphNode_BlendListByBool_50D1A82A4333BC39A9A45E961A45F789__pf.BlendTime.Add(0.100000f);
	bpv__AnimGraphNode_BlendListByBool_50D1A82A4333BC39A9A45E961A45F789__pf.EvaluateGraphExposedInputs.CopyRecords = TArray<FExposedValueCopyRecord> ();
	bpv__AnimGraphNode_BlendListByBool_50D1A82A4333BC39A9A45E961A45F789__pf.EvaluateGraphExposedInputs.CopyRecords.AddUninitialized(1);
	FExposedValueCopyRecord::StaticStruct()->InitializeStruct(bpv__AnimGraphNode_BlendListByBool_50D1A82A4333BC39A9A45E961A45F789__pf.EvaluateGraphExposedInputs.CopyRecords.GetData(), 1);
	auto& __Local__19 = bpv__AnimGraphNode_BlendListByBool_50D1A82A4333BC39A9A45E961A45F789__pf.EvaluateGraphExposedInputs.CopyRecords[0];
	__Local__19.SourcePropertyName = FName(TEXT("Is Dead"));
	__Local__19.DestProperty = FindFieldChecked<UBoolProperty>(FAnimNode_BlendListByBool::StaticStruct(), TEXT("bActiveValue"));
	__Local__19.Size = 1;
}
void UABP_TP_C__pf117350442::__InitAnimNode__AnimGraphNode_SequencePlayer_96134A414BCA0FA7562755AF6E8B0FAF()
{
	bpv__AnimGraphNode_SequencePlayer_96134A414BCA0FA7562755AF6E8B0FAF__pf.Sequence = CastChecked<UAnimSequenceBase>(CastChecked<UDynamicClass>(UABP_TP_C__pf117350442::StaticClass())->UsedAssets[9], ECastCheckedType::NullAllowed);
	bpv__AnimGraphNode_SequencePlayer_96134A414BCA0FA7562755AF6E8B0FAF__pf.bLoopAnimation = false;
}
void UABP_TP_C__pf117350442::__InitAnimNode__AnimGraphNode_LayeredBoneBlend_1EBB97F049A58A8C49417BBCCDCB127C()
{
	bpv__AnimGraphNode_LayeredBoneBlend_1EBB97F049A58A8C49417BBCCDCB127C__pf.BasePose.LinkID = 40;
	bpv__AnimGraphNode_LayeredBoneBlend_1EBB97F049A58A8C49417BBCCDCB127C__pf.BlendPoses = TArray<FPoseLink> ();
	bpv__AnimGraphNode_LayeredBoneBlend_1EBB97F049A58A8C49417BBCCDCB127C__pf.BlendPoses.AddUninitialized(1);
	FPoseLink::StaticStruct()->InitializeStruct(bpv__AnimGraphNode_LayeredBoneBlend_1EBB97F049A58A8C49417BBCCDCB127C__pf.BlendPoses.GetData(), 1);
	auto& __Local__20 = bpv__AnimGraphNode_LayeredBoneBlend_1EBB97F049A58A8C49417BBCCDCB127C__pf.BlendPoses[0];
	__Local__20.LinkID = 39;
	bpv__AnimGraphNode_LayeredBoneBlend_1EBB97F049A58A8C49417BBCCDCB127C__pf.LayerSetup = TArray<FInputBlendPose> ();
	bpv__AnimGraphNode_LayeredBoneBlend_1EBB97F049A58A8C49417BBCCDCB127C__pf.LayerSetup.AddUninitialized(1);
	FInputBlendPose::StaticStruct()->InitializeStruct(bpv__AnimGraphNode_LayeredBoneBlend_1EBB97F049A58A8C49417BBCCDCB127C__pf.LayerSetup.GetData(), 1);
	auto& __Local__21 = bpv__AnimGraphNode_LayeredBoneBlend_1EBB97F049A58A8C49417BBCCDCB127C__pf.LayerSetup[0];
	__Local__21.BranchFilters = TArray<FBranchFilter> ();
	__Local__21.BranchFilters.AddUninitialized(1);
	FBranchFilter::StaticStruct()->InitializeStruct(__Local__21.BranchFilters.GetData(), 1);
	auto& __Local__22 = __Local__21.BranchFilters[0];
	__Local__22.BoneName = FName(TEXT("spine_01"));
	__Local__22.BlendDepth = 1;
	bpv__AnimGraphNode_LayeredBoneBlend_1EBB97F049A58A8C49417BBCCDCB127C__pf.BlendWeights = TArray<float> ();
	bpv__AnimGraphNode_LayeredBoneBlend_1EBB97F049A58A8C49417BBCCDCB127C__pf.BlendWeights.Reserve(1);
	bpv__AnimGraphNode_LayeredBoneBlend_1EBB97F049A58A8C49417BBCCDCB127C__pf.BlendWeights.Add(1.000000f);
}
void UABP_TP_C__pf117350442::__InitAnimNode__AnimGraphNode_SaveCachedPose_4E344B114BA23C91AF76CA82B09BCF3F()
{
	bpv__AnimGraphNode_SaveCachedPose_4E344B114BA23C91AF76CA82B09BCF3F__pf.Pose.LinkID = 29;
	bpv__AnimGraphNode_SaveCachedPose_4E344B114BA23C91AF76CA82B09BCF3F__pf.CachePoseName = FName(TEXT("LocomotionCached"));
}
void UABP_TP_C__pf117350442::__InitAnimNode__AnimGraphNode_UseCachedPose_228FCBE0403B02D29A99AF8FCECD2598()
{
	bpv__AnimGraphNode_UseCachedPose_228FCBE0403B02D29A99AF8FCECD2598__pf.LinkToCachingNode.LinkID = 37;
	bpv__AnimGraphNode_UseCachedPose_228FCBE0403B02D29A99AF8FCECD2598__pf.CachePoseName = FName(TEXT("LocomotionCached"));
}
void UABP_TP_C__pf117350442::__InitAnimNode__AnimGraphNode_Slot_8C801B6C4B22962DD963CB948A9E9169()
{
	bpv__AnimGraphNode_Slot_8C801B6C4B22962DD963CB948A9E9169__pf.Source.LinkID = 38;
	bpv__AnimGraphNode_Slot_8C801B6C4B22962DD963CB948A9E9169__pf.SlotName = FName(TEXT("Arms"));
}
void UABP_TP_C__pf117350442::__InitAnimNode__AnimGraphNode_UseCachedPose_C94056184C8FA2A45BD06883D0526F36()
{
	bpv__AnimGraphNode_UseCachedPose_C94056184C8FA2A45BD06883D0526F36__pf.LinkToCachingNode.LinkID = 37;
	bpv__AnimGraphNode_UseCachedPose_C94056184C8FA2A45BD06883D0526F36__pf.CachePoseName = FName(TEXT("LocomotionCached"));
}
void UABP_TP_C__pf117350442::PostLoadSubobjects(FObjectInstancingGraph* OuterInstanceGraph)
{
	Super::PostLoadSubobjects(OuterInstanceGraph);
}
PRAGMA_DISABLE_OPTIMIZATION
void UABP_TP_C__pf117350442::__CustomDynamicClassInitialization(UDynamicClass* InDynamicClass)
{
	ensure(0 == InDynamicClass->ReferencedConvertedFields.Num());
	ensure(0 == InDynamicClass->MiscConvertedSubobjects.Num());
	ensure(0 == InDynamicClass->DynamicBindingObjects.Num());
	ensure(0 == InDynamicClass->ComponentTemplates.Num());
	ensure(0 == InDynamicClass->Timelines.Num());
	ensure(nullptr == InDynamicClass->AnimClassImplementation);
	InDynamicClass->AssembleReferenceTokenStream();
	// List of all referenced converted classes
	InDynamicClass->ReferencedConvertedFields.Add(ABP_TL4Character_C__pf3043094308::StaticClass());
	FConvertedBlueprintsDependencies::FillUsedAssetsInDynamicClass(InDynamicClass, &__StaticDependencies_DirectlyUsedAssets);
	auto __Local__23 = NewObject<UAnimClassData>(InDynamicClass, TEXT("AnimClassData"));
	__Local__23->BakedStateMachines = TArray<FBakedAnimationStateMachine> ();
	__Local__23->BakedStateMachines.AddUninitialized(1);
	FBakedAnimationStateMachine::StaticStruct()->InitializeStruct(__Local__23->BakedStateMachines.GetData(), 1);
	auto& __Local__24 = __Local__23->BakedStateMachines[0];
	__Local__24.MachineName = FName(TEXT("Locomotion"));
	__Local__24.InitialState = 0;
	__Local__24.States = TArray<FBakedAnimationState> ();
	__Local__24.States.AddUninitialized(6);
	FBakedAnimationState::StaticStruct()->InitializeStruct(__Local__24.States.GetData(), 6);
	auto& __Local__25 = __Local__24.States[0];
	__Local__25.StateName = FName(TEXT("Idle"));
	__Local__25.Transitions = TArray<FBakedStateExitTransition> ();
	__Local__25.Transitions.AddUninitialized(3);
	FBakedStateExitTransition::StaticStruct()->InitializeStruct(__Local__25.Transitions.GetData(), 3);
	auto& __Local__26 = __Local__25.Transitions[0];
	__Local__26.CanTakeDelegateIndex = 28;
	__Local__26.TransitionIndex = 4;
	auto& __Local__27 = __Local__25.Transitions[1];
	__Local__27.CanTakeDelegateIndex = 29;
	__Local__27.TransitionIndex = 6;
	auto& __Local__28 = __Local__25.Transitions[2];
	__Local__28.CanTakeDelegateIndex = 30;
	__Local__28.TransitionIndex = 2;
	__Local__25.StateRootNodeIndex = 20;
	__Local__25.StartNotify = 2;
	__Local__25.PlayerNodeIndices = TArray<int32> ();
	__Local__25.PlayerNodeIndices.Reserve(2);
	__Local__25.PlayerNodeIndices.Add(21);
	__Local__25.PlayerNodeIndices.Add(22);
	auto& __Local__29 = __Local__24.States[1];
	__Local__29.StateName = FName(TEXT("Move"));
	__Local__29.Transitions = TArray<FBakedStateExitTransition> ();
	__Local__29.Transitions.AddUninitialized(3);
	FBakedStateExitTransition::StaticStruct()->InitializeStruct(__Local__29.Transitions.GetData(), 3);
	auto& __Local__30 = __Local__29.Transitions[0];
	__Local__30.CanTakeDelegateIndex = 31;
	__Local__30.TransitionIndex = 3;
	auto& __Local__31 = __Local__29.Transitions[1];
	__Local__31.CanTakeDelegateIndex = 32;
	__Local__31.TransitionIndex = 10;
	auto& __Local__32 = __Local__29.Transitions[2];
	__Local__32.CanTakeDelegateIndex = 33;
	__Local__32.TransitionIndex = 0;
	__Local__29.StateRootNodeIndex = 12;
	__Local__29.StartNotify = 0;
	__Local__29.PlayerNodeIndices = TArray<int32> ();
	__Local__29.PlayerNodeIndices.Reserve(2);
	__Local__29.PlayerNodeIndices.Add(13);
	__Local__29.PlayerNodeIndices.Add(15);
	auto& __Local__33 = __Local__24.States[2];
	__Local__33.StateName = FName(TEXT("Jump"));
	__Local__33.Transitions = TArray<FBakedStateExitTransition> ();
	__Local__33.Transitions.AddUninitialized(1);
	FBakedStateExitTransition::StaticStruct()->InitializeStruct(__Local__33.Transitions.GetData(), 1);
	auto& __Local__34 = __Local__33.Transitions[0];
	__Local__34.CanTakeDelegateIndex = 34;
	__Local__34.TransitionIndex = 5;
	__Local__33.StateRootNodeIndex = 16;
	__Local__33.StartNotify = 1;
	__Local__33.PlayerNodeIndices = TArray<int32> ();
	__Local__33.PlayerNodeIndices.Reserve(1);
	__Local__33.PlayerNodeIndices.Add(17);
	auto& __Local__35 = __Local__24.States[3];
	__Local__35.StateName = FName(TEXT("Run Jump"));
	__Local__35.Transitions = TArray<FBakedStateExitTransition> ();
	__Local__35.Transitions.AddUninitialized(1);
	FBakedStateExitTransition::StaticStruct()->InitializeStruct(__Local__35.Transitions.GetData(), 1);
	auto& __Local__36 = __Local__35.Transitions[0];
	__Local__36.CanTakeDelegateIndex = 35;
	__Local__36.TransitionIndex = 1;
	__Local__35.StateRootNodeIndex = 18;
	__Local__35.PlayerNodeIndices = TArray<int32> ();
	__Local__35.PlayerNodeIndices.Reserve(1);
	__Local__35.PlayerNodeIndices.Add(19);
	auto& __Local__37 = __Local__24.States[4];
	__Local__37.StateName = FName(TEXT("Crouch"));
	__Local__37.Transitions = TArray<FBakedStateExitTransition> ();
	__Local__37.Transitions.AddUninitialized(2);
	FBakedStateExitTransition::StaticStruct()->InitializeStruct(__Local__37.Transitions.GetData(), 2);
	auto& __Local__38 = __Local__37.Transitions[0];
	__Local__38.CanTakeDelegateIndex = 36;
	__Local__38.TransitionIndex = 8;
	auto& __Local__39 = __Local__37.Transitions[1];
	__Local__39.CanTakeDelegateIndex = 37;
	__Local__39.TransitionIndex = 7;
	__Local__37.StateRootNodeIndex = 24;
	__Local__37.PlayerNodeIndices = TArray<int32> ();
	__Local__37.PlayerNodeIndices.Reserve(1);
	__Local__37.PlayerNodeIndices.Add(25);
	auto& __Local__40 = __Local__24.States[5];
	__Local__40.StateName = FName(TEXT("Crouch Walk"));
	__Local__40.Transitions = TArray<FBakedStateExitTransition> ();
	__Local__40.Transitions.AddUninitialized(2);
	FBakedStateExitTransition::StaticStruct()->InitializeStruct(__Local__40.Transitions.GetData(), 2);
	auto& __Local__41 = __Local__40.Transitions[0];
	__Local__41.CanTakeDelegateIndex = 38;
	__Local__41.TransitionIndex = 11;
	auto& __Local__42 = __Local__40.Transitions[1];
	__Local__42.CanTakeDelegateIndex = 39;
	__Local__42.TransitionIndex = 9;
	__Local__40.StateRootNodeIndex = 26;
	__Local__40.PlayerNodeIndices = TArray<int32> ();
	__Local__40.PlayerNodeIndices.Reserve(1);
	__Local__40.PlayerNodeIndices.Add(27);
	__Local__24.Transitions = TArray<FAnimationTransitionBetweenStates> ();
	__Local__24.Transitions.AddUninitialized(12);
	FAnimationTransitionBetweenStates::StaticStruct()->InitializeStruct(__Local__24.Transitions.GetData(), 12);
	auto& __Local__43 = __Local__24.Transitions[0];
	__Local__43.PreviousState = 1;
	__Local__43.NextState = 3;
	__Local__43.CrossfadeDuration = 0.200000f;
	__Local__43.BlendMode = EAlphaBlendOption::Linear;
	auto& __Local__44 = __Local__24.Transitions[1];
	__Local__44.PreviousState = 3;
	__Local__44.NextState = 1;
	__Local__44.CrossfadeDuration = 0.200000f;
	__Local__44.BlendMode = EAlphaBlendOption::Linear;
	auto& __Local__45 = __Local__24.Transitions[2];
	__Local__45.PreviousState = 0;
	__Local__45.NextState = 1;
	__Local__45.CrossfadeDuration = 0.200000f;
	__Local__45.BlendMode = EAlphaBlendOption::Linear;
	auto& __Local__46 = __Local__24.Transitions[3];
	__Local__46.PreviousState = 1;
	__Local__46.NextState = 0;
	__Local__46.CrossfadeDuration = 0.200000f;
	__Local__46.BlendMode = EAlphaBlendOption::Linear;
	auto& __Local__47 = __Local__24.Transitions[4];
	__Local__47.PreviousState = 0;
	__Local__47.NextState = 2;
	__Local__47.CrossfadeDuration = 0.200000f;
	__Local__47.BlendMode = EAlphaBlendOption::Linear;
	auto& __Local__48 = __Local__24.Transitions[5];
	__Local__48.PreviousState = 2;
	__Local__48.NextState = 0;
	__Local__48.CrossfadeDuration = 0.200000f;
	__Local__48.BlendMode = EAlphaBlendOption::Linear;
	auto& __Local__49 = __Local__24.Transitions[6];
	__Local__49.PreviousState = 0;
	__Local__49.NextState = 4;
	__Local__49.CrossfadeDuration = 0.200000f;
	__Local__49.BlendMode = EAlphaBlendOption::Linear;
	auto& __Local__50 = __Local__24.Transitions[7];
	__Local__50.PreviousState = 4;
	__Local__50.NextState = 0;
	__Local__50.CrossfadeDuration = 0.200000f;
	__Local__50.BlendMode = EAlphaBlendOption::Linear;
	auto& __Local__51 = __Local__24.Transitions[8];
	__Local__51.PreviousState = 4;
	__Local__51.NextState = 5;
	__Local__51.CrossfadeDuration = 0.200000f;
	__Local__51.BlendMode = EAlphaBlendOption::Linear;
	auto& __Local__52 = __Local__24.Transitions[9];
	__Local__52.PreviousState = 5;
	__Local__52.NextState = 4;
	__Local__52.CrossfadeDuration = 0.200000f;
	__Local__52.BlendMode = EAlphaBlendOption::Linear;
	auto& __Local__53 = __Local__24.Transitions[10];
	__Local__53.PreviousState = 1;
	__Local__53.NextState = 5;
	__Local__53.CrossfadeDuration = 0.200000f;
	__Local__53.BlendMode = EAlphaBlendOption::Linear;
	auto& __Local__54 = __Local__24.Transitions[11];
	__Local__54.PreviousState = 5;
	__Local__54.NextState = 1;
	__Local__54.CrossfadeDuration = 0.200000f;
	__Local__54.BlendMode = EAlphaBlendOption::Linear;
	__Local__23->TargetSkeleton = CastChecked<USkeleton>(CastChecked<UDynamicClass>(UABP_TP_C__pf117350442::StaticClass())->UsedAssets[10], ECastCheckedType::NullAllowed);
	__Local__23->AnimNotifies = TArray<FAnimNotifyEvent> ();
	__Local__23->AnimNotifies.AddUninitialized(3);
	FAnimNotifyEvent::StaticStruct()->InitializeStruct(__Local__23->AnimNotifies.GetData(), 3);
	auto& __Local__55 = __Local__23->AnimNotifies[0];
	__Local__55.NotifyName = FName(TEXT("JogStart"));
	auto& __Local__56 = __Local__23->AnimNotifies[1];
	__Local__56.NotifyName = FName(TEXT("StartJump"));
	auto& __Local__57 = __Local__23->AnimNotifies[2];
	__Local__57.NotifyName = FName(TEXT("IdleStart"));
	__Local__23->OrderedSavedPoseIndices = TArray<int32> ();
	__Local__23->OrderedSavedPoseIndices.Reserve(1);
	__Local__23->OrderedSavedPoseIndices.Add(3);
	__Local__23->RootAnimNodeProperty = InDynamicClass->FindStructPropertyChecked(TEXT("AnimGraphNode_Root_0C0304664E5D637E777F8F82B80BDD47"));
	__Local__23->AnimNodeProperties = TArray<UStructProperty*> ();
	__Local__23->AnimNodeProperties.Reserve(41);
	__Local__23->AnimNodeProperties.Add(InDynamicClass->FindStructPropertyChecked(TEXT("AnimGraphNode_Root_0C0304664E5D637E777F8F82B80BDD47")));
	__Local__23->AnimNodeProperties.Add(InDynamicClass->FindStructPropertyChecked(TEXT("AnimGraphNode_TransitionResult_4AAD4A594EABD4F92E1D44BBD3F4830B")));
	__Local__23->AnimNodeProperties.Add(InDynamicClass->FindStructPropertyChecked(TEXT("AnimGraphNode_TransitionResult_78C3746B4C10022D609AD6A27618D2ED")));
	__Local__23->AnimNodeProperties.Add(InDynamicClass->FindStructPropertyChecked(TEXT("AnimGraphNode_TransitionResult_58EDEBE34160C294191EBFAB9F63A3B7")));
	__Local__23->AnimNodeProperties.Add(InDynamicClass->FindStructPropertyChecked(TEXT("AnimGraphNode_TransitionResult_F13AB6764EBE8335385B039A555B9993")));
	__Local__23->AnimNodeProperties.Add(InDynamicClass->FindStructPropertyChecked(TEXT("AnimGraphNode_TransitionResult_23C30FC54F432C27A8D2E086D1A0B4F4")));
	__Local__23->AnimNodeProperties.Add(InDynamicClass->FindStructPropertyChecked(TEXT("AnimGraphNode_TransitionResult_FFFCF0144598EDEE64EE439BD7BDAFDF")));
	__Local__23->AnimNodeProperties.Add(InDynamicClass->FindStructPropertyChecked(TEXT("AnimGraphNode_TransitionResult_5540D45C4BCA55AEC05DA1A1E21BFB58")));
	__Local__23->AnimNodeProperties.Add(InDynamicClass->FindStructPropertyChecked(TEXT("AnimGraphNode_TransitionResult_E2FA68D046B28AF86B5CBAA099FA056F")));
	__Local__23->AnimNodeProperties.Add(InDynamicClass->FindStructPropertyChecked(TEXT("AnimGraphNode_TransitionResult_315E9BF04EA579C328EB6A866B91C07A")));
	__Local__23->AnimNodeProperties.Add(InDynamicClass->FindStructPropertyChecked(TEXT("AnimGraphNode_TransitionResult_D25429B644E6C66D625827BA5AE5F04E")));
	__Local__23->AnimNodeProperties.Add(InDynamicClass->FindStructPropertyChecked(TEXT("AnimGraphNode_TransitionResult_5C6D350441E998F44093C798BB4C071C")));
	__Local__23->AnimNodeProperties.Add(InDynamicClass->FindStructPropertyChecked(TEXT("AnimGraphNode_TransitionResult_3163F8234400D90A8D5A4494801CFE23")));
	__Local__23->AnimNodeProperties.Add(InDynamicClass->FindStructPropertyChecked(TEXT("AnimGraphNode_BlendSpacePlayer_C6840A414E888EE73F8F539647D22D1E")));
	__Local__23->AnimNodeProperties.Add(InDynamicClass->FindStructPropertyChecked(TEXT("AnimGraphNode_StateResult_4664B17F4CB02F4B6A675C8868CAFEA8")));
	__Local__23->AnimNodeProperties.Add(InDynamicClass->FindStructPropertyChecked(TEXT("AnimGraphNode_SequencePlayer_C2D643BA4C3600B1D9D7D8A75F5F8A1F")));
	__Local__23->AnimNodeProperties.Add(InDynamicClass->FindStructPropertyChecked(TEXT("AnimGraphNode_StateResult_923A2E92452270AE79162487AF01084E")));
	__Local__23->AnimNodeProperties.Add(InDynamicClass->FindStructPropertyChecked(TEXT("AnimGraphNode_BlendListByBool_190BB93E435681C335E326B9FFD454C7")));
	__Local__23->AnimNodeProperties.Add(InDynamicClass->FindStructPropertyChecked(TEXT("AnimGraphNode_SequencePlayer_C9E7B9F5420E8D13B586D0B99107B101")));
	__Local__23->AnimNodeProperties.Add(InDynamicClass->FindStructPropertyChecked(TEXT("AnimGraphNode_SequencePlayer_1C0BEA184A4517BE4A73F6B8B18270A7")));
	__Local__23->AnimNodeProperties.Add(InDynamicClass->FindStructPropertyChecked(TEXT("AnimGraphNode_StateResult_EB35F24F40F27C351EA7A7914C563941")));
	__Local__23->AnimNodeProperties.Add(InDynamicClass->FindStructPropertyChecked(TEXT("AnimGraphNode_SequencePlayer_7CE5CCED48F84FFA8C99C89BAFADD7E9")));
	__Local__23->AnimNodeProperties.Add(InDynamicClass->FindStructPropertyChecked(TEXT("AnimGraphNode_StateResult_8707377A4A7AB494C1B0E98C0818F0AE")));
	__Local__23->AnimNodeProperties.Add(InDynamicClass->FindStructPropertyChecked(TEXT("AnimGraphNode_SequencePlayer_6B905355488C69F57D1302949E0D172D")));
	__Local__23->AnimNodeProperties.Add(InDynamicClass->FindStructPropertyChecked(TEXT("AnimGraphNode_StateResult_2CECE82A42D9A4C2879D88A5ECEE39CB")));
	__Local__23->AnimNodeProperties.Add(InDynamicClass->FindStructPropertyChecked(TEXT("AnimGraphNode_BlendSpacePlayer_1CB238214E41B71CA1F34B95EA74ADA1")));
	__Local__23->AnimNodeProperties.Add(InDynamicClass->FindStructPropertyChecked(TEXT("AnimGraphNode_BlendListByBool_43889F2643908C49EFAE0D8D7EB4CCC0")));
	__Local__23->AnimNodeProperties.Add(InDynamicClass->FindStructPropertyChecked(TEXT("AnimGraphNode_BlendSpacePlayer_1995CFFC4E4C2C5D00C8549266C69DEE")));
	__Local__23->AnimNodeProperties.Add(InDynamicClass->FindStructPropertyChecked(TEXT("AnimGraphNode_StateResult_5925BA954F8CE550AF7F2186CB70990D")));
	__Local__23->AnimNodeProperties.Add(InDynamicClass->FindStructPropertyChecked(TEXT("AnimGraphNode_StateMachine_AD21F2574F606D48D79179B2C8B9494C")));
	__Local__23->AnimNodeProperties.Add(InDynamicClass->FindStructPropertyChecked(TEXT("AnimGraphNode_RotationOffsetBlendSpace_AEA90FFA4CA2B79C913CE8860FF4295A")));
	__Local__23->AnimNodeProperties.Add(InDynamicClass->FindStructPropertyChecked(TEXT("AnimGraphNode_TwoBoneIK_111D181F4D478FDDAF41D68A861FC312")));
	__Local__23->AnimNodeProperties.Add(InDynamicClass->FindStructPropertyChecked(TEXT("AnimGraphNode_LocalToComponentSpace_2DCCCE774E2909C36E5D9FB62C8CA8AB")));
	__Local__23->AnimNodeProperties.Add(InDynamicClass->FindStructPropertyChecked(TEXT("AnimGraphNode_ComponentToLocalSpace_72D9FB7049B2FDBC82442B84EDB8401A")));
	__Local__23->AnimNodeProperties.Add(InDynamicClass->FindStructPropertyChecked(TEXT("AnimGraphNode_BlendListByBool_50D1A82A4333BC39A9A45E961A45F789")));
	__Local__23->AnimNodeProperties.Add(InDynamicClass->FindStructPropertyChecked(TEXT("AnimGraphNode_SequencePlayer_96134A414BCA0FA7562755AF6E8B0FAF")));
	__Local__23->AnimNodeProperties.Add(InDynamicClass->FindStructPropertyChecked(TEXT("AnimGraphNode_LayeredBoneBlend_1EBB97F049A58A8C49417BBCCDCB127C")));
	__Local__23->AnimNodeProperties.Add(InDynamicClass->FindStructPropertyChecked(TEXT("AnimGraphNode_SaveCachedPose_4E344B114BA23C91AF76CA82B09BCF3F")));
	__Local__23->AnimNodeProperties.Add(InDynamicClass->FindStructPropertyChecked(TEXT("AnimGraphNode_UseCachedPose_228FCBE0403B02D29A99AF8FCECD2598")));
	__Local__23->AnimNodeProperties.Add(InDynamicClass->FindStructPropertyChecked(TEXT("AnimGraphNode_Slot_8C801B6C4B22962DD963CB948A9E9169")));
	__Local__23->AnimNodeProperties.Add(InDynamicClass->FindStructPropertyChecked(TEXT("AnimGraphNode_UseCachedPose_C94056184C8FA2A45BD06883D0526F36")));
	InDynamicClass->AnimClassImplementation = __Local__23;
}
PRAGMA_ENABLE_OPTIMIZATION
void UABP_TP_C__pf117350442::bpf__ExecuteUbergraph_ABP_TP__pf_0(int32 bpp__EntryPoint__pf)
{
	check(bpp__EntryPoint__pf == 19);
	// optimized KCST_UnconditionalGoto
	bpf__BindxMovement__pfT();
	bpf__BindxJump__pfT();
	// optimized KCST_UnconditionalGoto
	bpf__BindxNPCxGuardxState__pfTTT();
	bpf__BindxAim__pfT();
	return; // KCST_GotoReturn
}
void UABP_TP_C__pf117350442::bpf__ExecuteUbergraph_ABP_TP__pf_1(int32 bpp__EntryPoint__pf)
{
	APawn* bpfv__CallFunc_TryGetPawnOwner_ReturnValue__pf{};
	int32 __CurrentState = bpp__EntryPoint__pf;
	do
	{
		switch( __CurrentState )
		{
		case 22:
			{
				bpfv__CallFunc_TryGetPawnOwner_ReturnValue__pf = TryGetPawnOwner();
				b0l__K2Node_DynamicCast_AsBP_TL4Character__pf = Cast<ABP_TL4Character_C__pf3043094308>(bpfv__CallFunc_TryGetPawnOwner_ReturnValue__pf);
				b0l__K2Node_DynamicCast_bSuccess__pf = (b0l__K2Node_DynamicCast_AsBP_TL4Character__pf != nullptr);;
				if (!b0l__K2Node_DynamicCast_bSuccess__pf)
				{
					__CurrentState = -1;
					break;
				}
			}
		case 23:
			{
				if(::IsValid(b0l__K2Node_DynamicCast_AsBP_TL4Character__pf))
				{
					b0l__K2Node_DynamicCast_AsBP_TL4Character__pf->bpv__JumpxButtonxDown__pfTT = false;
				}
				__CurrentState = -1;
				break;
			}
		case 28:
			{
				__CurrentState = 29;
				break;
			}
		case 29:
			{
				bpv__EnablexJump__pfT = false;
				__CurrentState = 22;
				break;
			}
		case 30:
			{
				__CurrentState = 29;
				break;
			}
		default:
			break;
		}
	} while( __CurrentState != -1 );
}
void UABP_TP_C__pf117350442::bpf__ExecuteUbergraph_ABP_TP__pf_2(int32 bpp__EntryPoint__pf)
{
	APawn* bpfv__CallFunc_TryGetPawnOwner_ReturnValue__pf{};
	check(bpp__EntryPoint__pf == 27);
	// optimized KCST_UnconditionalGoto
	bpfv__CallFunc_TryGetPawnOwner_ReturnValue__pf = TryGetPawnOwner();
	b0l__K2Node_DynamicCast_AsCharacter__pf = Cast<ACharacter>(bpfv__CallFunc_TryGetPawnOwner_ReturnValue__pf);
	b0l__K2Node_DynamicCast_bSuccess1__pf = (b0l__K2Node_DynamicCast_AsCharacter__pf != nullptr);;
	if (!b0l__K2Node_DynamicCast_bSuccess1__pf)
	{
		return; //KCST_GotoReturnIfNot
	}
	if (!bpv__EnablexJump__pfT)
	{
		return; //KCST_GotoReturnIfNot
	}
	if(::IsValid(b0l__K2Node_DynamicCast_AsCharacter__pf))
	{
		b0l__K2Node_DynamicCast_AsCharacter__pf->Jump();
	}
	return; // KCST_GotoReturn
}
void UABP_TP_C__pf117350442::bpf__ExecuteUbergraph_ABP_TP__pf_3(int32 bpp__EntryPoint__pf)
{
	bool bpfv__CallFunc_LessEqual_FloatFloat_ReturnValue4__pf{};
	bool bpfv__CallFunc_BooleanAND_ReturnValue1__pf{};
	check(bpp__EntryPoint__pf == 3);
	// optimized KCST_UnconditionalGoto
	bpfv__CallFunc_LessEqual_FloatFloat_ReturnValue4__pf = UKismetMathLibrary::LessEqual_FloatFloat(bpv__Speed__pf, 10.000000);
	bpfv__CallFunc_BooleanAND_ReturnValue1__pf = UKismetMathLibrary::BooleanAND(bpfv__CallFunc_LessEqual_FloatFloat_ReturnValue4__pf, bpv__EnablexJump__pfT);
	bpv__AnimGraphNode_TransitionResult_3163F8234400D90A8D5A4494801CFE23__pf.bCanEnterTransition = bpfv__CallFunc_BooleanAND_ReturnValue1__pf;
	return; // KCST_GotoReturn
}
void UABP_TP_C__pf117350442::bpf__ExecuteUbergraph_ABP_TP__pf_4(int32 bpp__EntryPoint__pf)
{
	bool bpfv__CallFunc_Greater_FloatFloat_ReturnValue2__pf{};
	bool bpfv__CallFunc_BooleanAND_ReturnValue__pf{};
	check(bpp__EntryPoint__pf == 13);
	// optimized KCST_UnconditionalGoto
	bpfv__CallFunc_Greater_FloatFloat_ReturnValue2__pf = UKismetMathLibrary::Greater_FloatFloat(bpv__Speed__pf, 10.000000);
	bpfv__CallFunc_BooleanAND_ReturnValue__pf = UKismetMathLibrary::BooleanAND(bpfv__CallFunc_Greater_FloatFloat_ReturnValue2__pf, bpv__EnablexJump__pfT);
	bpv__AnimGraphNode_TransitionResult_5540D45C4BCA55AEC05DA1A1E21BFB58__pf.bCanEnterTransition = bpfv__CallFunc_BooleanAND_ReturnValue__pf;
	return; // KCST_GotoReturn
}
void UABP_TP_C__pf117350442::bpf__ExecuteUbergraph_ABP_TP__pf_5(int32 bpp__EntryPoint__pf)
{
	bool bpfv__CallFunc_LessEqual_FloatFloat_ReturnValue3__pf{};
	check(bpp__EntryPoint__pf == 18);
	// optimized KCST_UnconditionalGoto
	bpfv__CallFunc_LessEqual_FloatFloat_ReturnValue3__pf = UKismetMathLibrary::LessEqual_FloatFloat(bpv__Speed__pf, 10.000000);
	bpv__AnimGraphNode_TransitionResult_4AAD4A594EABD4F92E1D44BBD3F4830B__pf.bCanEnterTransition = bpfv__CallFunc_LessEqual_FloatFloat_ReturnValue3__pf;
	return; // KCST_GotoReturn
}
void UABP_TP_C__pf117350442::bpf__ExecuteUbergraph_ABP_TP__pf_6(int32 bpp__EntryPoint__pf)
{
	bool bpfv__CallFunc_LessEqual_FloatFloat_ReturnValue2__pf{};
	check(bpp__EntryPoint__pf == 16);
	// optimized KCST_UnconditionalGoto
	bpfv__CallFunc_LessEqual_FloatFloat_ReturnValue2__pf = UKismetMathLibrary::LessEqual_FloatFloat(bpv__Speed__pf, 10.000000);
	bpv__AnimGraphNode_TransitionResult_315E9BF04EA579C328EB6A866B91C07A__pf.bCanEnterTransition = bpfv__CallFunc_LessEqual_FloatFloat_ReturnValue2__pf;
	return; // KCST_GotoReturn
}
void UABP_TP_C__pf117350442::bpf__ExecuteUbergraph_ABP_TP__pf_7(int32 bpp__EntryPoint__pf)
{
	bool bpfv__CallFunc_Greater_FloatFloat_ReturnValue1__pf{};
	check(bpp__EntryPoint__pf == 11);
	// optimized KCST_UnconditionalGoto
	bpfv__CallFunc_Greater_FloatFloat_ReturnValue1__pf = UKismetMathLibrary::Greater_FloatFloat(bpv__Speed__pf, 10.000000);
	bpv__AnimGraphNode_TransitionResult_F13AB6764EBE8335385B039A555B9993__pf.bCanEnterTransition = bpfv__CallFunc_Greater_FloatFloat_ReturnValue1__pf;
	return; // KCST_GotoReturn
}
void UABP_TP_C__pf117350442::bpf__ExecuteUbergraph_ABP_TP__pf_8(int32 bpp__EntryPoint__pf)
{
	float bpfv__CallFunc_GetInstanceAssetPlayerTimeFromEndFraction_ReturnValue1__pf{};
	bool bpfv__CallFunc_LessEqual_FloatFloat_ReturnValue1__pf{};
	check(bpp__EntryPoint__pf == 7);
	// optimized KCST_UnconditionalGoto
	bpfv__CallFunc_GetInstanceAssetPlayerTimeFromEndFraction_ReturnValue1__pf = UAnimInstance::GetInstanceAssetPlayerTimeFromEndFraction(19);
	bpfv__CallFunc_LessEqual_FloatFloat_ReturnValue1__pf = UKismetMathLibrary::LessEqual_FloatFloat(bpfv__CallFunc_GetInstanceAssetPlayerTimeFromEndFraction_ReturnValue1__pf, 0.100000);
	bpv__AnimGraphNode_TransitionResult_23C30FC54F432C27A8D2E086D1A0B4F4__pf.bCanEnterTransition = bpfv__CallFunc_LessEqual_FloatFloat_ReturnValue1__pf;
	return; // KCST_GotoReturn
}
void UABP_TP_C__pf117350442::bpf__ExecuteUbergraph_ABP_TP__pf_9(int32 bpp__EntryPoint__pf)
{
	bool bpfv__CallFunc_Greater_FloatFloat_ReturnValue__pf{};
	check(bpp__EntryPoint__pf == 10);
	// optimized KCST_UnconditionalGoto
	bpfv__CallFunc_Greater_FloatFloat_ReturnValue__pf = UKismetMathLibrary::Greater_FloatFloat(bpv__Speed__pf, 10.000000);
	bpv__AnimGraphNode_TransitionResult_D25429B644E6C66D625827BA5AE5F04E__pf.bCanEnterTransition = bpfv__CallFunc_Greater_FloatFloat_ReturnValue__pf;
	return; // KCST_GotoReturn
}
void UABP_TP_C__pf117350442::bpf__ExecuteUbergraph_ABP_TP__pf_10(int32 bpp__EntryPoint__pf)
{
	float bpfv__CallFunc_GetInstanceAssetPlayerTimeFromEndFraction_ReturnValue__pf{};
	bool bpfv__CallFunc_LessEqual_FloatFloat_ReturnValue__pf{};
	check(bpp__EntryPoint__pf == 5);
	// optimized KCST_UnconditionalGoto
	bpfv__CallFunc_GetInstanceAssetPlayerTimeFromEndFraction_ReturnValue__pf = UAnimInstance::GetInstanceAssetPlayerTimeFromEndFraction(17);
	bpfv__CallFunc_LessEqual_FloatFloat_ReturnValue__pf = UKismetMathLibrary::LessEqual_FloatFloat(bpfv__CallFunc_GetInstanceAssetPlayerTimeFromEndFraction_ReturnValue__pf, 0.100000);
	bpv__AnimGraphNode_TransitionResult_FFFCF0144598EDEE64EE439BD7BDAFDF__pf.bCanEnterTransition = bpfv__CallFunc_LessEqual_FloatFloat_ReturnValue__pf;
	return; // KCST_GotoReturn
}
void UABP_TP_C__pf117350442::bpf__AnimNotify_JogStart__pf()
{
	bpf__ExecuteUbergraph_ABP_TP__pf_1(30);
}
void UABP_TP_C__pf117350442::bpf__AnimNotify_IdleStart__pf()
{
	bpf__ExecuteUbergraph_ABP_TP__pf_1(28);
}
void UABP_TP_C__pf117350442::bpf__AnimNotify_Jump__pf()
{
	bpf__ExecuteUbergraph_ABP_TP__pf_2(27);
}
void UABP_TP_C__pf117350442::bpf__BlueprintUpdateAnimation__pf(float bpp__DeltaTimeX__pf)
{
	b0l__K2Node_Event_DeltaTimeX__pf = bpp__DeltaTimeX__pf;
	bpf__ExecuteUbergraph_ABP_TP__pf_0(19);
}
void UABP_TP_C__pf117350442::bpf__EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_TP_AnimGraphNode_TransitionResult_4AAD4A594EABD4F92E1D44BBD3F4830B__pf()
{
	bpf__ExecuteUbergraph_ABP_TP__pf_5(18);
}
void UABP_TP_C__pf117350442::bpf__EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_TP_AnimGraphNode_TransitionResult_315E9BF04EA579C328EB6A866B91C07A__pf()
{
	bpf__ExecuteUbergraph_ABP_TP__pf_6(16);
}
void UABP_TP_C__pf117350442::bpf__EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_TP_AnimGraphNode_TransitionResult_5540D45C4BCA55AEC05DA1A1E21BFB58__pf()
{
	bpf__ExecuteUbergraph_ABP_TP__pf_4(13);
}
void UABP_TP_C__pf117350442::bpf__EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_TP_AnimGraphNode_TransitionResult_F13AB6764EBE8335385B039A555B9993__pf()
{
	bpf__ExecuteUbergraph_ABP_TP__pf_7(11);
}
void UABP_TP_C__pf117350442::bpf__EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_TP_AnimGraphNode_TransitionResult_D25429B644E6C66D625827BA5AE5F04E__pf()
{
	bpf__ExecuteUbergraph_ABP_TP__pf_9(10);
}
void UABP_TP_C__pf117350442::bpf__EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_TP_AnimGraphNode_TransitionResult_23C30FC54F432C27A8D2E086D1A0B4F4__pf()
{
	bpf__ExecuteUbergraph_ABP_TP__pf_8(7);
}
void UABP_TP_C__pf117350442::bpf__EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_TP_AnimGraphNode_TransitionResult_3163F8234400D90A8D5A4494801CFE23__pf()
{
	bpf__ExecuteUbergraph_ABP_TP__pf_3(3);
}
void UABP_TP_C__pf117350442::bpf__EvaluateGraphExposedInputs_ExecuteUbergraph_ABP_TP_AnimGraphNode_TransitionResult_FFFCF0144598EDEE64EE439BD7BDAFDF__pf()
{
	bpf__ExecuteUbergraph_ABP_TP__pf_10(5);
}
void UABP_TP_C__pf117350442::bpf__CanxJump__pfT(bool bpp__ShouldxJump__pfT, /*out*/ bool& bpp__bJumping__pf)
{
	bool bpfv__CallFunc_EqualEqual_BoolBool_ReturnValue__pf{};
	bool bpfv__CallFunc_BooleanAND_ReturnValue__pf{};
	bpfv__CallFunc_EqualEqual_BoolBool_ReturnValue__pf = UKismetMathLibrary::EqualEqual_BoolBool(bpv__EnablexJump__pfT, false);
	bpfv__CallFunc_BooleanAND_ReturnValue__pf = UKismetMathLibrary::BooleanAND(bpp__ShouldxJump__pfT, bpfv__CallFunc_EqualEqual_BoolBool_ReturnValue__pf);
	bpp__bJumping__pf = bpfv__CallFunc_BooleanAND_ReturnValue__pf;
}
void UABP_TP_C__pf117350442::bpf__BindxAim__pfT()
{
	bool bpfv__Temp_bool_Variable__pf{};
	APawn* bpfv__CallFunc_TryGetPawnOwner_ReturnValue__pf{};
	FRotator bpfv__CallFunc_GetControlRotation_ReturnValue__pf(EForceInit::ForceInit);
	float bpfv__CallFunc_BreakRotator_Roll__pf{};
	float bpfv__CallFunc_BreakRotator_Pitch__pf{};
	float bpfv__CallFunc_BreakRotator_Yaw__pf{};
	bool bpfv__CallFunc_Greater_FloatFloat_ReturnValue__pf{};
	float bpfv__CallFunc_Subtract_FloatFloat_ReturnValue__pf{};
	float bpfv__K2Node_Select_Default__pf{};
	bpfv__CallFunc_TryGetPawnOwner_ReturnValue__pf = TryGetPawnOwner();
	if(::IsValid(bpfv__CallFunc_TryGetPawnOwner_ReturnValue__pf))
	{
		bpfv__CallFunc_GetControlRotation_ReturnValue__pf = bpfv__CallFunc_TryGetPawnOwner_ReturnValue__pf->APawn::GetControlRotation();
	}
	UKismetMathLibrary::BreakRotator(bpfv__CallFunc_GetControlRotation_ReturnValue__pf, /*out*/ bpfv__CallFunc_BreakRotator_Roll__pf, /*out*/ bpfv__CallFunc_BreakRotator_Pitch__pf, /*out*/ bpfv__CallFunc_BreakRotator_Yaw__pf);
	bpfv__CallFunc_Greater_FloatFloat_ReturnValue__pf = UKismetMathLibrary::Greater_FloatFloat(bpfv__CallFunc_BreakRotator_Pitch__pf, 180.000000);
	bpfv__CallFunc_Subtract_FloatFloat_ReturnValue__pf = UKismetMathLibrary::Subtract_FloatFloat(bpfv__CallFunc_BreakRotator_Pitch__pf, 360.000000);
	bpfv__Temp_bool_Variable__pf = bpfv__CallFunc_Greater_FloatFloat_ReturnValue__pf;
	bpv__AimAngle__pf = TSwitchValue<bool , float >(bpfv__Temp_bool_Variable__pf, bpfv__K2Node_Select_Default__pf, 2, TSwitchPair<bool , float >(false, bpfv__CallFunc_BreakRotator_Pitch__pf), TSwitchPair<bool , float >(true, bpfv__CallFunc_Subtract_FloatFloat_ReturnValue__pf));
}
void UABP_TP_C__pf117350442::bpf__BindxMovement__pfT()
{
	APawn* bpfv__CallFunc_TryGetPawnOwner_ReturnValue__pf{};
	FVector bpfv__CallFunc_GetVelocity_ReturnValue__pf(EForceInit::ForceInit);
	ACharacter* bpfv__K2Node_DynamicCast_AsCharacter__pf{};
	bool bpfv__K2Node_DynamicCast_bSuccess__pf{};
	float bpfv__CallFunc_VSize_ReturnValue__pf{};
	FRotator bpfv__CallFunc_K2_GetActorRotation_ReturnValue__pf(EForceInit::ForceInit);
	float bpfv__CallFunc_CalculateDirection_ReturnValue__pf{};
	int32 __CurrentState = 1;
	do
	{
		switch( __CurrentState )
		{
		case 1:
			{
				bpfv__CallFunc_TryGetPawnOwner_ReturnValue__pf = TryGetPawnOwner();
				bpfv__K2Node_DynamicCast_AsCharacter__pf = Cast<ACharacter>(bpfv__CallFunc_TryGetPawnOwner_ReturnValue__pf);
				bpfv__K2Node_DynamicCast_bSuccess__pf = (bpfv__K2Node_DynamicCast_AsCharacter__pf != nullptr);;
				if (!bpfv__K2Node_DynamicCast_bSuccess__pf)
				{
					__CurrentState = -1;
					break;
				}
			}
		case 2:
			{
				bpfv__CallFunc_TryGetPawnOwner_ReturnValue__pf = TryGetPawnOwner();
				if(::IsValid(bpfv__CallFunc_TryGetPawnOwner_ReturnValue__pf))
				{
					bpfv__CallFunc_GetVelocity_ReturnValue__pf = bpfv__CallFunc_TryGetPawnOwner_ReturnValue__pf->GetVelocity();
				}
				bpfv__CallFunc_VSize_ReturnValue__pf = UKismetMathLibrary::VSize(bpfv__CallFunc_GetVelocity_ReturnValue__pf);
				bpv__Speed__pf = bpfv__CallFunc_VSize_ReturnValue__pf;
			}
		case 3:
			{
				bpfv__CallFunc_TryGetPawnOwner_ReturnValue__pf = TryGetPawnOwner();
				if(::IsValid(bpfv__CallFunc_TryGetPawnOwner_ReturnValue__pf))
				{
					bpfv__CallFunc_GetVelocity_ReturnValue__pf = bpfv__CallFunc_TryGetPawnOwner_ReturnValue__pf->GetVelocity();
				}
				if(::IsValid(bpfv__CallFunc_TryGetPawnOwner_ReturnValue__pf))
				{
					bpfv__CallFunc_K2_GetActorRotation_ReturnValue__pf = bpfv__CallFunc_TryGetPawnOwner_ReturnValue__pf->AActor::K2_GetActorRotation();
				}
				bpfv__CallFunc_CalculateDirection_ReturnValue__pf = UAnimInstance::CalculateDirection(bpfv__CallFunc_GetVelocity_ReturnValue__pf, bpfv__CallFunc_K2_GetActorRotation_ReturnValue__pf);
				bpv__Direction__pf = bpfv__CallFunc_CalculateDirection_ReturnValue__pf;
				__CurrentState = -1;
				break;
			}
		default:
			break;
		}
	} while( __CurrentState != -1 );
}
void UABP_TP_C__pf117350442::bpf__GetxPawnxOwnerxasxNPCxGuard__pfTTTTT(/*out*/ ABP_TL4Character_C__pf3043094308*& bpp__AsNPCxGuard__pfT)
{
	APawn* bpfv__CallFunc_TryGetPawnOwner_ReturnValue__pf{};
	ABP_TL4Character_C__pf3043094308* bpfv__K2Node_DynamicCast_AsBP_TL4Character__pf{};
	bool bpfv__K2Node_DynamicCast_bSuccess__pf{};
	int32 __CurrentState = 1;
	do
	{
		switch( __CurrentState )
		{
		case 1:
			{
				bpfv__CallFunc_TryGetPawnOwner_ReturnValue__pf = TryGetPawnOwner();
				bpfv__K2Node_DynamicCast_AsBP_TL4Character__pf = Cast<ABP_TL4Character_C__pf3043094308>(bpfv__CallFunc_TryGetPawnOwner_ReturnValue__pf);
				bpfv__K2Node_DynamicCast_bSuccess__pf = (bpfv__K2Node_DynamicCast_AsBP_TL4Character__pf != nullptr);;
				if (!bpfv__K2Node_DynamicCast_bSuccess__pf)
				{
					__CurrentState = -1;
					break;
				}
			}
		case 2:
			{
				bpp__AsNPCxGuard__pfT = bpfv__K2Node_DynamicCast_AsBP_TL4Character__pf;
				__CurrentState = -1;
				break;
			}
		default:
			break;
		}
	} while( __CurrentState != -1 );
}
void UABP_TP_C__pf117350442::bpf__BindxJump__pfT()
{
	ABP_TL4Character_C__pf3043094308* bpfv__CallFunc_Get_Pawn_Owner_as_NPC_Guard_AsNPC_Guard__pf{};
	bool bpfv__CallFunc_Can_Jump_bJumping__pf{};
	int32 __CurrentState = 1;
	do
	{
		switch( __CurrentState )
		{
		case 1:
			{
				bpf__GetxPawnxOwnerxasxNPCxGuard__pfTTTTT(/*out*/ bpfv__CallFunc_Get_Pawn_Owner_as_NPC_Guard_AsNPC_Guard__pf);
			}
		case 2:
			{
				bool  __Local__58 = false;
				bpf__CanxJump__pfT(((::IsValid(bpfv__CallFunc_Get_Pawn_Owner_as_NPC_Guard_AsNPC_Guard__pf)) ? (bpfv__CallFunc_Get_Pawn_Owner_as_NPC_Guard_AsNPC_Guard__pf->bpv__JumpxButtonxDown__pfTT) : (__Local__58)), /*out*/ bpfv__CallFunc_Can_Jump_bJumping__pf);
			}
		case 3:
			{
				if (!bpfv__CallFunc_Can_Jump_bJumping__pf)
				{
					__CurrentState = -1;
					break;
				}
			}
		case 4:
			{
				bpv__EnablexJump__pfT = true;
				__CurrentState = -1;
				break;
			}
		default:
			break;
		}
	} while( __CurrentState != -1 );
}
void UABP_TP_C__pf117350442::bpf__BindxNPCxGuardxState__pfTTT()
{
	ABP_TL4Character_C__pf3043094308* bpfv__CallFunc_Get_Pawn_Owner_as_NPC_Guard_AsNPC_Guard__pf{};
	bool bpfv__CallFunc_Is_Dead_Dead__pf{};
	TArray< int32, TInlineAllocator<8> > __StateStack;

	int32 __CurrentState = 1;
	do
	{
		switch( __CurrentState )
		{
		case 1:
			{
				bpf__GetxPawnxOwnerxasxNPCxGuard__pfTTTTT(/*out*/ bpfv__CallFunc_Get_Pawn_Owner_as_NPC_Guard_AsNPC_Guard__pf);
			}
		case 2:
			{
				__StateStack.Push(5);
				__StateStack.Push(4);
			}
		case 3:
			{
				bool  __Local__59 = false;
				bpv__Crouching__pf = ((::IsValid(bpfv__CallFunc_Get_Pawn_Owner_as_NPC_Guard_AsNPC_Guard__pf)) ? (bpfv__CallFunc_Get_Pawn_Owner_as_NPC_Guard_AsNPC_Guard__pf->bpv__CrouchxButtonxDown__pfTT) : (__Local__59));
				__CurrentState = (__StateStack.Num() > 0) ? __StateStack.Pop(/*bAllowShrinking=*/ false) : -1;
				break;
			}
		case 4:
			{
				bool  __Local__60 = false;
				bpv__Aiming__pf = ((::IsValid(bpfv__CallFunc_Get_Pawn_Owner_as_NPC_Guard_AsNPC_Guard__pf)) ? (bpfv__CallFunc_Get_Pawn_Owner_as_NPC_Guard_AsNPC_Guard__pf->bpv__Aiming__pf) : (__Local__60));
				__CurrentState = (__StateStack.Num() > 0) ? __StateStack.Pop(/*bAllowShrinking=*/ false) : -1;
				break;
			}
		case 5:
			{
				if(::IsValid(bpfv__CallFunc_Get_Pawn_Owner_as_NPC_Guard_AsNPC_Guard__pf))
				{
					bpfv__CallFunc_Get_Pawn_Owner_as_NPC_Guard_AsNPC_Guard__pf->bpf__IsxDead__pfT(/*out*/ bpfv__CallFunc_Is_Dead_Dead__pf);
				}
				bpv__IsxDead__pfT = bpfv__CallFunc_Is_Dead_Dead__pf;
				__CurrentState = (__StateStack.Num() > 0) ? __StateStack.Pop(/*bAllowShrinking=*/ false) : -1;
				break;
			}
		default:
			check(false); // Invalid state
			break;
		}
	} while( __CurrentState != -1 );
}
PRAGMA_DISABLE_OPTIMIZATION
void UABP_TP_C__pf117350442::__StaticDependencies_DirectlyUsedAssets(TArray<FBlueprintDependencyData>& AssetsToLoad)
{
	const FCompactBlueprintDependencyData LocCompactBlueprintDependencyData[] =
	{
		{97, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  BlendSpace /Game/Dynamic/TL4Character/Animations/TPClips/BS_CrouchWalk.BS_CrouchWalk 
		{98, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  AnimSequence /Game/Dynamic/TL4Character/Animations/TPClips/Crouch_Idle_Rifle_Hip.Crouch_Idle_Rifle_Hip 
		{99, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  AnimSequence /Game/Dynamic/TL4Character/Animations/TPClips/Idle_Rifle_Ironsights.Idle_Rifle_Ironsights 
		{100, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  AnimSequence /Game/Dynamic/TL4Character/Animations/TPClips/Idle_Rifle_Hip.Idle_Rifle_Hip 
		{101, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  AnimSequence /Game/Dynamic/TL4Character/Animations/TPClips/Jump_From_Jog.Jump_From_Jog 
		{102, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  AnimSequence /Game/Dynamic/TL4Character/Animations/TPClips/Jump_From_Stand.Jump_From_Stand 
		{103, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  BlendSpace /Game/Dynamic/TL4Character/Animations/TPClips/BS_AimingWalk.BS_AimingWalk 
		{104, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  BlendSpace /Game/Dynamic/TL4Character/Animations/TPClips/BS_Jog.BS_Jog 
		{105, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  AimOffsetBlendSpace1D /Game/Dynamic/TL4Character/Animations/AimOffsets/AimOffset.AimOffset 
		{106, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  AnimSequence /Game/Dynamic/TL4Character/Animations/TPClips/Death_1.Death_1 
		{107, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  Skeleton /Game/Static/NPC/Character/Mesh/UE4_Mannequin_Skeleton.UE4_Mannequin_Skeleton 
	};
	for(const FCompactBlueprintDependencyData& CompactData : LocCompactBlueprintDependencyData)
	{
		AssetsToLoad.Add(FBlueprintDependencyData(F__NativeDependencies::Get(CompactData.ObjectRefIndex), CompactData));
	}
}
PRAGMA_ENABLE_OPTIMIZATION
PRAGMA_DISABLE_OPTIMIZATION
void UABP_TP_C__pf117350442::__StaticDependenciesAssets(TArray<FBlueprintDependencyData>& AssetsToLoad)
{
	__StaticDependencies_DirectlyUsedAssets(AssetsToLoad);
	const FCompactBlueprintDependencyData LocCompactBlueprintDependencyData[] =
	{
		{67, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  Class /Script/Engine.Pawn 
		{32, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  Class /Script/Engine.Character 
		{3, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  Class /Script/Engine.KismetMathLibrary 
		{87, FBlueprintDependencyType(true, false, false, false), FBlueprintDependencyType(false, false, false, false)},  //  ScriptStruct /Script/Engine.AnimNode_TransitionResult 
		{86, FBlueprintDependencyType(true, false, false, false), FBlueprintDependencyType(false, false, false, false)},  //  Class /Script/Engine.AnimInstance 
		{4, FBlueprintDependencyType(true, false, false, false), FBlueprintDependencyType(false, false, false, false)},  //  ScriptStruct /Script/Engine.PointerToUberGraphFrame 
		{5, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  Class /Script/Engine.Actor 
		{88, FBlueprintDependencyType(true, false, false, false), FBlueprintDependencyType(false, false, false, false)},  //  ScriptStruct /Script/AnimGraphRuntime.AnimNode_Root 
		{108, FBlueprintDependencyType(true, false, false, false), FBlueprintDependencyType(false, false, false, false)},  //  ScriptStruct /Script/AnimGraphRuntime.AnimNode_BlendSpacePlayer 
		{90, FBlueprintDependencyType(true, false, false, false), FBlueprintDependencyType(false, false, false, false)},  //  ScriptStruct /Script/AnimGraphRuntime.AnimNode_StateResult 
		{89, FBlueprintDependencyType(true, false, false, false), FBlueprintDependencyType(false, false, false, false)},  //  ScriptStruct /Script/Engine.AnimNode_SequencePlayer 
		{109, FBlueprintDependencyType(true, false, false, false), FBlueprintDependencyType(false, false, false, false)},  //  ScriptStruct /Script/AnimGraphRuntime.AnimNode_BlendListByBool 
		{91, FBlueprintDependencyType(true, false, false, false), FBlueprintDependencyType(false, false, false, false)},  //  ScriptStruct /Script/Engine.AnimNode_StateMachine 
		{110, FBlueprintDependencyType(true, false, false, false), FBlueprintDependencyType(false, false, false, false)},  //  ScriptStruct /Script/AnimGraphRuntime.AnimNode_RotationOffsetBlendSpace 
		{93, FBlueprintDependencyType(true, false, false, false), FBlueprintDependencyType(false, false, false, false)},  //  ScriptStruct /Script/AnimGraphRuntime.AnimNode_TwoBoneIK 
		{94, FBlueprintDependencyType(true, false, false, false), FBlueprintDependencyType(false, false, false, false)},  //  ScriptStruct /Script/Engine.AnimNode_ConvertLocalToComponentSpace 
		{95, FBlueprintDependencyType(true, false, false, false), FBlueprintDependencyType(false, false, false, false)},  //  ScriptStruct /Script/Engine.AnimNode_ConvertComponentToLocalSpace 
		{111, FBlueprintDependencyType(true, false, false, false), FBlueprintDependencyType(false, false, false, false)},  //  ScriptStruct /Script/AnimGraphRuntime.AnimNode_LayeredBoneBlend 
		{112, FBlueprintDependencyType(true, false, false, false), FBlueprintDependencyType(false, false, false, false)},  //  ScriptStruct /Script/Engine.AnimNode_SaveCachedPose 
		{113, FBlueprintDependencyType(true, false, false, false), FBlueprintDependencyType(false, false, false, false)},  //  ScriptStruct /Script/Engine.AnimNode_UseCachedPose 
		{92, FBlueprintDependencyType(true, false, false, false), FBlueprintDependencyType(false, false, false, false)},  //  ScriptStruct /Script/AnimGraphRuntime.AnimNode_Slot 
		{96, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  Class /Script/Engine.SkeletalMeshComponent 
		{16, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  BlueprintGeneratedClass /Game/Dynamic/TL4Character/Behaviour/BP_TL4Character.BP_TL4Character_C 
	};
	for(const FCompactBlueprintDependencyData& CompactData : LocCompactBlueprintDependencyData)
	{
		AssetsToLoad.Add(FBlueprintDependencyData(F__NativeDependencies::Get(CompactData.ObjectRefIndex), CompactData));
	}
}
PRAGMA_ENABLE_OPTIMIZATION
struct FRegisterHelper__UABP_TP_C__pf117350442
{
	FRegisterHelper__UABP_TP_C__pf117350442()
	{
		FConvertedBlueprintsDependencies::Get().RegisterConvertedClass(TEXT("/Game/Dynamic/TL4Character/Animations/ABP_TP"), &UABP_TP_C__pf117350442::__StaticDependenciesAssets);
	}
	static FRegisterHelper__UABP_TP_C__pf117350442 Instance;
};
FRegisterHelper__UABP_TP_C__pf117350442 FRegisterHelper__UABP_TP_C__pf117350442::Instance;
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif

#pragma once
#include "Blueprint/BlueprintSupport.h"
#include "Runtime/UMG/Public/Blueprint/UserWidget.h"
class UButton;
class UWidgetSwitcher;
class UImage;
#include "WBP_MainMenu_TL3__pf3389175594.generated.h"
UCLASS(config=Engine, Blueprintable, BlueprintType, meta=(ReplaceConverted="/Game/Dynamic/UI/WBP_MainMenu_TL3.WBP_MainMenu_TL3_C", OverrideNativeName="WBP_MainMenu_TL3_C"))
class UWBP_MainMenu_TL3_C__pf3389175594 : public UUserWidget
{
public:
	GENERATED_BODY()
	UPROPERTY(BlueprintReadWrite, Export, meta=(DisplayName="Background", Category="WBP_MainMenu_TL3", OverrideNativeName="Background"))
	UImage* bpv__Background__pf;
	UPROPERTY(BlueprintReadWrite, Export, meta=(DisplayName="Image_0", Category="WBP_MainMenu_TL3", OverrideNativeName="Image_0"))
	UImage* bpv__Image_0__pf;
	UPROPERTY(BlueprintReadWrite, Export, meta=(DisplayName="LoadingScreenSwitcher", Category="WBP_MainMenu_TL3", OverrideNativeName="LoadingScreenSwitcher"))
	UWidgetSwitcher* bpv__LoadingScreenSwitcher__pf;
	UPROPERTY(BlueprintReadWrite, Export, meta=(DisplayName="Quit Button", Category="WBP_MainMenu_TL3", OverrideNativeName="QuitButton"))
	UButton* bpv__QuitButton__pf;
	UPROPERTY(BlueprintReadWrite, Export, meta=(DisplayName="Start Button", Category="WBP_MainMenu_TL3", OverrideNativeName="StartButton"))
	UButton* bpv__StartButton__pf;
	UWBP_MainMenu_TL3_C__pf3389175594(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());
	virtual void PostLoadSubobjects(FObjectInstancingGraph* OuterInstanceGraph) override;
	static void __CustomDynamicClassInitialization(UDynamicClass* InDynamicClass);
	static void __StaticDependenciesAssets(TArray<FBlueprintDependencyData>& AssetsToLoad);
	static void __StaticDependencies_DirectlyUsedAssets(TArray<FBlueprintDependencyData>& AssetsToLoad);
	void bpf__ExecuteUbergraph_WBP_MainMenu_TL3__pf_0(int32 bpp__EntryPoint__pf);
	UFUNCTION(meta=(OverrideNativeName="ExecuteUbergraph_WBP_MainMenu_TL3_1"))
	void bpf__ExecuteUbergraph_WBP_MainMenu_TL3__pf_1(int32 bpp__EntryPoint__pf);
	void bpf__ExecuteUbergraph_WBP_MainMenu_TL3__pf_2(int32 bpp__EntryPoint__pf);
	void bpf__ExecuteUbergraph_WBP_MainMenu_TL3__pf_3(int32 bpp__EntryPoint__pf);
	UFUNCTION(BlueprintCosmetic, meta=(Category="User Interface", Keywords="Begin Play", ToolTip="Called after the underlying slate widget is constructed.  Depending on how the slate object is usedthis event may be called multiple times due to adding and removing from the hierarchy.If you need a true called-once-when-created event, use OnInitialized.", CppFromBpEvent, OverrideNativeName="Construct"))
	virtual void bpf__Construct__pf();
	UFUNCTION(meta=(OverrideNativeName="BndEvt__QuitButton_K2Node_ComponentBoundEvent_158_OnButtonReleasedEvent__DelegateSignature"))
	virtual void bpf__BndEvt__QuitButton_K2Node_ComponentBoundEvent_158_OnButtonReleasedEvent__DelegateSignature__pf();
	UFUNCTION(BlueprintCallable, meta=(Category, OverrideNativeName="WidgetReady"))
	virtual void bpf__WidgetReady__pf();
	UFUNCTION(meta=(OverrideNativeName="BndEvt__Button_0_K2Node_ComponentBoundEvent_80_OnButtonClickedEvent__DelegateSignature"))
	virtual void bpf__BndEvt__Button_0_K2Node_ComponentBoundEvent_80_OnButtonClickedEvent__DelegateSignature__pf();
public:
	virtual void GetSlotNames(TArray<FName>& SlotNames) const override;
	virtual void PreSave(const class ITargetPlatform* TargetPlatform) override;
	virtual void InitializeNativeClassData() override;
};

#pragma once
#include "Blueprint/BlueprintSupport.h"
#include "Runtime/AIModule/Classes/BehaviorTree/BehaviorTreeTypes.h"
#include "Runtime/AIModule/Classes/BehaviorTree/Services/BTService_BlueprintBase.h"
class AActor;
#include "BTTask_UpdateLastSeenLocation__pf2310560554.generated.h"
UCLASS(config=Game, Blueprintable, BlueprintType, meta=(ReplaceConverted="/Game/Dynamic/TL4Character/AI/BTTask_UpdateLastSeenLocation.BTTask_UpdateLastSeenLocation_C", OverrideNativeName="BTTask_UpdateLastSeenLocation_C"))
class UBTTask_UpdateLastSeenLocation_C__pf2310560554 : public UBTService_BlueprintBase
{
public:
	GENERATED_BODY()
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(DisplayName="Last Seen Location Key", Category="Default", OverrideNativeName="LastSeenLocationKey"))
	FBlackboardKeySelector bpv__LastSeenLocationKey__pf;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(DisplayName="Actor Key", Category="Default", OverrideNativeName="ActorKey"))
	FBlackboardKeySelector bpv__ActorKey__pf;
	UPROPERTY(Transient, DuplicateTransient, meta=(OverrideNativeName="K2Node_Event_OwnerActor"))
	AActor* b0l__K2Node_Event_OwnerActor__pf;
	UPROPERTY(Transient, DuplicateTransient, meta=(OverrideNativeName="K2Node_Event_DeltaSeconds"))
	float b0l__K2Node_Event_DeltaSeconds__pf;
	UBTTask_UpdateLastSeenLocation_C__pf2310560554(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());
	virtual void PostLoadSubobjects(FObjectInstancingGraph* OuterInstanceGraph) override;
	static void __CustomDynamicClassInitialization(UDynamicClass* InDynamicClass);
	static void __StaticDependenciesAssets(TArray<FBlueprintDependencyData>& AssetsToLoad);
	static void __StaticDependencies_DirectlyUsedAssets(TArray<FBlueprintDependencyData>& AssetsToLoad);
	void bpf__ExecuteUbergraph_BTTask_UpdateLastSeenLocation__pf_0(int32 bpp__EntryPoint__pf);
	UFUNCTION(meta=(ToolTip="tick function    @Note that if both generic and AI event versions are implemented only the more    suitable one will be called, meaning the AI version if called for AI, generic one otherwise", CppFromBpEvent, OverrideNativeName="ReceiveTick"))
	virtual void bpf__ReceiveTick__pf(AActor* bpp__OwnerActor__pf, float bpp__DeltaSeconds__pf);
public:
};

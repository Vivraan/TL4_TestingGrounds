#pragma once
#include "Blueprint/BlueprintSupport.h"
#include "Runtime/UMG/Public/Blueprint/UserWidget.h"
#include "WBP_Resetting__pf3389175594.generated.h"
UCLASS(config=Engine, Blueprintable, BlueprintType, meta=(ReplaceConverted="/Game/Dynamic/UI/WBP_Resetting.WBP_Resetting_C", OverrideNativeName="WBP_Resetting_C"))
class UWBP_Resetting_C__pf3389175594 : public UUserWidget
{
public:
	GENERATED_BODY()
	UWBP_Resetting_C__pf3389175594(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());
	virtual void PostLoadSubobjects(FObjectInstancingGraph* OuterInstanceGraph) override;
	static void __CustomDynamicClassInitialization(UDynamicClass* InDynamicClass);
	static void __StaticDependenciesAssets(TArray<FBlueprintDependencyData>& AssetsToLoad);
	static void __StaticDependencies_DirectlyUsedAssets(TArray<FBlueprintDependencyData>& AssetsToLoad);
	UFUNCTION(BlueprintCallable, BlueprintPure, meta=(Category, OverrideNativeName="GetScore"))
	virtual FText  bpf__GetScore__pf();
	UFUNCTION(BlueprintCallable, BlueprintPure, meta=(Category, OverrideNativeName="GetHealthPercent"))
	virtual float  bpf__GetHealthPercent__pf();
	UFUNCTION(BlueprintCallable, BlueprintPure, meta=(Category, OverrideNativeName="GetHealthText"))
	virtual FText  bpf__GetHealthText__pf();
public:
	virtual void GetSlotNames(TArray<FName>& SlotNames) const override;
	virtual void PreSave(const class ITargetPlatform* TargetPlatform) override;
	virtual void InitializeNativeClassData() override;
};

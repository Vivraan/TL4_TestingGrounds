#pragma once
#include "Blueprint/BlueprintSupport.h"
#include "Runtime/AIModule/Classes/BehaviorTree/BehaviorTreeTypes.h"
#include "Runtime/AIModule/Classes/BehaviorTree/Decorators/BTDecorator_BlueprintBase.h"
class AAIController;
class APawn;
#include "BTDecorator___pf2310560554.generated.h"
UCLASS(config=Game, Blueprintable, BlueprintType, meta=(ReplaceConverted="/Game/Dynamic/TL4Character/AI/BTDecorator_.BTDecorator__C", OverrideNativeName="BTDecorator__C"))
class UBTDecorator__C__pf2310560554 : public UBTDecorator_BlueprintBase
{
public:
	GENERATED_BODY()
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(DisplayName="Target", Category="Default", OverrideNativeName="Target"))
	FBlackboardKeySelector bpv__Target__pf;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(DisplayName="Range", Category="Default", OverrideNativeName="Range"))
	float bpv__Range__pf;
	UBTDecorator__C__pf2310560554(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());
	virtual void PostLoadSubobjects(FObjectInstancingGraph* OuterInstanceGraph) override;
	static void __CustomDynamicClassInitialization(UDynamicClass* InDynamicClass);
	static void __StaticDependenciesAssets(TArray<FBlueprintDependencyData>& AssetsToLoad);
	static void __StaticDependencies_DirectlyUsedAssets(TArray<FBlueprintDependencyData>& AssetsToLoad);
	UFUNCTION(BlueprintCallable, meta=(Category="AI", ToolTip="Alternative AI version of ReceiveConditionCheck    @see ReceiveConditionCheck for more details    @Note that if both generic and AI event versions are implemented only the more    suitable one will be called, meaning the AI version if called for AI, generic one otherwise", CppFromBpEvent, OverrideNativeName="PerformConditionCheckAI"))
	virtual bool  bpf__PerformConditionCheckAI__pf(AAIController* bpp__OwnerController__pf, APawn* bpp__ControlledPawn__pf);
public:
};

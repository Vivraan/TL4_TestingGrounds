#include "BP_TL4Character__pf3043094308.h"
#include "GeneratedCodeHelpers.h"
#include "Runtime/Engine/Classes/Engine/InputAxisDelegateBinding.h"
#include "Runtime/Engine/Classes/Engine/InputKeyDelegateBinding.h"
#include "Runtime/Engine/Classes/Engine/InputActionDelegateBinding.h"
#include "Runtime/Engine/Classes/Engine/TimelineTemplate.h"
#include "Runtime/Engine/Classes/Curves/CurveFloat.h"
#include "Runtime/Engine/Classes/Engine/SimpleConstructionScript.h"
#include "Runtime/Engine/Classes/Engine/SCS_Node.h"
#include "TL4_TestingGrounds/NPC/PatrolRoute.h"
#include "Runtime/Engine/Classes/Components/ActorComponent.h"
#include "Runtime/CoreUObject/Public/UObject/NoExportTypes.h"
#include "Runtime/Engine/Classes/Engine/EngineBaseTypes.h"
#include "Runtime/Engine/Classes/Engine/AssetUserData.h"
#include "Runtime/Engine/Classes/EdGraph/EdGraphPin.h"
#include "Runtime/Engine/Classes/GameFramework/Actor.h"
#include "Runtime/Engine/Classes/Engine/EngineTypes.h"
#include "Runtime/Engine/Classes/Engine/NetSerialization.h"
#include "Runtime/Engine/Classes/Components/SceneComponent.h"
#include "Runtime/Engine/Classes/GameFramework/PhysicsVolume.h"
#include "Runtime/Engine/Classes/GameFramework/Volume.h"
#include "Runtime/Engine/Classes/Engine/Brush.h"
#include "Runtime/Engine/Classes/Components/BrushComponent.h"
#include "Runtime/Engine/Classes/Components/PrimitiveComponent.h"
#include "Runtime/Engine/Classes/PhysicsEngine/BodyInstance.h"
#include "Runtime/Engine/Classes/PhysicalMaterials/PhysicalMaterial.h"
#include "Runtime/Engine/Classes/PhysicsEngine/PhysicsSettingsEnums.h"
#include "Runtime/Engine/Classes/PhysicalMaterials/PhysicalMaterialPropertyBase.h"
#include "Runtime/Engine/Classes/Vehicles/TireType.h"
#include "Runtime/Engine/Classes/Engine/DataAsset.h"
#include "Runtime/Engine/Classes/Materials/MaterialInterface.h"
#include "Runtime/Engine/Classes/Engine/SubsurfaceProfile.h"
#include "Runtime/Engine/Classes/Materials/Material.h"
#include "Runtime/Engine/Public/MaterialShared.h"
#include "Runtime/Engine/Classes/Materials/MaterialExpression.h"
#include "Runtime/Engine/Classes/Materials/MaterialFunction.h"
#include "Runtime/Engine/Classes/Materials/MaterialFunctionInterface.h"
#include "Runtime/Engine/Classes/Materials/MaterialParameterCollection.h"
#include "Runtime/Engine/Classes/Engine/BlendableInterface.h"
#include "Runtime/Engine/Classes/Engine/Texture.h"
#include "Runtime/Engine/Classes/Engine/TextureDefines.h"
#include "Runtime/Engine/Classes/Interfaces/Interface_AssetUserData.h"
#include "Runtime/Engine/Classes/Materials/MaterialInstanceDynamic.h"
#include "Runtime/Engine/Classes/Materials/MaterialInstance.h"
#include "Runtime/Engine/Classes/Materials/MaterialLayersFunctions.h"
#include "Runtime/Engine/Classes/Engine/Font.h"
#include "Runtime/Engine/Classes/Engine/Texture2D.h"
#include "Runtime/Engine/Classes/Engine/FontImportOptions.h"
#include "Runtime/SlateCore/Public/Fonts/CompositeFont.h"
#include "Runtime/SlateCore/Public/Fonts/FontProviderInterface.h"
#include "Runtime/Engine/Classes/Materials/MaterialInstanceBasePropertyOverrides.h"
#include "Runtime/Engine/Public/StaticParameterSet.h"
#include "Runtime/Engine/Classes/GameFramework/Pawn.h"
#include "Runtime/Engine/Classes/GameFramework/Controller.h"
#include "Runtime/Engine/Classes/GameFramework/PlayerState.h"
#include "Runtime/Engine/Classes/GameFramework/Info.h"
#include "Runtime/Engine/Classes/Components/BillboardComponent.h"
#include "Runtime/Engine/Classes/GameFramework/LocalMessage.h"
#include "Runtime/Engine/Classes/GameFramework/OnlineReplStructs.h"
#include "Runtime/CoreUObject/Public/UObject/CoreOnline.h"
#include "Runtime/Engine/Classes/GameFramework/EngineMessage.h"
#include "Runtime/Engine/Classes/GameFramework/DamageType.h"
#include "Runtime/Engine/Classes/GameFramework/Character.h"
#include "Runtime/Engine/Classes/Components/SkeletalMeshComponent.h"
#include "Runtime/Engine/Classes/Components/SkinnedMeshComponent.h"
#include "Runtime/Engine/Classes/Components/MeshComponent.h"
#include "Runtime/Engine/Classes/Engine/SkeletalMesh.h"
#include "Runtime/Engine/Classes/Animation/Skeleton.h"
#include "Runtime/Engine/Classes/Engine/SkeletalMeshSocket.h"
#include "Runtime/Engine/Classes/Animation/SmartName.h"
#include "Runtime/Engine/Classes/Animation/BlendProfile.h"
#include "Runtime/Engine/Public/BoneContainer.h"
#include "Runtime/Engine/Classes/Interfaces/Interface_PreviewMeshProvider.h"
#include "Runtime/Engine/Public/Components.h"
#include "Runtime/Engine/Public/PerPlatformProperties.h"
#include "Runtime/Engine/Public/SkeletalMeshReductionSettings.h"
#include "Runtime/Engine/Classes/Animation/AnimSequence.h"
#include "Runtime/Engine/Classes/Animation/AnimSequenceBase.h"
#include "Runtime/Engine/Classes/Animation/AnimationAsset.h"
#include "Runtime/Engine/Classes/Animation/AnimMetaData.h"
#include "Runtime/Engine/Public/Animation/AnimTypes.h"
#include "Runtime/Engine/Classes/Animation/AnimLinkableElement.h"
#include "Runtime/Engine/Classes/Animation/AnimMontage.h"
#include "Runtime/Engine/Classes/Animation/AnimCompositeBase.h"
#include "Runtime/Engine/Public/AlphaBlend.h"
#include "Runtime/Engine/Classes/Curves/CurveBase.h"
#include "Runtime/Engine/Classes/Curves/RichCurve.h"
#include "Runtime/Engine/Classes/Curves/IndexedCurve.h"
#include "Runtime/Engine/Classes/Curves/KeyHandle.h"
#include "Runtime/Engine/Classes/Animation/AnimEnums.h"
#include "Runtime/Engine/Classes/Animation/TimeStretchCurve.h"
#include "Runtime/Engine/Classes/Animation/AnimNotifies/AnimNotify.h"
#include "Runtime/Engine/Classes/Animation/AnimNotifies/AnimNotifyState.h"
#include "Runtime/Engine/Public/Animation/AnimCurveTypes.h"
#include "Runtime/Engine/Classes/PhysicsEngine/BodySetup.h"
#include "Runtime/Engine/Classes/PhysicsEngine/AggregateGeom.h"
#include "Runtime/Engine/Classes/PhysicsEngine/SphereElem.h"
#include "Runtime/Engine/Classes/PhysicsEngine/ShapeElem.h"
#include "Runtime/Engine/Classes/PhysicsEngine/BoxElem.h"
#include "Runtime/Engine/Classes/PhysicsEngine/SphylElem.h"
#include "Runtime/Engine/Classes/PhysicsEngine/ConvexElem.h"
#include "Runtime/Engine/Classes/PhysicsEngine/TaperedCapsuleElem.h"
#include "Runtime/Engine/Classes/PhysicsEngine/BodySetupEnums.h"
#include "Runtime/Engine/Classes/PhysicsEngine/PhysicsAsset.h"
#include "Runtime/Engine/Classes/PhysicsEngine/PhysicalAnimationComponent.h"
#include "Runtime/Engine/Classes/PhysicsEngine/PhysicsConstraintTemplate.h"
#include "Runtime/Engine/Classes/PhysicsEngine/ConstraintInstance.h"
#include "Runtime/Engine/Classes/PhysicsEngine/ConstraintTypes.h"
#include "Runtime/Engine/Classes/PhysicsEngine/ConstraintDrives.h"
#include "Runtime/Engine/Classes/EditorFramework/ThumbnailInfo.h"
#include "Runtime/Engine/Public/Animation/NodeMappingContainer.h"
#include "Runtime/Engine/Public/Animation/NodeMappingProviderInterface.h"
#include "Runtime/Engine/Classes/Animation/MorphTarget.h"
#include "Runtime/Engine/Classes/Animation/AnimInstance.h"
#include "Runtime/Engine/Public/Animation/AnimNotifyQueue.h"
#include "Runtime/Engine/Public/Animation/PoseSnapshot.h"
#include "Runtime/ClothingSystemRuntimeInterface/Public/ClothingAssetInterface.h"
#include "Runtime/Engine/Classes/Engine/SkeletalMeshSampling.h"
#include "Runtime/Engine/Classes/Engine/SkeletalMeshLODSettings.h"
#include "Runtime/Engine/Classes/Engine/Blueprint.h"
#include "Runtime/Engine/Classes/Engine/BlueprintCore.h"
#include "Runtime/Engine/Classes/Engine/BlueprintGeneratedClass.h"
#include "Runtime/Engine/Classes/Curves/CurveVector.h"
#include "Runtime/Engine/Classes/Curves/CurveLinearColor.h"
#include "Runtime/Engine/Classes/Engine/InheritableComponentHandler.h"
#include "Runtime/Engine/Classes/Interfaces/Interface_CollisionDataProvider.h"
#include "Runtime/Engine/Classes/Animation/AnimBlueprintGeneratedClass.h"
#include "Runtime/Engine/Classes/Engine/DynamicBlueprintBinding.h"
#include "Runtime/Engine/Classes/Animation/AnimStateMachineTypes.h"
#include "Runtime/Engine/Classes/Animation/AnimClassInterface.h"
#include "Runtime/Engine/Public/SingleAnimationPlayData.h"
#include "Runtime/ClothingSystemRuntimeInterface/Public/ClothingSimulationFactoryInterface.h"
#include "Runtime/ClothingSystemRuntimeInterface/Public/ClothingSimulationInteractor.h"
#include "Runtime/Engine/Classes/GameFramework/CharacterMovementComponent.h"
#include "Runtime/Engine/Classes/GameFramework/PawnMovementComponent.h"
#include "Runtime/Engine/Classes/GameFramework/NavMovementComponent.h"
#include "Runtime/Engine/Classes/GameFramework/MovementComponent.h"
#include "Runtime/Engine/Classes/AI/Navigation/NavigationTypes.h"
#include "Runtime/Engine/Classes/AI/Navigation/NavigationAvoidanceTypes.h"
#include "Runtime/Engine/Classes/GameFramework/RootMotionSource.h"
#include "Runtime/Engine/Public/AI/RVOAvoidanceInterface.h"
#include "Runtime/Engine/Classes/Interfaces/NetworkPredictionInterface.h"
#include "Runtime/Engine/Classes/Components/CapsuleComponent.h"
#include "Runtime/Engine/Classes/Components/ShapeComponent.h"
#include "Runtime/Engine/Classes/AI/Navigation/NavAreaBase.h"
#include "Runtime/AIModule/Classes/AIController.h"
#include "Runtime/AIModule/Classes/Navigation/PathFollowingComponent.h"
#include "Runtime/NavigationSystem/Public/NavigationData.h"
#include "Runtime/Engine/Classes/AI/Navigation/NavigationDataInterface.h"
#include "Runtime/AIModule/Classes/AIResourceInterface.h"
#include "Runtime/Engine/Classes/AI/Navigation/PathFollowingAgentInterface.h"
#include "Runtime/AIModule/Classes/BrainComponent.h"
#include "Runtime/AIModule/Classes/BehaviorTree/BlackboardComponent.h"
#include "Runtime/AIModule/Classes/BehaviorTree/BlackboardData.h"
#include "Runtime/AIModule/Classes/BehaviorTree/Blackboard/BlackboardKeyType.h"
#include "Runtime/AIModule/Classes/Perception/AIPerceptionComponent.h"
#include "Runtime/AIModule/Classes/Perception/AISenseConfig.h"
#include "Runtime/AIModule/Classes/Perception/AISense.h"
#include "Runtime/AIModule/Classes/Perception/AIPerceptionTypes.h"
#include "Runtime/AIModule/Classes/Perception/AIPerceptionSystem.h"
#include "Runtime/AIModule/Classes/Perception/AISenseEvent.h"
#include "Runtime/AIModule/Classes/Actions/PawnActionsComponent.h"
#include "Runtime/AIModule/Classes/Actions/PawnAction.h"
#include "Runtime/AIModule/Classes/AITypes.h"
#include "Runtime/GameplayTasks/Classes/GameplayTasksComponent.h"
#include "Runtime/GameplayTasks/Classes/GameplayTask.h"
#include "Runtime/GameplayTasks/Classes/GameplayTaskOwnerInterface.h"
#include "Runtime/GameplayTasks/Classes/GameplayTaskResource.h"
#include "Runtime/NavigationSystem/Public/NavFilters/NavigationQueryFilter.h"
#include "Runtime/NavigationSystem/Public/NavAreas/NavArea.h"
#include "Runtime/AIModule/Classes/BehaviorTree/BehaviorTree.h"
#include "Runtime/AIModule/Classes/BehaviorTree/BTCompositeNode.h"
#include "Runtime/AIModule/Classes/BehaviorTree/BTNode.h"
#include "Runtime/AIModule/Classes/BehaviorTree/BTTaskNode.h"
#include "Runtime/AIModule/Classes/BehaviorTree/BTService.h"
#include "Runtime/AIModule/Classes/BehaviorTree/BTAuxiliaryNode.h"
#include "Runtime/AIModule/Classes/BehaviorTree/BTDecorator.h"
#include "Runtime/AIModule/Classes/BehaviorTree/BehaviorTreeTypes.h"
#include "Runtime/AIModule/Classes/Perception/AIPerceptionListenerInterface.h"
#include "Runtime/AIModule/Classes/GenericTeamAgentInterface.h"
#include "Runtime/Engine/Public/VisualLogger/VisualLoggerDebugSnapshotInterface.h"
#include "Runtime/Engine/Classes/GameFramework/PlayerController.h"
#include "Runtime/Engine/Classes/Engine/Player.h"
#include "Runtime/Engine/Classes/Matinee/InterpTrackInstDirector.h"
#include "Runtime/Engine/Classes/Matinee/InterpTrackInst.h"
#include "Runtime/Engine/Classes/GameFramework/HUD.h"
#include "Runtime/Engine/Classes/Engine/Canvas.h"
#include "Runtime/Engine/Classes/Debug/ReporterGraph.h"
#include "Runtime/Engine/Classes/Debug/ReporterBase.h"
#include "Runtime/Engine/Classes/GameFramework/DebugTextInfo.h"
#include "Runtime/Engine/Classes/Camera/PlayerCameraManager.h"
#include "Runtime/Engine/Classes/Camera/CameraTypes.h"
#include "Runtime/Engine/Classes/Engine/Scene.h"
#include "Runtime/Engine/Classes/Engine/TextureCube.h"
#include "Runtime/Engine/Classes/Camera/CameraModifier.h"
#include "Runtime/Engine/Classes/Particles/EmitterCameraLensEffectBase.h"
#include "Runtime/Engine/Classes/Particles/Emitter.h"
#include "Runtime/Engine/Classes/Particles/ParticleSystemComponent.h"
#include "Runtime/Engine/Classes/Particles/ParticleSystem.h"
#include "Runtime/Engine/Classes/Particles/ParticleEmitter.h"
#include "Runtime/Engine/Public/ParticleHelper.h"
#include "Runtime/Engine/Classes/Particles/ParticleLODLevel.h"
#include "Runtime/Engine/Classes/Particles/ParticleModuleRequired.h"
#include "Runtime/Engine/Classes/Particles/ParticleModule.h"
#include "Runtime/Engine/Classes/Particles/ParticleSpriteEmitter.h"
#include "Runtime/Engine/Classes/Distributions/DistributionFloat.h"
#include "Runtime/Engine/Classes/Distributions/Distribution.h"
#include "Runtime/Engine/Classes/Particles/SubUVAnimation.h"
#include "Runtime/Engine/Classes/Particles/TypeData/ParticleModuleTypeDataBase.h"
#include "Runtime/Engine/Classes/Particles/Spawn/ParticleModuleSpawn.h"
#include "Runtime/Engine/Classes/Particles/Spawn/ParticleModuleSpawnBase.h"
#include "Runtime/Engine/Classes/Particles/Event/ParticleModuleEventGenerator.h"
#include "Runtime/Engine/Classes/Particles/Event/ParticleModuleEventBase.h"
#include "Runtime/Engine/Classes/Particles/Event/ParticleModuleEventSendToGame.h"
#include "Runtime/Engine/Classes/Particles/Orbit/ParticleModuleOrbit.h"
#include "Runtime/Engine/Classes/Particles/Orbit/ParticleModuleOrbitBase.h"
#include "Runtime/Engine/Classes/Distributions/DistributionVector.h"
#include "Runtime/Engine/Classes/Particles/Event/ParticleModuleEventReceiverBase.h"
#include "Runtime/Engine/Classes/Engine/InterpCurveEdSetup.h"
#include "Runtime/Engine/Classes/Particles/ParticleSystemReplay.h"
#include "Runtime/Engine/Classes/Camera/CameraModifier_CameraShake.h"
#include "Runtime/Engine/Classes/Camera/CameraShake.h"
#include "Runtime/Engine/Classes/Camera/CameraAnim.h"
#include "Runtime/Engine/Classes/Matinee/InterpGroup.h"
#include "Runtime/Engine/Classes/Matinee/InterpTrack.h"
#include "Runtime/Engine/Classes/Camera/CameraAnimInst.h"
#include "Runtime/Engine/Classes/Matinee/InterpGroupInst.h"
#include "Runtime/Engine/Classes/Matinee/InterpTrackMove.h"
#include "Runtime/Engine/Classes/Matinee/InterpTrackInstMove.h"
#include "Runtime/Engine/Classes/Camera/CameraActor.h"
#include "Runtime/Engine/Classes/Camera/CameraComponent.h"
#include "Runtime/Engine/Classes/GameFramework/CheatManager.h"
#include "Runtime/Engine/Classes/Engine/DebugCameraController.h"
#include "Runtime/Engine/Classes/Components/DrawFrustumComponent.h"
#include "Runtime/Engine/Classes/GameFramework/PlayerInput.h"
#include "Runtime/Engine/Classes/GameFramework/ForceFeedbackEffect.h"
#include "Runtime/Engine/Classes/Engine/NetConnection.h"
#include "Runtime/Engine/Classes/Engine/ChildConnection.h"
#include "Runtime/Engine/Classes/Engine/PackageMapClient.h"
#include "Runtime/Engine/Classes/Engine/NetDriver.h"
#include "Runtime/Engine/Classes/Engine/World.h"
#include "Runtime/Engine/Classes/Engine/Level.h"
#include "Runtime/Engine/Classes/Components/ModelComponent.h"
#include "Runtime/Engine/Classes/Engine/LevelActorContainer.h"
#include "Runtime/Engine/Classes/Engine/LevelScriptActor.h"
#include "Runtime/Engine/Classes/Engine/NavigationObjectBase.h"
#include "Runtime/Engine/Classes/AI/Navigation/NavAgentInterface.h"
#include "Runtime/Engine/Classes/AI/Navigation/NavigationDataChunk.h"
#include "Runtime/Engine/Classes/Engine/MapBuildDataRegistry.h"
#include "Runtime/Engine/Classes/GameFramework/WorldSettings.h"
#include "Runtime/Engine/Classes/AI/NavigationSystemConfig.h"
#include "Runtime/Engine/Classes/GameFramework/DefaultPhysicsVolume.h"
#include "Runtime/Engine/Classes/PhysicsEngine/PhysicsCollisionHandler.h"
#include "Runtime/Engine/Classes/Sound/SoundBase.h"
#include "Runtime/Engine/Classes/Sound/SoundClass.h"
#include "Runtime/Engine/Classes/Sound/SoundMix.h"
#include "Runtime/Engine/Classes/Sound/SoundConcurrency.h"
#include "Runtime/Engine/Classes/Sound/SoundAttenuation.h"
#include "Runtime/Engine/Classes/Engine/Attenuation.h"
#include "Runtime/Engine/Public/IAudioExtensionPlugin.h"
#include "Runtime/Engine/Classes/Sound/SoundSubmix.h"
#include "Runtime/Engine/Classes/Sound/SoundEffectSubmix.h"
#include "Runtime/Engine/Classes/Sound/SoundEffectPreset.h"
#include "Runtime/Engine/Public/IAmbisonicsMixer.h"
#include "Runtime/Engine/Classes/Sound/SoundWave.h"
#include "Runtime/AudioPlatformConfiguration/Public/AudioCompressionSettings.h"
#include "Runtime/Engine/Classes/Sound/SoundGroups.h"
#include "Runtime/Engine/Classes/Engine/CurveTable.h"
#include "Runtime/Engine/Classes/Sound/SoundEffectSource.h"
#include "Runtime/Engine/Classes/Sound/SoundSourceBusSend.h"
#include "Runtime/Engine/Classes/Sound/SoundSourceBus.h"
#include "Runtime/Engine/Classes/GameFramework/GameModeBase.h"
#include "Runtime/Engine/Classes/GameFramework/GameSession.h"
#include "Runtime/Engine/Classes/GameFramework/GameStateBase.h"
#include "Runtime/Engine/Classes/GameFramework/SpectatorPawn.h"
#include "Runtime/Engine/Classes/GameFramework/DefaultPawn.h"
#include "Runtime/Engine/Classes/Components/SphereComponent.h"
#include "Runtime/Engine/Classes/Components/StaticMeshComponent.h"
#include "Runtime/Engine/Classes/Engine/StaticMesh.h"
#include "Runtime/Engine/Classes/Engine/StaticMeshSocket.h"
#include "Runtime/Engine/Classes/AI/Navigation/NavCollisionBase.h"
#include "Runtime/Engine/Classes/Engine/TextureStreamingTypes.h"
#include "Runtime/Engine/Classes/GameFramework/FloatingPawnMovement.h"
#include "Runtime/Engine/Classes/GameFramework/SpectatorPawnMovement.h"
#include "Runtime/Engine/Classes/Engine/ServerStatReplicator.h"
#include "Runtime/Engine/Classes/GameFramework/GameNetworkManager.h"
#include "Runtime/Engine/Classes/Sound/AudioVolume.h"
#include "Runtime/Engine/Classes/Sound/ReverbEffect.h"
#include "Runtime/Engine/Classes/Engine/BookMark.h"
#include "Runtime/Engine/Classes/Engine/BookmarkBase.h"
#include "Runtime/Engine/Classes/Components/LineBatchComponent.h"
#include "Runtime/Engine/Classes/Engine/LevelStreaming.h"
#include "Runtime/Engine/Classes/Engine/LevelStreamingVolume.h"
#include "Runtime/Engine/Classes/Engine/DemoNetDriver.h"
#include "Runtime/Engine/Classes/Particles/ParticleEventManager.h"
#include "Runtime/Engine/Classes/AI/NavigationSystemBase.h"
#include "Runtime/Engine/Classes/AI/AISystemBase.h"
#include "Runtime/Engine/Classes/AI/Navigation/AvoidanceManager.h"
#include "Runtime/Engine/Classes/Engine/GameInstance.h"
#include "Runtime/Engine/Classes/Engine/LocalPlayer.h"
#include "Runtime/Engine/Classes/Engine/GameViewportClient.h"
#include "Runtime/Engine/Classes/Engine/ScriptViewportClient.h"
#include "Runtime/Engine/Classes/Engine/Console.h"
#include "Runtime/Engine/Classes/Engine/DebugDisplayProperty.h"
#include "Runtime/Engine/Classes/Engine/Engine.h"
#include "Runtime/Engine/Classes/GameFramework/GameUserSettings.h"
#include "Runtime/Engine/Classes/Engine/AssetManager.h"
#include "Runtime/Engine/Classes/Engine/EngineCustomTimeStep.h"
#include "Runtime/Engine/Classes/Engine/TimecodeProvider.h"
#include "Runtime/Engine/Classes/GameFramework/OnlineSession.h"
#include "Runtime/Engine/Classes/Materials/MaterialParameterCollectionInstance.h"
#include "Runtime/Engine/Classes/Engine/WorldComposition.h"
#include "Runtime/Engine/Classes/Particles/WorldPSCPool.h"
#include "Runtime/Engine/Classes/Engine/Channel.h"
#include "Runtime/Engine/Classes/Engine/ReplicationDriver.h"
#include "Runtime/Engine/Classes/Components/InputComponent.h"
#include "Runtime/Engine/Classes/GameFramework/TouchInterface.h"
#include "Runtime/UMG/Public/Blueprint/UserWidget.h"
#include "Runtime/UMG/Public/Components/Widget.h"
#include "Runtime/UMG/Public/Components/Visual.h"
#include "Runtime/UMG/Public/Components/PanelSlot.h"
#include "Runtime/UMG/Public/Components/PanelWidget.h"
#include "Runtime/UMG/Public/Components/SlateWrapperTypes.h"
#include "Runtime/UMG/Public/Slate/WidgetTransform.h"
#include "Runtime/SlateCore/Public/Layout/Clipping.h"
#include "Runtime/UMG/Public/Blueprint/WidgetNavigation.h"
#include "Runtime/SlateCore/Public/Input/NavigationReply.h"
#include "Runtime/SlateCore/Public/Types/SlateEnums.h"
#include "Runtime/UMG/Public/Binding/PropertyBinding.h"
#include "Runtime/UMG/Public/Binding/DynamicPropertyPath.h"
#include "Runtime/PropertyPath/Public/PropertyPathHelpers.h"
#include "Runtime/SlateCore/Public/Layout/Geometry.h"
#include "Runtime/SlateCore/Public/Input/Events.h"
#include "Runtime/SlateCore/Public/Styling/SlateColor.h"
#include "Runtime/SlateCore/Public/Styling/SlateBrush.h"
#include "Runtime/SlateCore/Public/Layout/Margin.h"
#include "Runtime/SlateCore/Public/Styling/SlateTypes.h"
#include "Runtime/UMG/Public/Animation/UMGSequencePlayer.h"
#include "Runtime/UMG/Public/Animation/WidgetAnimation.h"
#include "Runtime/MovieScene/Public/MovieSceneSequence.h"
#include "Runtime/MovieScene/Public/MovieSceneSignedObject.h"
#include "Runtime/MovieScene/Public/Evaluation/MovieSceneEvaluationTemplate.h"
#include "Runtime/MovieScene/Public/Evaluation/MovieSceneTrackIdentifier.h"
#include "Runtime/MovieScene/Public/Evaluation/MovieSceneEvaluationTrack.h"
#include "Runtime/MovieScene/Public/Evaluation/MovieSceneSegment.h"
#include "Runtime/MovieScene/Public/MovieSceneTrack.h"
#include "Runtime/MovieScene/Public/Evaluation/MovieSceneEvalTemplate.h"
#include "Runtime/MovieScene/Public/Evaluation/MovieSceneTrackImplementation.h"
#include "Runtime/MovieScene/Public/Evaluation/MovieSceneEvaluationField.h"
#include "Runtime/MovieScene/Public/MovieSceneFrameMigration.h"
#include "Runtime/MovieScene/Public/MovieSceneSequenceID.h"
#include "Runtime/MovieScene/Public/Evaluation/MovieSceneEvaluationKey.h"
#include "Runtime/MovieScene/Public/Evaluation/MovieSceneSequenceHierarchy.h"
#include "Runtime/MovieScene/Public/Evaluation/MovieSceneSequenceTransform.h"
#include "Runtime/MovieScene/Public/Evaluation/MovieSceneSequenceInstanceData.h"
#include "Runtime/MovieScene/Public/MovieSceneSection.h"
#include "Runtime/MovieScene/Public/MovieScene.h"
#include "Runtime/MovieScene/Public/MovieSceneSpawnable.h"
#include "Runtime/MovieScene/Public/MovieScenePossessable.h"
#include "Runtime/MovieScene/Public/MovieSceneBinding.h"
#include "Runtime/MovieScene/Public/MovieSceneFwd.h"
#include "Runtime/UMG/Public/Animation/WidgetAnimationBinding.h"
#include "Runtime/UMG/Public/Blueprint/WidgetTree.h"
#include "Runtime/Slate/Public/Widgets/Layout/Anchors.h"
#include "Runtime/UMG/Public/Blueprint/DragDropOperation.h"
#include "Runtime/UMG/Public/Components/NamedSlotInterface.h"
#include "Runtime/Engine/Classes/Haptics/HapticFeedbackEffect_Base.h"
#include "Runtime/Engine/Classes/Engine/LatentActionManager.h"
#include "Runtime/Engine/Classes/AI/Navigation/NavRelevantInterface.h"
#include "Runtime/Engine/Classes/Matinee/MatineeActor.h"
#include "Runtime/Engine/Classes/Matinee/InterpData.h"
#include "Runtime/Engine/Classes/Matinee/InterpGroupDirector.h"
#include "Runtime/Engine/Classes/Components/ChildActorComponent.h"
#include "Runtime/Engine/Classes/Components/BoxComponent.h"
#include "Runtime/NavigationSystem/Public/NavAreas/NavArea_Obstacle.h"
#include "Runtime/Engine/Classes/Components/PostProcessComponent.h"
#include "Runtime/Engine/Classes/Interfaces/Interface_PostProcessVolume.h"
#include "Runtime/Engine/Classes/Components/ArrowComponent.h"
#include "WBP_Resetting__pf3389175594.h"
#include "Runtime/Engine/Classes/Kismet/KismetMathLibrary.h"
#include "Runtime/Engine/Classes/Kismet/BlueprintFunctionLibrary.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Runtime/Engine/Classes/Components/AudioComponent.h"
#include "Runtime/Engine/Classes/GameFramework/ForceFeedbackAttenuation.h"
#include "Runtime/Engine/Classes/Components/ForceFeedbackComponent.h"
#include "Runtime/Engine/Classes/Sound/DialogueWave.h"
#include "Runtime/Engine/Classes/Sound/DialogueTypes.h"
#include "Runtime/Engine/Classes/Sound/DialogueVoice.h"
#include "Runtime/Engine/Classes/Sound/DialogueSoundWaveProxy.h"
#include "Runtime/Engine/Classes/Components/DecalComponent.h"
#include "Runtime/Engine/Classes/GameFramework/SaveGame.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStaticsTypes.h"
#include "Runtime/Engine/Classes/Kismet/KismetSystemLibrary.h"
#include "Runtime/UMG/Public/Blueprint/WidgetBlueprintLibrary.h"
#include "Runtime/Engine/Public/Slate/SGameLayerManager.h"
#include "Runtime/Engine/Classes/Slate/SlateBrushAsset.h"
#include "Runtime/Engine/Classes/Kismet/KismetStringLibrary.h"
#include "TL4_TestingGrounds/Weapons/Gun.h"
#include "TL4_TestingGrounds/Weapons/BallProjectile.h"
#include "Runtime/Engine/Classes/GameFramework/ProjectileMovementComponent.h"
#include "Runtime/ClothingSystemRuntime/Public/ClothingSimulationFactory.h"
#include "WBP_GameOver__pf3389175594.h"
#include "Runtime/Engine/Classes/Engine/CollisionProfile.h"
#include "ABP_FP__pf117350442.h"
#include "ABP_TP__pf117350442.h"
#include "BP_NPC_AI__pf2310560554.h"


#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
PRAGMA_DISABLE_OPTIMIZATION
ABP_TL4Character_C__pf3043094308::ABP_TL4Character_C__pf3043094308(const FObjectInitializer& ObjectInitializer) : Super()
{
	if(HasAnyFlags(RF_ClassDefaultObject) && (ABP_TL4Character_C__pf3043094308::StaticClass() == GetClass()))
	{
		ABP_TL4Character_C__pf3043094308::__CustomDynamicClassInitialization(CastChecked<UDynamicClass>(GetClass()));
	}
	
	auto __Local__0 = CastChecked<UCapsuleComponent>(this->GetDefaultSubobjectByName(TEXT("CollisionCylinder")), ECastCheckedType::NullAllowed);
	auto __Local__1 = CastChecked<UCharacterMovementComponent>(this->GetDefaultSubobjectByName(TEXT("CharMoveComp")), ECastCheckedType::NullAllowed);
	auto __Local__2 = CastChecked<USkeletalMeshComponent>(this->GetDefaultSubobjectByName(TEXT("CharacterMesh0")), ECastCheckedType::NullAllowed);
	auto __Local__3 = CastChecked<UCameraComponent>(this->GetDefaultSubobjectByName(TEXT("FirstPersonCamera")), ECastCheckedType::NullAllowed);
	auto __Local__4 = CastChecked<USkeletalMeshComponent>(this->GetDefaultSubobjectByName(TEXT("CharacterMesh1P")), ECastCheckedType::NullAllowed);
	if(__Local__0)
	{
		// --- Default subobject 'CollisionCylinder' //
		auto& __Local__5 = (*(AccessPrivateProperty<float >((__Local__0), UCapsuleComponent::__PPO__CapsuleHalfHeight() )));
		__Local__5 = 100.000000f;
		auto& __Local__6 = (*(AccessPrivateProperty<float >((__Local__0), UCapsuleComponent::__PPO__CapsuleRadius() )));
		__Local__6 = 45.000000f;
		auto& __Local__7 = (*(AccessPrivateProperty<float >(&(__Local__0->BodyInstance), FBodyInstance::__PPO__MassInKgOverride() )));
		__Local__7 = 188.588409f;
		__Local__0->bVisible = false;
		__Local__0->bHiddenInGame = false;
		static TWeakObjectPtr<UProperty> __Local__9{};
		const UProperty* __Local__8 = __Local__9.Get();
		if (nullptr == __Local__8)
		{
			__Local__8 = (UActorComponent::StaticClass())->FindPropertyByName(FName(TEXT("bCanEverAffectNavigation")));
			check(__Local__8);
			__Local__9 = __Local__8;
		}
		(((UBoolProperty*)__Local__8)->SetPropertyValue_InContainer((__Local__0), false, 0));
		// --- END default subobject 'CollisionCylinder' //
	}
	if(__Local__1)
	{
		// --- Default subobject 'CharMoveComp' //
		__Local__1->GravityScale = 1.500000f;
		__Local__1->JumpZVelocity = 300.000000f;
		auto& __Local__10 = (*(AccessPrivateProperty<float >((__Local__1), UCharacterMovementComponent::__PPO__WalkableFloorZ() )));
		__Local__10 = 0.710000f;
		__Local__1->MaxSwimSpeed = 170.000000f;
		__Local__1->MaxFlySpeed = 270.000000f;
		__Local__1->MaxCustomMovementSpeed = 270.000000f;
		__Local__1->bUseControllerDesiredRotation = true;
		__Local__1->bIgnoreBaseRotation = true;
		__Local__1->NavAgentProps.AgentRadius = 45.000000f;
		__Local__1->NavAgentProps.AgentHeight = 200.000000f;
		static TWeakObjectPtr<UProperty> __Local__12{};
		const UProperty* __Local__11 = __Local__12.Get();
		if (nullptr == __Local__11)
		{
			__Local__11 = (UNavMovementComponent::StaticClass())->FindPropertyByName(FName(TEXT("bUseAccelerationForPaths")));
			check(__Local__11);
			__Local__12 = __Local__11;
		}
		(((UBoolProperty*)__Local__11)->SetPropertyValue_InContainer((__Local__1), false, 0));
		static TWeakObjectPtr<UProperty> __Local__14{};
		const UProperty* __Local__13 = __Local__14.Get();
		if (nullptr == __Local__13)
		{
			__Local__13 = (UActorComponent::StaticClass())->FindPropertyByName(FName(TEXT("bCanEverAffectNavigation")));
			check(__Local__13);
			__Local__14 = __Local__13;
		}
		(((UBoolProperty*)__Local__13)->SetPropertyValue_InContainer((__Local__1), false, 0));
		// --- END default subobject 'CharMoveComp' //
	}
	if(__Local__2)
	{
		// --- Default subobject 'CharacterMesh0' //
		__Local__2->AnimClass = UABP_TP_C__pf117350442::StaticClass();
		__Local__2->SkeletalMesh = CastChecked<USkeletalMesh>(CastChecked<UDynamicClass>(ABP_TL4Character_C__pf3043094308::StaticClass())->UsedAssets[0], ECastCheckedType::NullAllowed);
		__Local__2->VisibilityBasedAnimTickOption = EVisibilityBasedAnimTickOption::AlwaysTickPose;
		__Local__2->bOwnerNoSee = true;
		auto& __Local__15 = (*(AccessPrivateProperty<float >(&(__Local__2->BodyInstance), FBodyInstance::__PPO__MassInKgOverride() )));
		__Local__15 = 85.812500f;
		auto& __Local__16 = (*(AccessPrivateProperty<UCapsuleComponent*>((__Local__2), USceneComponent::__PPO__AttachParent() )));
		__Local__16 = __Local__0;
		__Local__2->RelativeLocation = FVector(0.000000, 0.000000, -100.000000);
		__Local__2->RelativeRotation = FRotator(-0.000000, -90.000000, -0.000000);
		__Local__2->PrimaryComponentTick.EndTickGroup = ETickingGroup::TG_PostPhysics;
		static TWeakObjectPtr<UProperty> __Local__18{};
		const UProperty* __Local__17 = __Local__18.Get();
		if (nullptr == __Local__17)
		{
			__Local__17 = (UActorComponent::StaticClass())->FindPropertyByName(FName(TEXT("bCanEverAffectNavigation")));
			check(__Local__17);
			__Local__18 = __Local__17;
		}
		(((UBoolProperty*)__Local__17)->SetPropertyValue_InContainer((__Local__2), false, 0));
		// --- END default subobject 'CharacterMesh0' //
	}
	if(__Local__3)
	{
		// --- Default subobject 'FirstPersonCamera' //
		auto& __Local__19 = (*(AccessPrivateProperty<UCapsuleComponent*>((__Local__3), USceneComponent::__PPO__AttachParent() )));
		__Local__19 = __Local__0;
		__Local__3->RelativeLocation = FVector(-9.580000, -8.250000, 64.000000);
		static TWeakObjectPtr<UProperty> __Local__21{};
		const UProperty* __Local__20 = __Local__21.Get();
		if (nullptr == __Local__20)
		{
			__Local__20 = (UActorComponent::StaticClass())->FindPropertyByName(FName(TEXT("bCanEverAffectNavigation")));
			check(__Local__20);
			__Local__21 = __Local__20;
		}
		(((UBoolProperty*)__Local__20)->SetPropertyValue_InContainer((__Local__3), false, 0));
		// --- END default subobject 'FirstPersonCamera' //
	}
	if(__Local__4)
	{
		// --- Default subobject 'CharacterMesh1P' //
		__Local__4->AnimClass = UABP_FP_C__pf117350442::StaticClass();
		__Local__4->SkeletalMesh = CastChecked<USkeletalMesh>(CastChecked<UDynamicClass>(ABP_TL4Character_C__pf3043094308::StaticClass())->UsedAssets[1], ECastCheckedType::NullAllowed);
		__Local__4->VisibilityBasedAnimTickOption = EVisibilityBasedAnimTickOption::AlwaysTickPoseAndRefreshBones;
		auto& __Local__22 = (*(AccessPrivateProperty<UCameraComponent*>((__Local__4), USceneComponent::__PPO__AttachParent() )));
		__Local__22 = __Local__3;
		static TWeakObjectPtr<UProperty> __Local__24{};
		const UProperty* __Local__23 = __Local__24.Get();
		if (nullptr == __Local__23)
		{
			__Local__23 = (UActorComponent::StaticClass())->FindPropertyByName(FName(TEXT("bCanEverAffectNavigation")));
			check(__Local__23);
			__Local__24 = __Local__23;
		}
		(((UBoolProperty*)__Local__23)->SetPropertyValue_InContainer((__Local__4), false, 0));
		// --- END default subobject 'CharacterMesh1P' //
	}
	bpv__PatrolRoute__pf = CreateDefaultSubobject<UPatrolRoute>(TEXT("PatrolRoute"));
	bpv__EffectVolume__pf = CreateDefaultSubobject<UBoxComponent>(TEXT("EffectVolume"));
	bpv__PostProcess__pf = CreateDefaultSubobject<UPostProcessComponent>(TEXT("PostProcess"));
	bpv__DeathCameraSpawnPoint__pf = CreateDefaultSubobject<UArrowComponent>(TEXT("DeathCameraSpawnPoint"));
	bpv__PatrolRoute__pf->CreationMethod = EComponentCreationMethod::Native;
	static TWeakObjectPtr<UProperty> __Local__26{};
	const UProperty* __Local__25 = __Local__26.Get();
	if (nullptr == __Local__25)
	{
		__Local__25 = (UActorComponent::StaticClass())->FindPropertyByName(FName(TEXT("bCanEverAffectNavigation")));
		check(__Local__25);
		__Local__26 = __Local__25;
	}
	(((UBoolProperty*)__Local__25)->SetPropertyValue_InContainer((bpv__PatrolRoute__pf), false, 0));
	bpv__EffectVolume__pf->CreationMethod = EComponentCreationMethod::Native;
	bpv__EffectVolume__pf->AttachToComponent(__Local__0, FAttachmentTransformRules::KeepRelativeTransform );
	auto& __Local__27 = (*(AccessPrivateProperty<FVector >((bpv__EffectVolume__pf), UBoxComponent::__PPO__BoxExtent() )));
	__Local__27 = FVector(44.619007, 36.117527, 101.765686);
	bpv__EffectVolume__pf->AreaClass = UNavArea_Obstacle::StaticClass();
	(((UBoolProperty*)__Local__25)->SetPropertyValue_InContainer((bpv__EffectVolume__pf), true, 0));
	bpv__PostProcess__pf->CreationMethod = EComponentCreationMethod::Native;
	bpv__PostProcess__pf->AttachToComponent(bpv__EffectVolume__pf, FAttachmentTransformRules::KeepRelativeTransform );
	bpv__PostProcess__pf->Settings.bOverride_SceneColorTint = true;
	bpv__PostProcess__pf->Settings.bOverride_SceneFringeIntensity = true;
	bpv__PostProcess__pf->Settings.bOverride_VignetteIntensity = true;
	bpv__PostProcess__pf->Settings.SceneColorTint = FLinearColor(0.900000, 0.100000, 0.100000, 1.000000);
	bpv__PostProcess__pf->Settings.SceneFringeIntensity = 4.200000f;
	bpv__PostProcess__pf->BlendWeight = 0.000000f;
	bpv__PostProcess__pf->bUnbound = false;
	(((UBoolProperty*)__Local__25)->SetPropertyValue_InContainer((bpv__PostProcess__pf), false, 0));
	bpv__DeathCameraSpawnPoint__pf->CreationMethod = EComponentCreationMethod::Native;
	bpv__DeathCameraSpawnPoint__pf->AttachToComponent(__Local__0, FAttachmentTransformRules::KeepRelativeTransform );
	bpv__DeathCameraSpawnPoint__pf->RelativeLocation = FVector(-410.000000, 0.000000, 115.000000);
	bpv__DeathCameraSpawnPoint__pf->RelativeRotation = FRotator(-30.000000, 0.000000, 0.000000);
	(((UBoolProperty*)__Local__25)->SetPropertyValue_InContainer((bpv__DeathCameraSpawnPoint__pf), false, 0));
	bpv__Damage_Fade_Blend_Weight_AEF775E04681EC7A1A49ACB60F6072C8__pf = 0.000000f;
	bpv__Damage_Fade__Direction_AEF775E04681EC7A1A49ACB60F6072C8__pf = ETimelineDirection::Type::Forward;
	bpv__DamagexFade__pfT = nullptr;
	bpv__JumpxButtonxDown__pfTT = false;
	bpv__CrouchxButtonxDown__pfTT = false;
	bpv__Aiming__pf = false;
	bpv__Health__pf = 30.000000f;
	bpv__MaxHealth__pf = 30.000000f;
	bpv__OldxController__pfT = nullptr;
	bpv__IsxGamexOver__pfTT = true;
	GunBlueprint = CastChecked<UClass>(CastChecked<UDynamicClass>(ABP_TL4Character_C__pf3043094308::StaticClass())->UsedAssets[2], ECastCheckedType::NullAllowed);
	auto& __Local__28 = (*(AccessPrivateProperty<USkeletalMeshComponent*>((this), AMannequin::__PPO__Mesh1P() )));
	__Local__28 = __Local__4;
	auto& __Local__29 = (*(AccessPrivateProperty<UCameraComponent*>((this), AMannequin::__PPO__FirstPersonCameraComponent() )));
	__Local__29 = __Local__3;
	auto& __Local__30 = (*(AccessPrivateProperty<USkeletalMeshComponent*>((this), ACharacter::__PPO__Mesh() )));
	__Local__30 = __Local__2;
	auto& __Local__31 = (*(AccessPrivateProperty<UCharacterMovementComponent*>((this), ACharacter::__PPO__CharacterMovement() )));
	__Local__31 = __Local__1;
	auto& __Local__32 = (*(AccessPrivateProperty<UCapsuleComponent*>((this), ACharacter::__PPO__CapsuleComponent() )));
	__Local__32 = __Local__0;
	AIControllerClass = ABP_NPC_AI_C__pf2310560554::StaticClass();
	SpawnCollisionHandlingMethod = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	Tags = TArray<FName> ();
	Tags.Reserve(1);
	Tags.Add(FName(TEXT("BotTeam")));
}
PRAGMA_ENABLE_OPTIMIZATION
void ABP_TL4Character_C__pf3043094308::PostLoadSubobjects(FObjectInstancingGraph* OuterInstanceGraph)
{
	Super::PostLoadSubobjects(OuterInstanceGraph);
	if(bpv__PatrolRoute__pf)
	{
		bpv__PatrolRoute__pf->CreationMethod = EComponentCreationMethod::Native;
	}
	if(bpv__EffectVolume__pf)
	{
		bpv__EffectVolume__pf->CreationMethod = EComponentCreationMethod::Native;
	}
	if(bpv__PostProcess__pf)
	{
		bpv__PostProcess__pf->CreationMethod = EComponentCreationMethod::Native;
	}
	if(bpv__DeathCameraSpawnPoint__pf)
	{
		bpv__DeathCameraSpawnPoint__pf->CreationMethod = EComponentCreationMethod::Native;
	}
}
PRAGMA_DISABLE_OPTIMIZATION
void ABP_TL4Character_C__pf3043094308::__CustomDynamicClassInitialization(UDynamicClass* InDynamicClass)
{
	ensure(0 == InDynamicClass->ReferencedConvertedFields.Num());
	ensure(0 == InDynamicClass->MiscConvertedSubobjects.Num());
	ensure(0 == InDynamicClass->DynamicBindingObjects.Num());
	ensure(0 == InDynamicClass->ComponentTemplates.Num());
	ensure(0 == InDynamicClass->Timelines.Num());
	ensure(nullptr == InDynamicClass->AnimClassImplementation);
	InDynamicClass->AssembleReferenceTokenStream();
	// List of all referenced converted classes
	InDynamicClass->ReferencedConvertedFields.Add(UWBP_Resetting_C__pf3389175594::StaticClass());
	InDynamicClass->ReferencedConvertedFields.Add(UWBP_GameOver_C__pf3389175594::StaticClass());
	InDynamicClass->ReferencedConvertedFields.Add(UABP_FP_C__pf117350442::StaticClass());
	InDynamicClass->ReferencedConvertedFields.Add(UABP_TP_C__pf117350442::StaticClass());
	InDynamicClass->ReferencedConvertedFields.Add(ABP_NPC_AI_C__pf2310560554::StaticClass());
	FConvertedBlueprintsDependencies::FillUsedAssetsInDynamicClass(InDynamicClass, &__StaticDependencies_DirectlyUsedAssets);
	auto __Local__33 = NewObject<USceneComponent>(InDynamicClass, USceneComponent::StaticClass(), TEXT("SceneComponent_0"));
	InDynamicClass->ComponentTemplates.Add(__Local__33);
	auto __Local__34 = NewObject<UTimelineTemplate>(InDynamicClass, UTimelineTemplate::StaticClass(), TEXT("Damage Fade_Template"));
	InDynamicClass->Timelines.Add(__Local__34);
	auto __Local__35 = NewObject<UInputAxisDelegateBinding>(InDynamicClass, UInputAxisDelegateBinding::StaticClass(), TEXT("InputAxisDelegateBinding_1"));
	InDynamicClass->DynamicBindingObjects.Add(__Local__35);
	auto __Local__36 = NewObject<UInputKeyDelegateBinding>(InDynamicClass, UInputKeyDelegateBinding::StaticClass(), TEXT("InputKeyDelegateBinding_1"));
	InDynamicClass->DynamicBindingObjects.Add(__Local__36);
	auto __Local__37 = NewObject<UInputActionDelegateBinding>(InDynamicClass, UInputActionDelegateBinding::StaticClass(), TEXT("InputActionDelegateBinding_1"));
	InDynamicClass->DynamicBindingObjects.Add(__Local__37);
	static TWeakObjectPtr<UProperty> __Local__39{};
	const UProperty* __Local__38 = __Local__39.Get();
	if (nullptr == __Local__38)
	{
		__Local__38 = (UActorComponent::StaticClass())->FindPropertyByName(FName(TEXT("bCanEverAffectNavigation")));
		check(__Local__38);
		__Local__39 = __Local__38;
	}
	(((UBoolProperty*)__Local__38)->SetPropertyValue_InContainer((__Local__33), false, 0));
	__Local__34->bValidatedAsWired = true;
	__Local__34->FloatTracks = TArray<FTTFloatTrack> ();
	__Local__34->FloatTracks.AddUninitialized(1);
	FTTFloatTrack::StaticStruct()->InitializeStruct(__Local__34->FloatTracks.GetData(), 1);
	auto& __Local__40 = __Local__34->FloatTracks[0];
	auto __Local__41 = NewObject<UCurveFloat>(InDynamicClass, UCurveFloat::StaticClass(), TEXT("CurveFloat_0"));
	InDynamicClass->MiscConvertedSubobjects.Add(__Local__41);
	__Local__41->FloatCurve.Keys = TArray<FRichCurveKey> ();
	__Local__41->FloatCurve.Keys.AddUninitialized(2);
	FRichCurveKey::StaticStruct()->InitializeStruct(__Local__41->FloatCurve.Keys.GetData(), 2);
	auto& __Local__42 = __Local__41->FloatCurve.Keys[0];
	__Local__42.InterpMode = ERichCurveInterpMode::RCIM_Cubic;
	__Local__42.Value = 1.000000f;
	auto& __Local__43 = __Local__41->FloatCurve.Keys[1];
	__Local__43.Time = 0.500000f;
	__Local__40.CurveFloat = __Local__41;
	__Local__40.TrackName = FName(TEXT("Blend Weight"));
	__Local__34->TimelineGuid = FGuid(0xAEF775E0, 0x4681EC7A, 0x1A49ACB6, 0x0F6072C8);
	__Local__35->InputAxisDelegateBindings = TArray<FBlueprintInputAxisDelegateBinding> ();
	__Local__35->InputAxisDelegateBindings.AddUninitialized(4);
	FBlueprintInputAxisDelegateBinding::StaticStruct()->InitializeStruct(__Local__35->InputAxisDelegateBindings.GetData(), 4);
	auto& __Local__44 = __Local__35->InputAxisDelegateBindings[0];
	__Local__44.InputAxisName = FName(TEXT("MoveForward"));
	__Local__44.FunctionNameToBind = FName(TEXT("InpAxisEvt_MoveForward_K2Node_InputAxisEvent_180"));
	auto& __Local__45 = __Local__35->InputAxisDelegateBindings[1];
	__Local__45.InputAxisName = FName(TEXT("LookUp"));
	__Local__45.FunctionNameToBind = FName(TEXT("InpAxisEvt_LookUp_K2Node_InputAxisEvent_268"));
	auto& __Local__46 = __Local__35->InputAxisDelegateBindings[2];
	__Local__46.InputAxisName = FName(TEXT("Turn"));
	__Local__46.FunctionNameToBind = FName(TEXT("InpAxisEvt_Turn_K2Node_InputAxisEvent_256"));
	auto& __Local__47 = __Local__35->InputAxisDelegateBindings[3];
	__Local__47.InputAxisName = FName(TEXT("MoveRight"));
	__Local__47.FunctionNameToBind = FName(TEXT("InpAxisEvt_MoveRight_K2Node_InputAxisEvent_243"));
	__Local__36->InputKeyDelegateBindings = TArray<FBlueprintInputKeyDelegateBinding> ();
	__Local__36->InputKeyDelegateBindings.AddUninitialized(2);
	FBlueprintInputKeyDelegateBinding::StaticStruct()->InitializeStruct(__Local__36->InputKeyDelegateBindings.GetData(), 2);
	auto& __Local__48 = __Local__36->InputKeyDelegateBindings[0];
	__Local__48.InputChord.Key = FKey(TEXT("BackSpace"));
	__Local__48.FunctionNameToBind = FName(TEXT("InpActEvt_BackSpace_K2Node_InputKeyEvent_0"));
	auto& __Local__49 = __Local__36->InputKeyDelegateBindings[1];
	__Local__49.InputChord.Key = FKey(TEXT("Gamepad_FaceButton_Right"));
	__Local__49.FunctionNameToBind = FName(TEXT("InpActEvt_Gamepad_FaceButton_Right_K2Node_InputKeyEvent_1"));
	__Local__37->InputActionDelegateBindings = TArray<FBlueprintInputActionDelegateBinding> ();
	__Local__37->InputActionDelegateBindings.AddUninitialized(4);
	FBlueprintInputActionDelegateBinding::StaticStruct()->InitializeStruct(__Local__37->InputActionDelegateBindings.GetData(), 4);
	auto& __Local__50 = __Local__37->InputActionDelegateBindings[0];
	__Local__50.InputActionName = FName(TEXT("Crouch"));
	__Local__50.FunctionNameToBind = FName(TEXT("InpActEvt_Crouch_K2Node_InputActionEvent_0"));
	auto& __Local__51 = __Local__37->InputActionDelegateBindings[1];
	__Local__51.InputActionName = FName(TEXT("Crouch"));
	__Local__51.InputKeyEvent = EInputEvent::IE_Released;
	__Local__51.FunctionNameToBind = FName(TEXT("InpActEvt_Crouch_K2Node_InputActionEvent_1"));
	auto& __Local__52 = __Local__37->InputActionDelegateBindings[2];
	__Local__52.InputActionName = FName(TEXT("Jump"));
	__Local__52.FunctionNameToBind = FName(TEXT("InpActEvt_Jump_K2Node_InputActionEvent_2"));
	auto& __Local__53 = __Local__37->InputActionDelegateBindings[3];
	__Local__53.InputActionName = FName(TEXT("Jump"));
	__Local__53.InputKeyEvent = EInputEvent::IE_Released;
	__Local__53.FunctionNameToBind = FName(TEXT("InpActEvt_Jump_K2Node_InputActionEvent_3"));
}
PRAGMA_ENABLE_OPTIMIZATION
void ABP_TL4Character_C__pf3043094308::bpf__ExecuteUbergraph_BP_TL4Character__pf_0(int32 bpp__EntryPoint__pf)
{
	check(bpp__EntryPoint__pf == 44);
	// optimized KCST_UnconditionalGoto
	bpv__OldxController__pfT = b0l__K2Node_Event_NewController__pf;
	// optimized KCST_UnconditionalGoto
	b0l__K2Node_DynamicCast_AsPlayer_Controller__pf = Cast<APlayerController>(bpv__OldxController__pfT);
	b0l__K2Node_DynamicCast_bSuccess__pf = (b0l__K2Node_DynamicCast_AsPlayer_Controller__pf != nullptr);;
	if (!b0l__K2Node_DynamicCast_bSuccess__pf)
	{
		return; //KCST_EndOfThreadIfNot
	}
	if(::IsValid(b0l__K2Node_DynamicCast_AsPlayer_Controller__pf))
	{
		b0l__K2Node_DynamicCast_AsPlayer_Controller__pf->SetViewTargetWithBlend(this, 0.000000, EViewTargetBlendFunction::VTBlend_Linear, 0.000000, false);
	}
	return; //KCST_EndOfThread
}
void ABP_TL4Character_C__pf3043094308::bpf__ExecuteUbergraph_BP_TL4Character__pf_1(int32 bpp__EntryPoint__pf)
{
	check(bpp__EntryPoint__pf == 43);
	return; //KCST_EndOfThread
}
void ABP_TL4Character_C__pf3043094308::bpf__ExecuteUbergraph_BP_TL4Character__pf_2(int32 bpp__EntryPoint__pf)
{
	FName bpfv__CallFunc_Conv_StringToName_ReturnValue__pf{};
	TArray< int32, TInlineAllocator<8> > __StateStack;

	int32 __CurrentState = bpp__EntryPoint__pf;
	do
	{
		switch( __CurrentState )
		{
		case 19:
			{
				__CurrentState = 20;
				break;
			}
		case 20:
			{
				b0l__CallFunc_GetCurrentLevelName_ReturnValue__pf = UGameplayStatics::GetCurrentLevelName(this, true);
			}
		case 21:
			{
				__StateStack.Push(24);
			}
		case 22:
			{
				b0l__CallFunc_Create_ReturnValue__pf = CastChecked<UWBP_Resetting_C__pf3389175594>(UWidgetBlueprintLibrary::Create(this, UWBP_Resetting_C__pf3389175594::StaticClass(), ((APlayerController*)nullptr)), ECastCheckedType::NullAllowed);
			}
		case 23:
			{
				if(::IsValid(b0l__CallFunc_Create_ReturnValue__pf))
				{
					b0l__CallFunc_Create_ReturnValue__pf->UUserWidget::AddToViewport(0);
				}
				__CurrentState = (__StateStack.Num() > 0) ? __StateStack.Pop(/*bAllowShrinking=*/ false) : -1;
				break;
			}
		case 24:
			{
				bpfv__CallFunc_Conv_StringToName_ReturnValue__pf = UKismetStringLibrary::Conv_StringToName(b0l__CallFunc_GetCurrentLevelName_ReturnValue__pf);
				UGameplayStatics::OpenLevel(this, bpfv__CallFunc_Conv_StringToName_ReturnValue__pf, true, FString(TEXT("")));
				__CurrentState = (__StateStack.Num() > 0) ? __StateStack.Pop(/*bAllowShrinking=*/ false) : -1;
				break;
			}
		case 25:
			{
				__CurrentState = 20;
				break;
			}
		default:
			check(false); // Invalid state
			break;
		}
	} while( __CurrentState != -1 );
}
void ABP_TL4Character_C__pf3043094308::bpf__ExecuteUbergraph_BP_TL4Character__pf_3(int32 bpp__EntryPoint__pf)
{
	check(bpp__EntryPoint__pf == 42);
	// optimized KCST_UnconditionalGoto
	if(::IsValid(bpv__PostProcess__pf))
	{
		bpv__PostProcess__pf->BlendWeight = bpv__Damage_Fade_Blend_Weight_AEF775E04681EC7A1A49ACB60F6072C8__pf;
	}
	return; //KCST_EndOfThread
}
void ABP_TL4Character_C__pf3043094308::bpf__ExecuteUbergraph_BP_TL4Character__pf_4(int32 bpp__EntryPoint__pf)
{
	TArray< int32, TInlineAllocator<8> > __StateStack;

	int32 __CurrentState = bpp__EntryPoint__pf;
	do
	{
		switch( __CurrentState )
		{
		case 34:
			{
				if(::IsValid(bpv__DamagexFade__pfT))
				{
					bpv__DamagexFade__pfT->UTimelineComponent::PlayFromStart();
				}
				__CurrentState = (__StateStack.Num() > 0) ? __StateStack.Pop(/*bAllowShrinking=*/ false) : -1;
				break;
			}
		case 35:
			{
				__CurrentState = 36;
				break;
			}
		case 36:
			{
				__StateStack.Push(37);
				__CurrentState = 34;
				break;
			}
		case 37:
			{
				bpf__TakexDamage__pfT(b0l__K2Node_Event_Damage__pf);
				__CurrentState = (__StateStack.Num() > 0) ? __StateStack.Pop(/*bAllowShrinking=*/ false) : -1;
				break;
			}
		default:
			check(false); // Invalid state
			break;
		}
	} while( __CurrentState != -1 );
}
void ABP_TL4Character_C__pf3043094308::bpf__ExecuteUbergraph_BP_TL4Character__pf_5(int32 bpp__EntryPoint__pf)
{
	FRotator bpfv__CallFunc_GetControlRotation_ReturnValue__pf(EForceInit::ForceInit);
	FRotator bpfv__CallFunc_MakeRotator_ReturnValue__pf(EForceInit::ForceInit);
	FVector bpfv__CallFunc_GetForwardVector_ReturnValue__pf(EForceInit::ForceInit);
	check(bpp__EntryPoint__pf == 38);
	// optimized KCST_UnconditionalGoto
	bpfv__CallFunc_GetControlRotation_ReturnValue__pf = APawn::GetControlRotation();
	UKismetMathLibrary::BreakRotator(bpfv__CallFunc_GetControlRotation_ReturnValue__pf, /*out*/ b0l__CallFunc_BreakRotator_Roll__pf, /*out*/ b0l__CallFunc_BreakRotator_Pitch__pf, /*out*/ b0l__CallFunc_BreakRotator_Yaw__pf);
	bpfv__CallFunc_MakeRotator_ReturnValue__pf = UKismetMathLibrary::MakeRotator(0.000000, 0.000000, b0l__CallFunc_BreakRotator_Yaw__pf);
	bpfv__CallFunc_GetForwardVector_ReturnValue__pf = UKismetMathLibrary::GetForwardVector(bpfv__CallFunc_MakeRotator_ReturnValue__pf);
	AddMovementInput(bpfv__CallFunc_GetForwardVector_ReturnValue__pf, b0l__K2Node_InputAxisEvent_AxisValue__pf, false);
	return; //KCST_EndOfThread
}
void ABP_TL4Character_C__pf3043094308::bpf__ExecuteUbergraph_BP_TL4Character__pf_6(int32 bpp__EntryPoint__pf)
{
	UPawnMovementComponent* bpfv__CallFunc_GetMovementComponent_ReturnValue__pf{};
	bool bpfv__CallFunc_CanJump_ReturnValue__pf{};
	float bpfv__CallFunc_VSize_ReturnValue__pf{};
	bool bpfv__CallFunc_Greater_FloatFloat_ReturnValue__pf{};
	int32 __CurrentState = bpp__EntryPoint__pf;
	do
	{
		switch( __CurrentState )
		{
		case 1:
			{
				__CurrentState = 2;
				break;
			}
		case 2:
			{
				b0l__Temp_struct_Variable__pf = b0l__K2Node_InputActionEvent_Key3__pf;
			}
		case 3:
			{
				StopJumping();
			}
		case 4:
			{
				bpv__JumpxButtonxDown__pfTT = false;
			}
		case 5:
			{
				bpfv__CallFunc_GetMovementComponent_ReturnValue__pf = GetMovementComponent();
				b0l__K2Node_DynamicCast_AsCharacter_Movement_Component__pf = Cast<UCharacterMovementComponent>(bpfv__CallFunc_GetMovementComponent_ReturnValue__pf);
				b0l__K2Node_DynamicCast_bSuccess1__pf = (b0l__K2Node_DynamicCast_AsCharacter_Movement_Component__pf != nullptr);;
				if (!b0l__K2Node_DynamicCast_bSuccess1__pf)
				{
					__CurrentState = -1;
					break;
				}
			}
		case 6:
			{
				FVector  __Local__54 = FVector(0.000000,0.000000,0.000000);
				bpfv__CallFunc_VSize_ReturnValue__pf = UKismetMathLibrary::VSize(((::IsValid(b0l__K2Node_DynamicCast_AsCharacter_Movement_Component__pf)) ? (b0l__K2Node_DynamicCast_AsCharacter_Movement_Component__pf->Velocity) : (__Local__54)));
				bpfv__CallFunc_Greater_FloatFloat_ReturnValue__pf = UKismetMathLibrary::Greater_FloatFloat(bpfv__CallFunc_VSize_ReturnValue__pf, 0.000000);
				if (!bpfv__CallFunc_Greater_FloatFloat_ReturnValue__pf)
				{
					__CurrentState = 8;
					break;
				}
			}
		case 7:
			{
				if(::IsValid(b0l__K2Node_DynamicCast_AsCharacter_Movement_Component__pf))
				{
					b0l__K2Node_DynamicCast_AsCharacter_Movement_Component__pf->JumpZVelocity = 400.000000;
				}
				__CurrentState = -1;
				break;
			}
		case 8:
			{
				if(::IsValid(b0l__K2Node_DynamicCast_AsCharacter_Movement_Component__pf))
				{
					b0l__K2Node_DynamicCast_AsCharacter_Movement_Component__pf->JumpZVelocity = 400.000000;
				}
				__CurrentState = -1;
				break;
			}
		case 9:
			{
				__CurrentState = 10;
				break;
			}
		case 10:
			{
				b0l__Temp_struct_Variable__pf = b0l__K2Node_InputActionEvent_Key2__pf;
			}
		case 11:
			{
				Jump();
			}
		case 12:
			{
				bpfv__CallFunc_CanJump_ReturnValue__pf = ACharacter::CanJump();
				bpv__JumpxButtonxDown__pfTT = bpfv__CallFunc_CanJump_ReturnValue__pf;
				__CurrentState = 5;
				break;
			}
		default:
			break;
		}
	} while( __CurrentState != -1 );
}
void ABP_TL4Character_C__pf3043094308::bpf__ExecuteUbergraph_BP_TL4Character__pf_7(int32 bpp__EntryPoint__pf)
{
	check(bpp__EntryPoint__pf == 13);
	// optimized KCST_UnconditionalGoto
	b0l__Temp_struct_Variable1__pf = b0l__K2Node_InputActionEvent_Key1__pf;
	bpv__CrouchxButtonxDown__pfTT = false;
	return; //KCST_EndOfThread
}
void ABP_TL4Character_C__pf3043094308::bpf__ExecuteUbergraph_BP_TL4Character__pf_8(int32 bpp__EntryPoint__pf)
{
	check(bpp__EntryPoint__pf == 16);
	// optimized KCST_UnconditionalGoto
	b0l__Temp_struct_Variable1__pf = b0l__K2Node_InputActionEvent_Key__pf;
	bpv__CrouchxButtonxDown__pfTT = true;
	return; //KCST_EndOfThread
}
void ABP_TL4Character_C__pf3043094308::bpf__ExecuteUbergraph_BP_TL4Character__pf_9(int32 bpp__EntryPoint__pf)
{
	FRotator bpfv__CallFunc_GetControlRotation_ReturnValue__pf(EForceInit::ForceInit);
	FRotator bpfv__CallFunc_MakeRotator_ReturnValue__pf(EForceInit::ForceInit);
	FVector bpfv__CallFunc_GetRightVector_ReturnValue__pf(EForceInit::ForceInit);
	check(bpp__EntryPoint__pf == 28);
	// optimized KCST_UnconditionalGoto
	bpfv__CallFunc_GetControlRotation_ReturnValue__pf = APawn::GetControlRotation();
	UKismetMathLibrary::BreakRotator(bpfv__CallFunc_GetControlRotation_ReturnValue__pf, /*out*/ b0l__CallFunc_BreakRotator_Roll__pf, /*out*/ b0l__CallFunc_BreakRotator_Pitch__pf, /*out*/ b0l__CallFunc_BreakRotator_Yaw__pf);
	bpfv__CallFunc_MakeRotator_ReturnValue__pf = UKismetMathLibrary::MakeRotator(0.000000, 0.000000, b0l__CallFunc_BreakRotator_Yaw__pf);
	bpfv__CallFunc_GetRightVector_ReturnValue__pf = UKismetMathLibrary::GetRightVector(bpfv__CallFunc_MakeRotator_ReturnValue__pf);
	AddMovementInput(bpfv__CallFunc_GetRightVector_ReturnValue__pf, b0l__K2Node_InputAxisEvent_AxisValue3__pf, false);
	return; //KCST_EndOfThread
}
void ABP_TL4Character_C__pf3043094308::bpf__ExecuteUbergraph_BP_TL4Character__pf_10(int32 bpp__EntryPoint__pf)
{
	check(bpp__EntryPoint__pf == 32);
	// optimized KCST_UnconditionalGoto
	AddControllerPitchInput(b0l__K2Node_InputAxisEvent_AxisValue1__pf);
	return; //KCST_EndOfThread
}
void ABP_TL4Character_C__pf3043094308::bpf__ExecuteUbergraph_BP_TL4Character__pf_11(int32 bpp__EntryPoint__pf)
{
	check(bpp__EntryPoint__pf == 30);
	// optimized KCST_UnconditionalGoto
	AddControllerYawInput(b0l__K2Node_InputAxisEvent_AxisValue2__pf);
	return; //KCST_EndOfThread
}
void ABP_TL4Character_C__pf3043094308::bpf__ReceivePossessed__pf(AController* bpp__NewController__pf)
{
	b0l__K2Node_Event_NewController__pf = bpp__NewController__pf;
	bpf__ExecuteUbergraph_BP_TL4Character__pf_0(44);
}
void ABP_TL4Character_C__pf3043094308::bpf__InpAxisEvt_MoveForward_K2Node_InputAxisEvent_180__pf(float bpp__AxisValue__pf)
{
	b0l__K2Node_InputAxisEvent_AxisValue__pf = bpp__AxisValue__pf;
	bpf__ExecuteUbergraph_BP_TL4Character__pf_5(38);
}
void ABP_TL4Character_C__pf3043094308::bpf__ReceiveAnyDamage__pf(float bpp__Damage__pf, const UDamageType* bpp__DamageType__pf__const, AController* bpp__InstigatedBy__pf, AActor* bpp__DamageCauser__pf)
{
	typedef UDamageType*  T__Local__55;
	T__Local__55& bpp__DamageType__pf = *const_cast<T__Local__55 *>(&bpp__DamageType__pf__const);
	b0l__K2Node_Event_Damage__pf = bpp__Damage__pf;
	b0l__K2Node_Event_DamageType__pf = bpp__DamageType__pf;
	b0l__K2Node_Event_InstigatedBy__pf = bpp__InstigatedBy__pf;
	b0l__K2Node_Event_DamageCauser__pf = bpp__DamageCauser__pf;
	bpf__ExecuteUbergraph_BP_TL4Character__pf_4(35);
}
void ABP_TL4Character_C__pf3043094308::bpf__InpAxisEvt_LookUp_K2Node_InputAxisEvent_268__pf(float bpp__AxisValue__pf)
{
	b0l__K2Node_InputAxisEvent_AxisValue1__pf = bpp__AxisValue__pf;
	bpf__ExecuteUbergraph_BP_TL4Character__pf_10(32);
}
void ABP_TL4Character_C__pf3043094308::bpf__InpAxisEvt_Turn_K2Node_InputAxisEvent_256__pf(float bpp__AxisValue__pf)
{
	b0l__K2Node_InputAxisEvent_AxisValue2__pf = bpp__AxisValue__pf;
	bpf__ExecuteUbergraph_BP_TL4Character__pf_11(30);
}
void ABP_TL4Character_C__pf3043094308::bpf__InpAxisEvt_MoveRight_K2Node_InputAxisEvent_243__pf(float bpp__AxisValue__pf)
{
	b0l__K2Node_InputAxisEvent_AxisValue3__pf = bpp__AxisValue__pf;
	bpf__ExecuteUbergraph_BP_TL4Character__pf_9(28);
}
void ABP_TL4Character_C__pf3043094308::bpf__InpActEvt_BackSpace_K2Node_InputKeyEvent_0__pf(FKey bpp__Key__pf)
{
	b0l__K2Node_InputKeyEvent_Key__pf = bpp__Key__pf;
	bpf__ExecuteUbergraph_BP_TL4Character__pf_2(25);
}
void ABP_TL4Character_C__pf3043094308::bpf__InpActEvt_Gamepad_FaceButton_Right_K2Node_InputKeyEvent_1__pf(FKey bpp__Key__pf)
{
	b0l__K2Node_InputKeyEvent_Key1__pf = bpp__Key__pf;
	bpf__ExecuteUbergraph_BP_TL4Character__pf_2(19);
}
void ABP_TL4Character_C__pf3043094308::bpf__InpActEvt_Crouch_K2Node_InputActionEvent_0__pf(FKey bpp__Key__pf)
{
	b0l__K2Node_InputActionEvent_Key__pf = bpp__Key__pf;
	bpf__ExecuteUbergraph_BP_TL4Character__pf_8(16);
}
void ABP_TL4Character_C__pf3043094308::bpf__InpActEvt_Crouch_K2Node_InputActionEvent_1__pf(FKey bpp__Key__pf)
{
	b0l__K2Node_InputActionEvent_Key1__pf = bpp__Key__pf;
	bpf__ExecuteUbergraph_BP_TL4Character__pf_7(13);
}
void ABP_TL4Character_C__pf3043094308::bpf__InpActEvt_Jump_K2Node_InputActionEvent_2__pf(FKey bpp__Key__pf)
{
	b0l__K2Node_InputActionEvent_Key2__pf = bpp__Key__pf;
	bpf__ExecuteUbergraph_BP_TL4Character__pf_6(9);
}
void ABP_TL4Character_C__pf3043094308::bpf__InpActEvt_Jump_K2Node_InputActionEvent_3__pf(FKey bpp__Key__pf)
{
	b0l__K2Node_InputActionEvent_Key3__pf = bpp__Key__pf;
	bpf__ExecuteUbergraph_BP_TL4Character__pf_6(1);
}
void ABP_TL4Character_C__pf3043094308::bpf__DamagexFade__UpdateFunc__pfT()
{
	bpf__ExecuteUbergraph_BP_TL4Character__pf_3(42);
}
void ABP_TL4Character_C__pf3043094308::bpf__DamagexFade__FinishedFunc__pfT()
{
	bpf__ExecuteUbergraph_BP_TL4Character__pf_1(43);
}
void ABP_TL4Character_C__pf3043094308::bpf__UserConstructionScript__pf()
{
	bpv__Health__pf = bpv__MaxHealth__pf;
}
void ABP_TL4Character_C__pf3043094308::bpf__IsxDead__pfT(/*out*/ bool& bpp__Dead__pf)
{
	bool bpfv__CallFunc_LessEqual_FloatFloat_ReturnValue__pf{};
	bpfv__CallFunc_LessEqual_FloatFloat_ReturnValue__pf = UKismetMathLibrary::LessEqual_FloatFloat(bpv__Health__pf, 0.000000);
	bpp__Dead__pf = bpfv__CallFunc_LessEqual_FloatFloat_ReturnValue__pf;
}
void ABP_TL4Character_C__pf3043094308::bpf__TakexDamage__pfT(float bpp__Damage__pf)
{
	bool bpfv__CallFunc_IsPlayerControlled_ReturnValue__pf{};
	bool bpfv__CallFunc_Is_Dead_Dead__pf{};
	TArray<FName> bpfv__K2Node_MakeArray_Array__pf{};
	float bpfv__CallFunc_Subtract_FloatFloat_ReturnValue__pf{};
	int32 __CurrentState = 1;
	do
	{
		switch( __CurrentState )
		{
		case 1:
			{
				bpfv__CallFunc_Subtract_FloatFloat_ReturnValue__pf = UKismetMathLibrary::Subtract_FloatFloat(bpv__Health__pf, bpp__Damage__pf);
				bpv__Health__pf = bpfv__CallFunc_Subtract_FloatFloat_ReturnValue__pf;
			}
		case 2:
			{
				bpfv__CallFunc_IsPlayerControlled_ReturnValue__pf = IsPlayerControlled();
				if (!bpfv__CallFunc_IsPlayerControlled_ReturnValue__pf)
				{
					__CurrentState = 6;
					break;
				}
			}
		case 3:
			{
				bpf__IsxDead__pfT(/*out*/ bpfv__CallFunc_Is_Dead_Dead__pf);
				if (!bpfv__CallFunc_Is_Dead_Dead__pf)
				{
					__CurrentState = -1;
					break;
				}
			}
		case 4:
			{
				if(::IsValid((*(AccessPrivateProperty<UCapsuleComponent* >((this), ACharacter::__PPO__CapsuleComponent() )))))
				{
					(*(AccessPrivateProperty<UCapsuleComponent* >((this), ACharacter::__PPO__CapsuleComponent() )))->SetCollisionEnabled(ECollisionEnabled::NoCollision);
				}
			}
		case 5:
			{
				bpf__KillxPlayer__pfT();
				__CurrentState = -1;
				break;
			}
		case 6:
			{
				bpfv__K2Node_MakeArray_Array__pf.SetNum(1, true);
				bpfv__K2Node_MakeArray_Array__pf[0] = FName(TEXT("PlayerTeam"));
				Tags = bpfv__K2Node_MakeArray_Array__pf;
				__CurrentState = 3;
				break;
			}
		default:
			break;
		}
	} while( __CurrentState != -1 );
}
void ABP_TL4Character_C__pf3043094308::bpf__KillxPlayer__pfT()
{
	FTimerHandle bpfv__CallFunc_K2_SetTimer_ReturnValue__pf{};
	bool bpfv__CallFunc_IsValid_ReturnValue__pf{};
	bool bpfv__CallFunc_IsPlayerController_ReturnValue__pf{};
	bool bpfv__CallFunc_BooleanAND_ReturnValue__pf{};
	int32 __CurrentState = 1;
	do
	{
		switch( __CurrentState )
		{
		case 1:
			{
				bpfv__CallFunc_IsValid_ReturnValue__pf = UKismetSystemLibrary::IsValid(bpv__OldxController__pfT);
				if(::IsValid(bpv__OldxController__pfT))
				{
					bpfv__CallFunc_IsPlayerController_ReturnValue__pf = bpv__OldxController__pfT->AController::IsPlayerController();
				}
				bpfv__CallFunc_BooleanAND_ReturnValue__pf = UKismetMathLibrary::BooleanAND(bpfv__CallFunc_IsPlayerController_ReturnValue__pf, bpfv__CallFunc_IsValid_ReturnValue__pf);
				if (!bpfv__CallFunc_BooleanAND_ReturnValue__pf)
				{
					__CurrentState = 6;
					break;
				}
			}
		case 2:
			{
				if(::IsValid(bpv__OldxController__pfT))
				{
					bpv__OldxController__pfT->UnPossess();
				}
			}
		case 3:
			{
				bpf__SpawnxDeathxCamera__pfTT();
			}
		case 4:
			{
				if (!bpv__IsxGamexOver__pfTT)
				{
					__CurrentState = -1;
					break;
				}
			}
		case 5:
			{
				bpf__RenderxGamexOverxMenu__pfTTT();
				__CurrentState = -1;
				break;
			}
		case 6:
			{
				bpfv__CallFunc_K2_SetTimer_ReturnValue__pf = UKismetSystemLibrary::K2_SetTimer(this, FString(TEXT("CallDestroy")), 3.000000, false);
				__CurrentState = -1;
				break;
			}
		default:
			break;
		}
	} while( __CurrentState != -1 );
}
void ABP_TL4Character_C__pf3043094308::bpf__SpawnxDeathxCamera__pfTT()
{
	APlayerController* bpfv__K2Node_DynamicCast_AsPlayer_Controller__pf{};
	bool bpfv__K2Node_DynamicCast_bSuccess__pf{};
	FTransform bpfv__CallFunc_K2_GetComponentToWorld_ReturnValue__pf{};
	AActor* bpfv__CallFunc_GetViewTarget_ReturnValue__pf{};
	AActor* bpfv__CallFunc_BeginDeferredActorSpawnFromClass_ReturnValue__pf{};
	ACameraActor* bpfv__K2Node_DynamicCast_AsCamera_Actor__pf{};
	bool bpfv__K2Node_DynamicCast_bSuccess1__pf{};
	ACameraActor* bpfv__CallFunc_FinishSpawningActor_ReturnValue__pf{};
	int32 __CurrentState = 1;
	do
	{
		switch( __CurrentState )
		{
		case 1:
			{
				bpfv__K2Node_DynamicCast_AsPlayer_Controller__pf = Cast<APlayerController>(bpv__OldxController__pfT);
				bpfv__K2Node_DynamicCast_bSuccess__pf = (bpfv__K2Node_DynamicCast_AsPlayer_Controller__pf != nullptr);;
				if (!bpfv__K2Node_DynamicCast_bSuccess__pf)
				{
					__CurrentState = -1;
					break;
				}
			}
		case 2:
			{
				if(::IsValid(bpfv__K2Node_DynamicCast_AsPlayer_Controller__pf))
				{
					bpfv__CallFunc_GetViewTarget_ReturnValue__pf = bpfv__K2Node_DynamicCast_AsPlayer_Controller__pf->GetViewTarget();
				}
				bpfv__K2Node_DynamicCast_AsCamera_Actor__pf = Cast<ACameraActor>(bpfv__CallFunc_GetViewTarget_ReturnValue__pf);
				bpfv__K2Node_DynamicCast_bSuccess1__pf = (bpfv__K2Node_DynamicCast_AsCamera_Actor__pf != nullptr);;
				if (!bpfv__K2Node_DynamicCast_bSuccess1__pf)
				{
					__CurrentState = 3;
					break;
				}
				__CurrentState = -1;
				break;
			}
		case 3:
			{
				if(::IsValid(bpv__DeathCameraSpawnPoint__pf))
				{
					bpfv__CallFunc_K2_GetComponentToWorld_ReturnValue__pf = bpv__DeathCameraSpawnPoint__pf->USceneComponent::K2_GetComponentToWorld();
				}
				bpfv__CallFunc_BeginDeferredActorSpawnFromClass_ReturnValue__pf = UGameplayStatics::BeginDeferredActorSpawnFromClass(this, ACameraActor::StaticClass(), bpfv__CallFunc_K2_GetComponentToWorld_ReturnValue__pf, ESpawnActorCollisionHandlingMethod::Undefined, ((AActor*)nullptr));
			}
		case 4:
			{
				if(::IsValid(bpv__DeathCameraSpawnPoint__pf))
				{
					bpfv__CallFunc_K2_GetComponentToWorld_ReturnValue__pf = bpv__DeathCameraSpawnPoint__pf->USceneComponent::K2_GetComponentToWorld();
				}
				bpfv__CallFunc_FinishSpawningActor_ReturnValue__pf = CastChecked<ACameraActor>(UGameplayStatics::FinishSpawningActor(bpfv__CallFunc_BeginDeferredActorSpawnFromClass_ReturnValue__pf, bpfv__CallFunc_K2_GetComponentToWorld_ReturnValue__pf), ECastCheckedType::NullAllowed);
			}
		case 5:
			{
				if(::IsValid(bpfv__K2Node_DynamicCast_AsPlayer_Controller__pf))
				{
					bpfv__K2Node_DynamicCast_AsPlayer_Controller__pf->SetViewTargetWithBlend(bpfv__CallFunc_FinishSpawningActor_ReturnValue__pf, 1.000000, EViewTargetBlendFunction::VTBlend_Linear, 0.000000, false);
				}
				__CurrentState = -1;
				break;
			}
		default:
			break;
		}
	} while( __CurrentState != -1 );
}
void ABP_TL4Character_C__pf3043094308::bpf__RenderxGamexOverxMenu__pfTTT()
{
	APlayerController* bpfv__CallFunc_GetPlayerController_ReturnValue__pf{};
	UWBP_GameOver_C__pf3389175594* bpfv__CallFunc_Create_ReturnValue__pf{};
	bpfv__CallFunc_Create_ReturnValue__pf = CastChecked<UWBP_GameOver_C__pf3389175594>(UWidgetBlueprintLibrary::Create(this, UWBP_GameOver_C__pf3389175594::StaticClass(), ((APlayerController*)nullptr)), ECastCheckedType::NullAllowed);
	if(::IsValid(bpfv__CallFunc_Create_ReturnValue__pf))
	{
		bpfv__CallFunc_Create_ReturnValue__pf->UUserWidget::AddToViewport(0);
	}
	bpv__IsxGamexOver__pfTT = false;
	bpfv__CallFunc_GetPlayerController_ReturnValue__pf = UGameplayStatics::GetPlayerController(this, 0);
	if(::IsValid(bpfv__CallFunc_GetPlayerController_ReturnValue__pf))
	{
		bpfv__CallFunc_GetPlayerController_ReturnValue__pf->bShowMouseCursor = true;
	}
}
void ABP_TL4Character_C__pf3043094308::bpf__CallDestroy__pf()
{
	K2_DestroyActor();
}
PRAGMA_DISABLE_OPTIMIZATION
void ABP_TL4Character_C__pf3043094308::__StaticDependencies_DirectlyUsedAssets(TArray<FBlueprintDependencyData>& AssetsToLoad)
{
	const FCompactBlueprintDependencyData LocCompactBlueprintDependencyData[] =
	{
		{55, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  SkeletalMesh /Game/Static/NPC/Character/Mesh/SK_Mannequin.SK_Mannequin 
		{56, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  SkeletalMesh /Game/Static/Player/Character/Mesh/SK_Mannequin_Arms.SK_Mannequin_Arms 
		{57, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  BlueprintGeneratedClass /Game/Dynamic/Weapons/FPWeapon/Behaviour/BP_Gun.BP_Gun_C 
	};
	for(const FCompactBlueprintDependencyData& CompactData : LocCompactBlueprintDependencyData)
	{
		AssetsToLoad.Add(FBlueprintDependencyData(F__NativeDependencies::Get(CompactData.ObjectRefIndex), CompactData));
	}
}
PRAGMA_ENABLE_OPTIMIZATION
PRAGMA_DISABLE_OPTIMIZATION
void ABP_TL4Character_C__pf3043094308::__StaticDependenciesAssets(TArray<FBlueprintDependencyData>& AssetsToLoad)
{
	__StaticDependencies_DirectlyUsedAssets(AssetsToLoad);
	const FCompactBlueprintDependencyData LocCompactBlueprintDependencyData[] =
	{
		{58, FBlueprintDependencyType(true, false, false, false), FBlueprintDependencyType(false, false, false, false)},  //  Class /Script/TL4_TestingGrounds.PatrolRoute 
		{29, FBlueprintDependencyType(true, false, false, false), FBlueprintDependencyType(false, false, false, false)},  //  Class /Script/Engine.BoxComponent 
		{30, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  Class /Script/NavigationSystem.NavArea_Obstacle 
		{59, FBlueprintDependencyType(true, false, false, false), FBlueprintDependencyType(false, false, false, false)},  //  Class /Script/Engine.PostProcessComponent 
		{28, FBlueprintDependencyType(true, false, false, false), FBlueprintDependencyType(false, false, false, false)},  //  Class /Script/Engine.ArrowComponent 
		{1, FBlueprintDependencyType(true, false, false, false), FBlueprintDependencyType(false, false, false, false)},  //  Class /Script/Engine.SceneComponent 
		{60, FBlueprintDependencyType(true, false, false, false), FBlueprintDependencyType(false, false, false, false)},  //  ScriptStruct /Script/InputCore.Key 
		{45, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  Class /Script/Engine.PlayerController 
		{61, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  Class /Script/Engine.PawnMovementComponent 
		{62, FBlueprintDependencyType(true, false, false, false), FBlueprintDependencyType(false, false, false, false)},  //  Class /Script/Engine.CharacterMovementComponent 
		{63, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  Class /Script/Engine.DamageType 
		{64, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  Class /Script/Engine.Controller 
		{5, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  Class /Script/Engine.Actor 
		{3, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  Class /Script/Engine.KismetMathLibrary 
		{65, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  Class /Script/Engine.MovementComponent 
		{32, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  Class /Script/Engine.Character 
		{6, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  Class /Script/Engine.GameplayStatics 
		{48, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  Class /Script/UMG.WidgetBlueprintLibrary 
		{51, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  Class /Script/UMG.UserWidget 
		{66, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  Class /Script/Engine.KismetStringLibrary 
		{67, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  Class /Script/Engine.Pawn 
		{68, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  Class /Script/Engine.TimelineComponent 
		{69, FBlueprintDependencyType(true, false, false, false), FBlueprintDependencyType(false, false, false, false)},  //  Class /Script/TL4_TestingGrounds.Mannequin 
		{4, FBlueprintDependencyType(true, false, false, false), FBlueprintDependencyType(false, false, false, false)},  //  ScriptStruct /Script/Engine.PointerToUberGraphFrame 
		{70, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  Class /Script/Engine.CameraActor 
		{71, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  ScriptStruct /Script/Engine.TimerHandle 
		{46, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  Class /Script/Engine.KismetSystemLibrary 
		{72, FBlueprintDependencyType(true, false, false, false), FBlueprintDependencyType(false, false, false, false)},  //  Enum /Script/Engine.ETimelineDirection 
		{73, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  BP_Gun_C /Game/Dynamic/Weapons/FPWeapon/Behaviour/BP_Gun.Default__BP_Gun_C 
		{74, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  Class /Script/ClothingSystemRuntime.ClothingSimulationFactoryNv 
		{75, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  WidgetBlueprintGeneratedClass /Game/Dynamic/UI/WBP_Resetting.WBP_Resetting_C 
		{76, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  WidgetBlueprintGeneratedClass /Game/Dynamic/UI/WBP_GameOver.WBP_GameOver_C 
		{77, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  AnimBlueprintGeneratedClass /Game/Dynamic/TL4Character/Animations/ABP_FP.ABP_FP_C 
		{78, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  AnimBlueprintGeneratedClass /Game/Dynamic/TL4Character/Animations/ABP_TP.ABP_TP_C 
		{79, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  BlueprintGeneratedClass /Game/Dynamic/TL4Character/AI/BP_NPC_AI.BP_NPC_AI_C 
	};
	for(const FCompactBlueprintDependencyData& CompactData : LocCompactBlueprintDependencyData)
	{
		AssetsToLoad.Add(FBlueprintDependencyData(F__NativeDependencies::Get(CompactData.ObjectRefIndex), CompactData));
	}
}
PRAGMA_ENABLE_OPTIMIZATION
struct FRegisterHelper__ABP_TL4Character_C__pf3043094308
{
	FRegisterHelper__ABP_TL4Character_C__pf3043094308()
	{
		FConvertedBlueprintsDependencies::Get().RegisterConvertedClass(TEXT("/Game/Dynamic/TL4Character/Behaviour/BP_TL4Character"), &ABP_TL4Character_C__pf3043094308::__StaticDependenciesAssets);
	}
	static FRegisterHelper__ABP_TL4Character_C__pf3043094308 Instance;
};
FRegisterHelper__ABP_TL4Character_C__pf3043094308 FRegisterHelper__ABP_TL4Character_C__pf3043094308::Instance;
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
